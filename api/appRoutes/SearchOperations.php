<?php



function createSearchOperation($parameters, $conn, $userId, $logger) {
	if (isset($parameters["operation_beginning_date"], $parameters["location_region"], $parameters["location_district"], $parameters["location_locality"])) {
		$insertedValues = array();
		$insertedValues["location_region"] = ":location_region";
		$insertedValues["location_district"] = ":location_district";
		$insertedValues["location_locality"] = ":location_locality";
		$insertedValues["operation_beginning_date"] = ":operation_beginning_date";

		$optionalParameters = array("index", "operation_accident_date", "forum_url", "organization", "index_number", "leader", "description", "center_coordinates");
		for ($i = 0; $i < count($optionalParameters); $i++) {
			if (isset($parameters[$optionalParameters[$i]])) {
				$insertedValues[$optionalParameters[$i]] = ":" . $optionalParameters[$i];
			}
		}

		$insertedValues["author_id"] = ":author_id";
		$parameters["author_id"] = $userId;

		$keys = array_keys($insertedValues);
		$prepared = $conn->prepare("INSERT INTO search_operations(" . implode(",", $keys) . ") VALUES(" . implode(", ", $insertedValues) . ") RETURNING id;");
		for ($i = 0; $i < count($keys); $i++) {
			$prepared->bindParam(":" . $keys[$i], $parameters[$keys[$i]]);
		}

		if ($prepared->execute()) {
			return $prepared->fetch();
		} else {
			return false;
		}
	} else {
		$logger->error("Not enough parameters for creating the operation", $parameters);
		return false;
	}
}



/**
 * Restrieve search operations.
 */
$app->get('/searchoperations', function ($request, $response, $args) {
    global $conn;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$prepared = $conn->prepare("SELECT * FROM search_operations");
			$prepared->execute();

			$send = $prepared->fetchAll(PDO::FETCH_ASSOC);

			$response = $response->withHeader('Content-type', 'application/json');
			$body = $response->getBody();
			$body->write(json_encode($send));
		}
	}

	return $response;
});


/**
 * Imports search operation by identifier from the www.extremum.spb.ru.
 */
$app->post('/searchoperations/import', function ($request, $response, $args) {
    global $conn;
    global $config;
	global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));

		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$requestParameters = $request->getParsedBody();
			if (isset($requestParameters["id"])) {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "www.extremum.spb.ru/data1/extremum/ex.nsf/getpsrjsonsimpo?openagent&ID=" . $requestParameters["id"]);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$output = curl_exec($ch);
				$lastCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);

				if ($lastCode === 200) {
					$data = json_decode($output, true);
					if (is_null($data)) {
						$data = json_decode(mb_convert_encoding($output, "utf-8", "windows-1251"), true);
					}

					$searchOperation = $data["psr"];
					$parameters = array();				
					
					$parameters["index_number"] = $requestParameters["id"];

					if (isset($searchOperation["district0"]) && strlen($searchOperation["district0"]) > 0) {
						$parameters["location_region"] = $searchOperation["district0"];
					} else {
						$parameters["location_region"] = $config["searchOperationImport"]["defaultRegion"];
					}

					if (isset($searchOperation["district"])) $parameters["location_district"] = $searchOperation["district"];
					if (isset($searchOperation["town"])) $parameters["location_locality"] = $searchOperation["town"];

					if (isset($searchOperation["since"])) {
						$date = DateTime::createFromFormat("dmY", $searchOperation["since"]);
						$parameters["operation_beginning_date"] = $date->format(DateTime::ISO8601);
					}

					if (isset($searchOperation["emdate"])) {
						$date = DateTime::createFromFormat("dmY", $searchOperation["emdate"]);
						$parameters["operation_accident_date"] = $date->format(DateTime::ISO8601);
					}

					if (isset($searchOperation["description"])) $parameters["description"] = $searchOperation["description"];
					if (isset($searchOperation["forum"])) $parameters["forum_url"] = $searchOperation["forum"];

					if (isset($searchOperation["lat"]) && isset($searchOperation["lon"])) {
						$parameters["center_coordinates"] = str_replace(",", ".", $searchOperation["lon"]) . " " . str_replace(",", ".", $searchOperation["lat"]);
					}

					$conn->beginTransaction();

					$result = createSearchOperation($parameters, $conn, $userId, $logger);
					if ($result === false) {

						$conn->rollBack();

						$response = $response->withStatus(400);
						$response = $response->withHeader('Content-type', 'application/json');
						$body = $response->getBody();
						$body->write(json_encode(array(
							"error" => "UNABLE_TO_CREATE_SEARCH_OPERATION"
						)));
					} else {
						$createdSearchOperationId = $result["id"];
						$createdSearchOperationName = $parameters["location_region"] . ", " . $parameters["location_district"] . ", " . $parameters["location_locality"];

						$lostImported = true;
						if (isset($searchOperation["lost"]) && count($searchOperation["lost"]) > 0) {
							$numberOfLost = count($searchOperation["lost"]);
							for ($i = 0; $i < $numberOfLost; $i++) {
								$person = array();
								if ($searchOperation["lost"][$i]["sex"] === "M") {
									$person["sex"] = 0;
								} else if ($searchOperation["lost"][$i]["sex"] === "F") {
									$person["sex"] = 1;
								} else {
									$person["sex"] = 0;
								}

								$bornYear = 0;
								if (empty($searchOperation["lost"][$i]["born"]) === false) $bornYear = $searchOperation["lost"][$i]["born"];
								
								$subresult = $conn->query("INSERT INTO missing_people (name, birth_year, sex, author_id, search_operation_id) VALUES ('" .
								$searchOperation["lost"][$i]["fio"] . "', " . $bornYear . ", "
								. $person["sex"] . ", " . $userId . ", " . $createdSearchOperationId . ")");

								if ($subresult === false) {
									$lostImported = false;
									break;
								}
							}
						}

						if ($lostImported === false) {
							$logger->error("Unable to import missing persons for search operation:", $searchOperation);
							
							$conn->rollBack();

							$response = $response->withStatus(400);
							$response = $response->withHeader('Content-type', 'application/json');
							$body = $response->getBody();
							$body->write(json_encode(array(
								"error" => "UNABLE_TO_CREATE_MISSING_PERSON_FOR_SEARCH_OPERATION"
							)));
						} else {
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, "www.extremum.spb.ru/data1/extremum/ex.nsf/getpsrtrackjsonsimpo?openagent&ID=" . $requestParameters["id"]);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							$output = curl_exec($ch);
							$lastCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
							curl_close($ch);

							if ($lastCode === 200) {
								$tracksObject = json_decode($output, true);
								if (is_null($tracksObject)) {
									$tracksObject = json_decode(mb_convert_encoding($output, "utf-8", "windows-1251"), true);
								}
								
								$tracks = $tracksObject["psrtracklist"];
								$numberOfTracks = count($tracks);

								if ($numberOfTracks > 0) {
									$allTracksAreImported = true;
									for ($i = 0; $i < $numberOfTracks; $i++) {
										$route = array();

										$trackId = $tracks[$i]["id"];									
										
										$ch = curl_init();
										curl_setopt($ch, CURLOPT_URL, "www.extremum.spb.ru/data1/extremum/ex.nsf/0/" . $trackId . "/\$file/" . $trackId . "83.xml");
										curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
										$output = curl_exec($ch);
										$lastCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
										curl_close($ch);
										
										if ($lastCode === 200) {
											$trackGeo = geoPHP::load($output, "kml");
											$trackGeoElements = $trackGeo->getComponents();
											$trackPointRaw = "";

											for ($j = 0; $j < count($trackGeoElements); $j++) {
												$trackPointRaw .= $trackGeoElements[$j]->x() . " " . $trackGeoElements[$j]->y();
												if ($j < (count($trackGeoElements) - 1)) {
													$trackPointRaw .= ",";
												}
											}

											$route["geodata"] = "LINESTRING (" . $trackPointRaw . ")";

											$route["participants"] = $tracks[$i]["rescuers"];

											$date = DateTime::createFromFormat("dmY", $tracks[$i]["datetrack"]);
											$route["date_from"] = $date->format(DateTime::ISO8601);
											$route["date_to"] = $date->format(DateTime::ISO8601);

											if (isset($tracks[$i]["datetrackstart"]) && strlen($tracks[$i]["datetrackstart"]) > 0) {
												$date = DateTime::createFromFormat("dmY H:i:s", $tracks[$i]["datetrackstart"]);
												$route["date_from"] = $date->format(DateTime::ISO8601);

												if (isset($tracks[$i]["datetrackend"]) && strlen($tracks[$i]["datetrackend"]) > 0) {
													$date = DateTime::createFromFormat("dmY H:i:s", $tracks[$i]["datetrackend"]);
													$route["date_to"] = $date->format(DateTime::ISO8601);
												}
											}

											$query = "INSERT INTO routes(participants, date_from, date_to, geodata, author_id, search_operation_id) VALUES('"
											. $route["participants"] . "', '" . $route["date_from"] . "', '" . $route["date_to"] . "', ST_GeomFromText('" . $route["geodata"]
											. "', 4326), " . $userId . ", " . $createdSearchOperationId . ");";

											$subresult = $conn->query($query);
											if ($subresult === false) {
												$logger->error("Unable to import track, query: " . $query);
												
												$allTracksAreImported = false;
												break;
											}
										} else {
											$logger->notice("Unable to fetch GPX for track, track id: " . $trackId . ", status: " . $lastCode);
										}
									}

									if ($allTracksAreImported === false) {
										$logger->error("Unable to import tracks: ", $tracks);

										$conn->rollBack();

										$response = $response->withStatus(400);
										$response = $response->withHeader('Content-type', 'application/json');
										$body = $response->getBody();
										$body->write(json_encode(array(
											"error" => "UNABLE_TO_IMPORT_TRACKS_FOR_SEARCH_OPERATION"
										)));
									} else {
										$logger->info("Search operation was imported (with " . $numberOfTracks . " tracks): ", $searchOperation);
										
										$conn->commit();

										$response = $response->withHeader('Content-type', 'application/json');
										$body = $response->getBody();
										$body->write(json_encode(array(
											"id" => $createdSearchOperationId,
											"name" => $createdSearchOperationName
										)));
									}
								} else {
									$logger->info("Search operation was imported (with 0 tracks): ", $searchOperation);

									$conn->commit();

									$response = $response->withHeader('Content-type', 'application/json');
									$body = $response->getBody();
									$body->write(json_encode(array(
										"id" => $createdSearchOperationId,
										"name" => $createdSearchOperationName
									)));
								}
							} else {
								$logger->error("Unable to get tracks for search operation: ", $searchOperation);
								
								$conn->rollBack();

								$response = $response->withStatus(400);
								$response = $response->withHeader('Content-type', 'application/json');
								$body = $response->getBody();
								$body->write(json_encode(array(
									"error" => "UNABLE_TO_GET_TRACKS_FOR_SEARCH_OPERATION"
								)));
							}
						}
					}
				} else {
					$logger->error("Invalid search operation fetched, status: " . $lastCode, $lastCode);
					
					$response = $response->withStatus(400);
					$response = $response->withHeader('Content-type', 'application/json');
					$body = $response->getBody();
					$body->write(json_encode(array(
						"error" => "INVALID_FETCHED_DATA_FOR_SEARCH_OPERATION"
					)));
				}
			} else {
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});

/**
 * Create new search operation.
 */
$app->post('/searchoperations', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();

			$result = createSearchOperation($parameters, $conn, $userId, $logger);
			if ($result === false) {
				$logger->error("Failed search operation creation", $parameters);

				$response = $response->withStatus(500);
			} else {
				$response = $response->withHeader('Content-type', 'application/json');
				$body = $response->getBody();
				$body->write(json_encode(array(
					"id" => $result["id"]
				)));
			}
		}
	}

	return $response;
});


/**
 * Update specific search operation.
 */
$app->put('/searchoperation/{id}', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();

			$updatedValues = array();

			$optionalParameters = array("index", "location_region", "location_district", "location_locality", "index_number", "operation_beginning_date", "operation_accident_date", "organization", "leader", "forum_url", "description", "center_coordinates");

			for ($i = 0; $i < count($optionalParameters); $i++) {
				if (isset($parameters[$optionalParameters[$i]])) {
					$updatedValues[$optionalParameters[$i]] = " " . $optionalParameters[$i] . " = :" . $optionalParameters[$i];
				}
			}

			$keys = array_keys($updatedValues);
			$prepared = $conn->prepare("UPDATE search_operations SET " . implode(", ", $updatedValues) . " WHERE id = :id;");
			for ($i = 0; $i < count($keys); $i++) {
				$prepared->bindParam(":" . $keys[$i], $parameters[$keys[$i]]);
			}

			$prepared->bindParam(":id", $id);
			if ($prepared->execute()) {
				$body = $response->getBody();
				$body->write("OK");
			} else {
				ob_start();
				$prepared->debugDumpParams();
				$statement = ob_get_clean();

				$logger->error("Failed query: " . $statement, $conn->errorInfo());
				
				$response = $response->withStatus(500);
			}
		}
	}

	return $response;
});


/**
 * Retrieve specific search operation.
 */
$app->get('/searchoperation/{id}', function ($request, $response, $args) {
    global $conn;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$prepared = $conn->prepare("SELECT * FROM search_operations WHERE id = :id");
			$prepared->bindParam("id", $id);
			$prepared->execute();

			$send = array();
			if ($prepared->rowCount() > 0) {
				$send = $prepared->fetchAll(PDO::FETCH_ASSOC);

				$response = $response->withHeader('Content-type', 'application/json');
				$body = $response->getBody();
				$body->write(json_encode($send));
			} else {
				$response = $response->withStatus(404);
			}
		}
	}

	return $response;
});


/**
 * Delete specific search operation.
 */
$app->delete('/searchoperation/{id}', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$successfullDeletion = true;
			
			$prepared = $conn->prepare("DELETE FROM missing_people WHERE search_operation_id = :id");
			$prepared->bindParam(":id", $id);
			if ($prepared->execute() == false) {
				ob_start();
				$prepared->debugDumpParams();
				$statement = ob_get_clean();
				$logger->error("Failed query: " . $statement, $conn->errorInfo());
				$successfullDeletion = false;
			}
			
			$prepared = $conn->prepare("DELETE FROM routes WHERE search_operation_id = :id");
			$prepared->bindParam(":id", $id);
			if ($prepared->execute() == false) {
				ob_start();
				$prepared->debugDumpParams();
				$statement = ob_get_clean();
				$logger->error("Failed query: " . $statement, $conn->errorInfo());
				$successfullDeletion = false;
			}

			$prepared = $conn->prepare("DELETE FROM special_markup WHERE search_operation_id = :id");
			$prepared->bindParam(":id", $id);
			if ($prepared->execute() == false) {
				ob_start();
				$prepared->debugDumpParams();
				$statement = ob_get_clean();
				$logger->error("Failed query: " . $statement, $conn->errorInfo());
				$successfullDeletion = false;
			}
		
			$prepared = $conn->prepare("DELETE FROM versions WHERE search_operation_id = :id");
			$prepared->bindParam(":id", $id);
			if ($prepared->execute() == false) {
				ob_start();
				$prepared->debugDumpParams();
				$statement = ob_get_clean();
				$logger->error("Failed query: " . $statement, $conn->errorInfo());
				$successfullDeletion = false;
			}

			$prepared = $conn->prepare("DELETE FROM search_operations WHERE id = :id");
			$prepared->bindParam(":id", $id);
			if ($prepared->execute() == false) {
				ob_start();
				$prepared->debugDumpParams();
				$statement = ob_get_clean();
				$logger->error("Failed query: " . $statement, $conn->errorInfo());
				$successfullDeletion = false;
			}

			if ($successfullDeletion) {
				$body = $response->getBody();
				$body->write("OK");
			} else {
				$response = $response->withStatus(500);
			}
		}
	}

	return $response;
});



?>
