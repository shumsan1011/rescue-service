<?php

/**
 * Get all settings templates.
 */
$app->get('/settings-templates', function ($request, $response, $args) {
    global $conn;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$prepared = $conn->prepare("SELECT * FROM settings_templates");
			$prepared->execute();

			$send = $prepared->fetchAll(PDO::FETCH_ASSOC);

			$response = $response->withHeader('Content-type', 'application/json');
			$body = $response->getBody();
			$body->write(json_encode($send));
		}
	}

	return $response;
});


/**
 * Create new setting template.
 */
$app->post('/settings-templates', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
				$parameters = $request->getParsedBody();

				$insertedValues = array();
				$insertedValues["name"] = ":name";

				$optionalParameters = array("height", "weight", "speed", "tiredness", "hearing", "voice", "goal");
				for ($i = 0; $i < count($optionalParameters); $i++) {
					if (isset($parameters[$optionalParameters[$i]])) {
						$insertedValues[$optionalParameters[$i]] = ":" . $optionalParameters[$i];
					}
				}

				$insertedValues["author_id"] = ":author_id";
				$parameters["author_id"] = $userId;

				$keys = array_keys($insertedValues);
				$prepared = $conn->prepare("INSERT INTO settings_templates(" . implode(",", $keys) . ") VALUES(" . implode(", ", $insertedValues) . ") RETURNING id;");
				for ($i = 0; $i < count($keys); $i++) {
					$prepared->bindParam(":" . $keys[$i], $parameters[$keys[$i]]);
				}

				if ($prepared->execute()) {
					$result = $prepared->fetch();

					$response = $response->withHeader('Content-type', 'application/json');
					$body = $response->getBody();
					$body->write(json_encode(array(
						"id" => $result["id"]
					)));
				} else {
					ob_start();
					$prepared->debugDumpParams();
					$statement = ob_get_clean();

					$logger->error("Failed query: " . $statement, $conn->errorInfo());
					
					$response = $response->withStatus(500);
				}
		}
	}

	return $response;
});


/**
 * Delete specific settings template.
 */
$app->delete('/settings-templates/{id}', function ($request, $response, $args) {
    global $conn;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$prepared = $conn->prepare("DELETE FROM settings_templates WHERE id = :id");
			$prepared->bindParam(":id", $id);
			$result = $prepared->execute();
			
			if ($result) {
				$body = $response->getBody();
				$body->write("OK");
			} else {
				$response = $response->withStatus(500);
			}
		}
	}

	return $response;
});


?>
