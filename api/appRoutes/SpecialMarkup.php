<?php

/**
 * Restrieve all special markup.
 */
$app->get('/special-markup', function ($request, $response, $args) {
    global $conn;

	$params = $request->getQueryParams();

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$prepared = $conn->prepare("SELECT id, author_id, created_at, type, description, properties, display_style, ST_AsText(geodata) as geodata, sp, skd, ovn, shk, search_operation_id FROM special_markup WHERE search_operation_id = :search_operation_id OR search_operation_id IS NULL");
			$prepared->bindParam(":search_operation_id", $params["search_operation_id"]);
			$prepared->execute();

			$send = $prepared->fetchAll(PDO::FETCH_ASSOC);

			$response = $response->withHeader('Content-type', 'application/json');
			$body = $response->getBody();
			$body->write(json_encode($send));
		}
	}

	return $response;
});


/**
 * Create new mspecial markup element.
 */
$app->post('/special-markup', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();
			if (isset($parameters["type"], $parameters["geodata"], $parameters["description"])) {
				$insertedValues = array();
				$insertedValues["type"] = ":type";
				$insertedValues["geodata"] = "ST_GeomFromText(:geodata)";

				$optionalParameters = array("description", "display_style", "properties", "sp", "skd", "ovn", "shk", "search_operation_id");
				for ($i = 0; $i < count($optionalParameters); $i++) {
					if (isset($parameters[$optionalParameters[$i]])) {
						$insertedValues[$optionalParameters[$i]] = ":" . $optionalParameters[$i];
					}
				}

				$insertedValues["author_id"] = ":author_id";
				$parameters["author_id"] = $userId;
				$insertedValues["created_at"] = ":created_at";
				$parameters["created_at"] = "now()";

				$keys = array_keys($insertedValues);
				$prepared = $conn->prepare("INSERT INTO special_markup(" . implode(",", $keys) . ") VALUES(" . implode(", ", $insertedValues) . ") RETURNING id;");
				for ($i = 0; $i < count($keys); $i++) {
					$prepared->bindParam(":" . $keys[$i], $parameters[$keys[$i]]);
				}

				if ($prepared->execute()) {
					$result = $prepared->fetch();

					if (isset($result["id"])) {
						$response = $response->withHeader('Content-type', 'application/json');
						$body = $response->getBody();
						$body->write(json_encode(array(
							"id" => $result["id"]
						)));
					} else {
						$response = $response->withStatus(500);
					}
				} else {
					ob_start();
					$prepared->debugDumpParams();
					$statement = ob_get_clean();

					$logger->error("Failed query: " . $statement, $conn->errorInfo());
					
					$response = $response->withStatus(500);
				}
			} else {
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});



/**
 * Update specific special markup element.
 */
$app->put('/special-markup/{id}', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();
			$updatedValues = array();

			$optionalParameters = array("geodata", "description", "display_style", "properties", "sp", "skd", "ovn", "shk", "search_operation_id");

			for ($i = 0; $i < count($optionalParameters); $i++) {
				if (isset($parameters[$optionalParameters[$i]])) {
					if ($optionalParameters[$i] === "geodata") {
						$updatedValues[$optionalParameters[$i]] = " " . $optionalParameters[$i] . " = ST_GeomFromText(:" . $optionalParameters[$i] . ")";
					} else {
						$updatedValues[$optionalParameters[$i]] = " " . $optionalParameters[$i] . " = :" . $optionalParameters[$i];
					}
				}
			}

			if (isset($parameters["search_operation_id"]) === false) {
				$updatedValues["search_operation_id"] = "search_operation_id = :search_operation_id";
			}

			$keys = array_keys($updatedValues);
			$prepared = $conn->prepare("UPDATE special_markup SET " . implode(", ", $updatedValues) . " WHERE id = :id;");
			for ($i = 0; $i < count($keys); $i++) {
				$prepared->bindParam(":" . $keys[$i], $parameters[$keys[$i]]);
			}
		
			if (isset($parameters["search_operation_id"]) === false) {
				$value = null;
				$prepared->bindParam(":search_operation_id", $value, PDO::PARAM_STR);
			}
		
			$prepared->bindParam(":id", $id);
			if ($prepared->execute()) {
				$body = $response->getBody();
				$body->write("OK");
			} else {
				ob_start();
				$prepared->debugDumpParams();
				$statement = ob_get_clean();

				$logger->error("Failed query: " . $statement, $conn->errorInfo());
					
				$response = $response->withStatus(500);
			}
		}
	}

	return $response;
});


/**
 * Delete specific special markup.
 */
$app->delete('/special-markup/{id}', function ($request, $response, $args) {
    global $conn;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$prepared = $conn->prepare("DELETE FROM special_markup WHERE id = :id");
			$prepared->bindParam(":id", $id);

			$result = $prepared->execute();
			if ($result) {
				$body = $response->getBody();
				$body->write("OK");
			} else {
				$response = $response->withStatus(500);
			}
		}
	}

	return $response;
});

?>
