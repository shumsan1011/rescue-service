<?php


/**
 * Create new route.
 */
$app->post('/routes', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();
			if (isset($parameters["geodata"], $parameters["participants"], $parameters["search_operation_id"])) {
				$insertedValues = array();
				$insertedValues["geodata"] = "ST_GeomFromText(:geodata, 4326)";
				$insertedValues["participants"] = ":participants";
				$insertedValues["search_operation_id"] = ":search_operation_id";

				$optionalParameters = array("date_from", "date_to", "hearing_zone");
				for ($i = 0; $i < count($optionalParameters); $i++) {
					if (isset($parameters[$optionalParameters[$i]])) {
						$insertedValues[$optionalParameters[$i]] = ":" . $optionalParameters[$i];
					}
				}

				$insertedValues["author_id"] = ":author_id";
				$parameters["author_id"] = $userId;

				$keys = array_keys($insertedValues);
				$prepared = $conn->prepare("INSERT INTO routes(" . implode(",", $keys) . ") VALUES(" . implode(", ", $insertedValues) . ") RETURNING id;");
				for ($i = 0; $i < count($keys); $i++) {
					$prepared->bindParam(":" . $keys[$i], $parameters[$keys[$i]]);
				}

				if ($prepared->execute()) {
					$result = $prepared->fetch();

					$response = $response->withHeader('Content-type', 'application/json');
					$body = $response->getBody();
					$body->write(json_encode(array(
						"id" => $result["id"]
					)));
				} else {
					ob_start();
					$prepared->debugDumpParams();
					$statement = ob_get_clean();

					$logger->error("Failed query: " . $statement, $conn->errorInfo());
					
					$response = $response->withStatus(400);
				}
			} else {
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});


/**
 * Getting all available routes.
 */
$app->get('/routes', function ($request, $response, $args) {
    global $conn;

	$params = $request->getQueryParams();

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false) {
			$response = $response->withStatus(401);
		} else {
			if (isset($params["search_operation_id"])) {
				$prepared = $conn->prepare("SELECT id, participants, hearing_zone, ST_AsText(geodata) as geodata, date_from, date_to, hearing_zone, display_style FROM routes WHERE search_operation_id = :search_operation_id");
				$prepared->bindParam(":search_operation_id", $params["search_operation_id"], PDO::PARAM_INT);
			} else {
				$prepared = $conn->prepare("SELECT id, participants, ST_AsText(geodata) as geodata, hearing_zone, date_from, date_to, display_style FROM routes;");
			}

			$prepared->execute();

			$send = array();
			if ($prepared->rowCount() > 0) {
				$send = $prepared->fetchAll(PDO::FETCH_ASSOC);
			}

			$response = $response->withHeader('Content-type', 'application/json');
			$body = $response->getBody();
			$body->write(json_encode($send));
		}
	}

	return $response;
});


/**
 * Uploading GPX file as route.
 */
$app->post('/routes/upload', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false) {
			$response = $response->withStatus(401);
		} else {
			if (!isset($_FILES['file'])) {
				$response = $response->withStatus(400);
			} else {
				$file = $_FILES['file'];

				$parsedSuccessfully = true;
				try {
					$geometry   = geoPHP::load(file_get_contents($file["tmp_name"]), "gpx");
					$components = $geometry->getComponents();
				} catch (Exception $e) {
					$parsedSuccessfully = false;
					if ($e->getMessage() === "Cannot construct a LineString with a single point") {
						$xmlGiverDoc = new DOMDocument();
						$xmlGiverDoc->load($file["tmp_name"]);
						
						$xpath = new DOMXpath($xmlGiverDoc);
						$xpath->registerNamespace("gpx", "http://www.topografix.com/GPX/1/1");
						$elements = $xpath->query("//gpx:trkpt");

						$unifiedTrk    = $xmlGiverDoc->createElement("trk");
						$unifiedName   = $xmlGiverDoc->createElement("name", "Unified track");
						$unifiedTrkseq = $xmlGiverDoc->createElement("trkseg");

						for ($i = 0; $i < $elements->length; $i++) {
							$unifiedTrkseq->appendChild($elements->item($i)->cloneNode(true));
						}
						
						$unifiedTrk->appendChild($unifiedName);
						$unifiedTrk->appendChild($unifiedTrkseq);
						$xmlGiverDoc->documentElement->appendChild($unifiedTrk);
						
						$allTracks = $xpath->query("//gpx:trk");
						foreach($allTracks as $node) {
							if ($node->isSameNode($unifiedTrk) === false) {
								$node->parentNode->removeChild($node);
							}
						}

						$geometry   = geoPHP::load($xmlGiverDoc->saveXML(), "gpx");
						$components = $geometry->getComponents();
					}
				}

				$numberOfLineStrings   = 0;
				$multilineStringArrays = array();
				for ($i = 0; $i < count($components); $i++) {
					if ($components[$i]->geometryType() === "LineString") {
						$numberOfLineStrings++;

						$multilineStringArrays[] = str_replace("LINESTRING", "", $components[$i]->out("wkt"));
					}
				}
				
				// No linestrings detected, using points one by one
				if ($numberOfLineStrings === 0) {
					$linestringElements = [];
					
					for ($i = 0; $i < count($components); $i++) {
						if ($components[$i]->geometryType() === "Point") {
							$linestringElements[] = str_replace(")", "", str_replace("POINT (", "", $components[$i]->out("wkt")));
						}
					}
					
					$multilineStringArrays[] = "(" . implode(",", $linestringElements) . ")";
				}

				$geodata  = "MULTILINESTRING(" . implode(",", $multilineStringArrays) . ")";
				$response = $response->withHeader('Content-type', 'application/json');
				$body     = $response->getBody();
				$body->write(json_encode(array(
					"geodata" => $geodata
				)));
			}
		}
	}

	return $response;
});


/**
 * Fetch information about the specific route.
 */
$app->get('/route/{id}', function ($request, $response, $args) {
    global $conn;

	$header = $request->getHeader("Auth");
	$params = $request->getQueryParams();
	if (count($header) === 0) {
		if (isset($params["auth"])) {
			$header = array($params["auth"]);
		}
	}

	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false) {
			$response = $response->withStatus(401);
		} else {
			if (isset($args["id"])) {
				$id = intval($args["id"]);

				$prepared = $conn->prepare("SELECT id, name, ST_AsText(geodata) as geodata FROM routes WHERE id = :id;");
				$prepared->bindParam(":id", $id);
				$prepared->execute();

				$result = $prepared->fetch();
				if (isset($params["format"])) {
					if ($params["format"] === "GPX") {
						$geometry = geoPHP::load($result["geodata"], "wkt");
						$gpx = $geometry->out("gpx");

						$filename = trim($result["name"]);
						if (substr_count($filename, ".gpx") === 0) {
							$filename .= ".gpx";
						}

						$response = $response->withHeader('Content-type', 'application/gpx+xml');
						$response = $response->withHeader('Content-Description', 'File Transfer');
						$response = $response->withHeader('Expires', '0');
						$response = $response->withHeader('Content-Disposition', 'attachment; filename="' . $filename . '"');
						$response = $response->withHeader('Content-Length', strlen($gpx));
						echo $gpx;
					} else {
						$response = $response->withStatus(400);
					}
				} else {
					$response = $response->withHeader('Content-type', 'application/json');
					$body = $response->getBody();
					$body->write(json_encode($result));
				}
			} else {
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});


/**
 * Update specific route.
 */
$app->put('/route/{id}', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();
			if (count($parameters) > 0) {
				$insertedValues = array();

				$optionalParameters = array("hearing_zone", "participants", "date_from", "date_to", "display_style");
				for ($i = 0; $i < count($optionalParameters); $i++) {
					if (isset($parameters[$optionalParameters[$i]])) {
						$insertedValues[$optionalParameters[$i]] = $optionalParameters[$i] . " = :" . $optionalParameters[$i];
					}
				}

				$keys = array_keys($insertedValues);
				$prepared = $conn->prepare("UPDATE routes SET " . implode(", ", $insertedValues) . " WHERE id = :id;");
				for ($i = 0; $i < count($keys); $i++) {
					$prepared->bindParam(":" . $keys[$i], $parameters[$keys[$i]], PDO::PARAM_INT);
				}

				$prepared->bindParam(":id", $id);
				if ($prepared->execute()) {
					$body = $response->getBody();
					$body->write("OK");
				} else {
					ob_start();
					$prepared->debugDumpParams();
					$statement = ob_get_clean();

					$logger->error("Failed query: " . $statement, $conn->errorInfo());
					
					$response = $response->withStatus(500);
				};
			} else {
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});


/**
 * Delete specific route.
 */
$app->delete('/route/{id}', function ($request, $response, $args) {
    global $conn;

	$deletedId = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false) {
			$response = $response->withStatus(401);
		} else {
			$prepared = $conn->prepare("DELETE FROM routes WHERE id = :deletedId;");
			$prepared->bindParam(":deletedId", $deletedId);
			$prepared->execute();

			$body = $response->getBody();
			$body->write("OK");
		}
	}

	return $response;
});

?>
