<?php

/**
 * Create new base layer.
 */
$app->post('/baselayers', function ($request, $response, $args) {
    global $conn;
	global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();
			if (isset($parameters["name"], $parameters["tile_url"])) {
				$prepared = $conn->prepare("INSERT INTO base_layers(author_id, name, tile_url) VALUES(:author_id, :name, :tileUrl) RETURNING id;");
				$prepared->bindParam(":author_id", $userId);
				$prepared->bindParam(":name", $parameters["name"]);
				$prepared->bindParam(":tileUrl", $parameters["tile_url"]);
				if ($prepared->execute()) {
					$result = $prepared->fetch();

					$response = $response->withHeader('Content-type', 'application/json');
					$body = $response->getBody();
					$body->write(json_encode(array(
						"id" => $result["id"]
					)));
				} else {
					ob_start();
					$prepared->debugDumpParams();
					$statement = ob_get_clean();

					$logger->error("Failed query: " . $statement, $conn->errorInfo());

					$response = $response->withStatus(500);
				}
			} else {
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});


/**
 * Get all base layers.
 */
$app->get('/baselayers', function ($request, $response, $args) {
    global $conn;

	$prepared = $conn->prepare("SELECT * FROM base_layers;");
	$prepared->execute();
	$result = $prepared->fetchAll(PDO::FETCH_ASSOC);

	$response = $response->withHeader('Content-type', 'application/json');
	$body = $response->getBody();
	$body->write(json_encode($result));

	return $response;
});


/**
 * Create new base layer.
 */
$app->put('/baselayer/{id}', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();
			if (isset($parameters["name"], $parameters["tile_url"])) {
				$prepared = $conn->prepare("UPDATE base_layers SET name = :name, tile_url = :tile_url WHERE id = :userId;");
				$prepared->bindParam(":userId", $userId);
				$prepared->bindParam(":name", $parameters["name"]);
				$prepared->bindParam(":tile_url", $parameters["tile_url"]);
				if ($prepared->execute()) {
					$body = $response->getBody();
					$body->write("OK");
				} else {
					ob_start();
					$prepared->debugDumpParams();
					$statement = ob_get_clean();

					$logger->error("Failed query: " . $statement, $conn->errorInfo());

					$response = $response->withStatus(500);
				}
			} else {
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});


/**
 * Delete specific base layer.
 */
$app->delete('/baselayer/{id}', function ($request, $response, $args) {
    global $conn;

	$deletedId = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false) {
			$response = $response->withStatus(401);
		} else {
			$prepared = $conn->prepare("DELETE FROM base_layers WHERE id = :deletedId;");
			$prepared->bindParam(":deletedId", $deletedId);
			$prepared->execute();

			$body = $response->getBody();
			$body->write("OK");
		}
	}

	return $response;
});

?>
