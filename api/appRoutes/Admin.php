<?php

/**
 * Create user.
 */
$app->post('/admin/users', function ($request, $response, $args) {
	global $config;
	global $conn;
	global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$userIsAdmin = getUserIsAdmin($conn, $userId);
			if ($userIsAdmin) {
				$parameters = $request->getParsedBody();
				
				if (isset($parameters["name"], $parameters["email"], $parameters["password"])) {
					if (isset($parameters["is_admin"]) === false) {
						$parameters["is_admin"] = "false";
					} else {
						$parameters["is_admin"] = "true";
					}

					$prepared = $conn->prepare("INSERT INTO users(name, email, password, is_admin) VALUES(:name, :email, :password, :is_admin);");
					$prepared->bindParam(":name", $parameters["name"]);
					$prepared->bindParam(":email", $parameters["email"]);
					$prepared->bindParam(":password", md5($config["security"]["salt"] . $parameters["password"]));
					$prepared->bindParam(":is_admin", $parameters["is_admin"]);
					
					if ($prepared->execute()) {
						$body = $response->getBody();
						$body->write("OK");
					} else {
						ob_start();
						$prepared->debugDumpParams();
						$statement = ob_get_clean();

						$logger->error("Failed query: " . $statement, $conn->errorInfo());

						$response = $response->withStatus(500);
					}
				} else {
					$response = $response->withStatus(400);
				}
			} else {				
				$response = $response->withStatus(403);
			}
		}
	}

	return $response;
});


/**
 * Update user.
 */
$app->put('/admin/users/{id}', function ($request, $response, $args) {
	global $config;
	global $conn;
	global $logger;

	$updatedId = intval($args["id"]);
	
	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$userIsAdmin = getUserIsAdmin($conn, $userId);
			if ($userIsAdmin) {
				$parameters = $request->getParsedBody();

				if (isset($parameters["name"], $parameters["email"])) {
					if (isset($parameters["is_admin"]) === false || $parameters["is_admin"] === false) {
						$parameters["is_admin"] = "false";
					} else {
						$parameters["is_admin"] = "true";
					}

					$prepared = $conn->prepare("UPDATE users SET name = :name, email = :email, is_admin = :is_admin WHERE id = " . $updatedId . ";");
					$prepared->bindParam(":name", $parameters["name"]);
					$prepared->bindParam(":email", $parameters["email"]);
					$prepared->bindParam(":is_admin", $parameters["is_admin"]);

					if ($prepared->execute()) {
						if (empty($parameters["password"]) === false) {
							$prepared = $conn->prepare("UPDATE users SET password = '" . md5($config["security"]["salt"] . $parameters["password"]) . "' WHERE id = " . $updatedId . ";");
							if ($prepared->execute()) {
								$body = $response->getBody();
								$body->write("OK");
							} else {
								ob_start();
								$prepared->debugDumpParams();
								$statement = ob_get_clean();

								$logger->error("Failed query: " . $statement, $conn->errorInfo());

								$response = $response->withStatus(500);
							}
						} else {
							$body = $response->getBody();
							$body->write("OK");
						}
					} else {
						ob_start();
						$prepared->debugDumpParams();
						$statement = ob_get_clean();

						$logger->error("Failed query: " . $statement, $conn->errorInfo());

						$response = $response->withStatus(500);
					}
				} else {
					$response = $response->withStatus(400);
				}
			} else {				
				$response = $response->withStatus(403);
			}
		}
	}

	return $response;
});



/**
 * List users.
 */
$app->get('/admin/users', function ($request, $response, $args) {
	global $conn;
	global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$userIsAdmin = getUserIsAdmin($conn, $userId);
			if ($userIsAdmin) {
				$prepared = $conn->prepare("SELECT * FROM users WHERE enabled = true;");
				$prepared->execute();
				$result = $prepared->fetchAll(PDO::FETCH_ASSOC);

				$response = $response->withHeader('Content-type', 'application/json');
				$body = $response->getBody();
				$body->write(json_encode($result));
			} else {				
				$response = $response->withStatus(403);
			}
		}
	}

	return $response;
});


/**
 * Delete user.
 */
$app->delete('/admin/users/{id}', function ($request, $response, $args) {
	global $conn;
	global $logger;

	$deletedId = intval($args["id"]);
	
	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$userIsAdmin = getUserIsAdmin($conn, $userId);
			if ($userIsAdmin) {

				$prepared = $conn->prepare("UPDATE users SET enabled = false WHERE id = " . $deletedId . ";");
				if ($prepared->execute()) {
						$body = $response->getBody();
						$body->write("OK");
				} else {
					ob_start();
					$prepared->debugDumpParams();
					$statement = ob_get_clean();

					$logger->error("Failed query: " . $statement, $conn->errorInfo());

					$response = $response->withStatus(500);
				}
			} else {				
				$response = $response->withStatus(403);
			}
		}
	}

	return $response;
});

?>
