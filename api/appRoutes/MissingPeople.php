<?php

/**
 * Create new missing person.
 */
$app->post('/missing', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();
			if (isset($parameters["search_operation_id"], $parameters["name"], $parameters["birth_year"], $parameters["sex"])) {
				$prepared = $conn->prepare("INSERT INTO missing_people(author_id, search_operation_id, name, birth_year, sex) VALUES(?, ?, ?, ?, ?) RETURNING id;");
				$prepared->bindParam(1, $userId);
				$prepared->bindParam(2, $parameters["search_operation_id"], PDO::PARAM_INT);
				$prepared->bindParam(3, $parameters["name"]);
				$prepared->bindParam(4, $parameters["birth_year"], PDO::PARAM_INT);
				$prepared->bindParam(5, $parameters["sex"], PDO::PARAM_INT);
				
				if ($prepared->execute()) {
					$result = $prepared->fetch();

					if (isset($result["id"])) {
						$response = $response->withHeader('Content-type', 'application/json');
						$body = $response->getBody();
						$body->write(json_encode(array(
							"id" => $result["id"]
						)));
					} else {
						$response = $response->withStatus(400);
					}
				} else {
					ob_start();
					$prepared->debugDumpParams();
					$statement = ob_get_clean();

					$logger->error("Failed query: " . $statement, $conn->errorInfo());
					
					$response = $response->withStatus(400);
				}

			} else {
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});


/**
 * Update specific missing person.
 */
$app->put('/missing/{id}', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();

			$updatedValues = array();

			$optionalParameters = array("name", "birth_year", "sex");

			for ($i = 0; $i < count($optionalParameters); $i++) {
				if (isset($parameters[$optionalParameters[$i]])) {
					$updatedValues[$optionalParameters[$i]] = " " . $optionalParameters[$i] . " = :" . $optionalParameters[$i];
				}
			}

			$keys = array_keys($updatedValues);
			$prepared = $conn->prepare("UPDATE missing_people SET " . implode(", ", $updatedValues) . " WHERE id = :id;");
			for ($i = 0; $i < count($keys); $i++) {
				$prepared->bindParam(":" . $keys[$i], $parameters[$keys[$i]]);
			}

			$prepared->bindParam(":id", $id);
			if ($prepared->execute()) {
				$body = $response->getBody();
				$body->write("OK");
			} else {
				ob_start();
				$prepared->debugDumpParams();
				$statement = ob_get_clean();

				$logger->error("Failed query: " . $statement, $conn->errorInfo());
				
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});


/**
 * Get missing people.
 */
$app->get('/missing', function ($request, $response, $args) {
    global $conn;

	$params = $request->getQueryParams();

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			if (isset($params["search_operation_id"])) {
				$prepared = $conn->prepare("SELECT * FROM missing_people WHERE search_operation_id = :search_operation_id");
				$prepared->bindParam(":search_operation_id", $params["search_operation_id"], PDO::PARAM_INT);
			} else {
				$prepared = $conn->prepare("SELECT * FROM search_operations");
			}

			$prepared->execute();
			$send = array();
			if ($prepared->rowCount() > 0) {
				$send = $prepared->fetchAll(PDO::FETCH_ASSOC);
			}

			$response = $response->withHeader('Content-type', 'application/json');
			$body = $response->getBody();
			$body->write(json_encode($send));
		}
	}

	return $response;
});


/**
 * Delete specific missing person.
 */
$app->delete('/missing/{id}', function ($request, $response, $args) {
    global $conn;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$prepared = $conn->prepare("DELETE FROM missing_people WHERE id = :id;");
			$prepared->bindParam(":id", $id);
			$result = $prepared->execute();

			if ($result) {
				$body = $response->getBody();
				$body->write("OK");
			} else {
				$response = $response->withStatus(500);
			}
		}
	}

	return $response;
});


?>
