<?php

/**
 * Authorization by username (email) and password.
 */
$app->post('/auth', function ($request, $response, $args) {
    global $config;
    global $conn;
	global $logger;

	$parameters = $request->getParsedBody();
	if (isset($parameters["email"], $parameters["password"])) {
		$email = filter_var($parameters["email"], FILTER_SANITIZE_EMAIL);

		$salt = $config["security"]["salt"];
		$saltedPassword = md5($salt . $parameters["password"]);

		$result = $conn->query("SELECT * FROM users WHERE password = '" . $saltedPassword . "' AND email = '" . $email . "';")->fetch();
		if ($result === false) {
			$logger->info("Attempt to log in with " . $parameters["email"] . ":" . $parameters["password"]);
			
			$response = $response->withStatus(401);
		} else {
			$generatedToken = md5(uniqid(rand() . microtime(), true));
			$conn->query("INSERT INTO tokens (user_id, token) VALUES (" . $result["id"] . ", '" . $generatedToken . "')");

			$authorizationResponse = getAuthorizationResponse($result["id"]);
			$authorizationResponse["token"] = $generatedToken;

			$response = $response->withHeader('Content-type', 'application/json');
			$body = $response->getBody();
			$body->write(json_encode($authorizationResponse));
		}
	} else {
		$response = $response->withStatus(400);
	}

	return $response;
});


/**
 * Application settings.
 */
$app->get('/publicsettings', function ($request, $response, $args) {
    global $config;

	$response = $response->withHeader('Content-type', 'application/json');
	$body = $response->getBody();
	$body->write(json_encode($config["publicsettings"]));
});


/**
 * Authorization by username (email) and password.
 */
$app->post('/auth-by-token', function ($request, $response, $args) {
    global $config;
    global $conn;
	global $logger;

	$parameters = $request->getParsedBody();
	if (isset($parameters["token"])) {
		$prepared = $conn->prepare("SELECT user_id FROM tokens WHERE token = :token");
		$prepared->bindParam(":token", $parameters["token"]);
		$prepared->execute();

		$data = $prepared->fetch();
		if ($data === false) {
			$logger->info("Attempt to log in with token " . $parameters["token"]);

			$response = $response->withStatus(403);
		} else {
			$authorizationResponse = getAuthorizationResponse($data["user_id"]);
			$authorizationResponse["token"] = $parameters["token"];

			$response = $response->withHeader('Content-type', 'application/json');
			$body = $response->getBody();
			$body->write(json_encode($authorizationResponse));
		}
	} else {
		$response = $response->withStatus(400);
	}

	return $response;
});

?>
