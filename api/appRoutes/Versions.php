<?php

/**
 * Restrieve all versions.
 */
$app->get('/versions', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$body = $response->getBody();

	$params = $request->getQueryParams();
	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else if (isset($params["search_operation_id"]) === false) {
			$response = $response->withStatus(400);
		} else {
			$preparedVersions = $conn->prepare("SELECT * FROM versions WHERE search_operation_id = :id");
			$preparedVersions->bindParam(":id", $params["search_operation_id"]);
			if ($preparedVersions->execute()) {
				$versions = $preparedVersions->fetchAll(PDO::FETCH_ASSOC);
				if (count($versions) === 0) {
					$response = $response->withHeader('Content-type', 'application/json');
					$body->write(json_encode(array()));
				} else {
					$versionIds = array();
					for ($i = 0; $i < count($versions); $i++) {
						$versionIds[] = $versions[$i]["id"];
					}

					$preparedAreas = $conn->prepare("SELECT id, probability, ST_AsText(geodata) as geodata, description, version_id FROM version_areas WHERE version_id IN (" . implode(", ", $versionIds) . ") ORDER BY version_id ASC, probability DESC;");
					if ($preparedAreas->execute()) {
						$areas = $preparedAreas->fetchAll(PDO::FETCH_ASSOC);
						for ($v = 0; $v < count($versions); $v++) {
							$versions[$v]["areas"] = array();
							for ($a = 0; $a < count($areas); $a++) {
								if ($areas[$a]["version_id"] === $versions[$v]["id"]) {
									$versions[$v]["areas"][] = $areas[$a];
								}
							}
						}

						$response = $response->withHeader('Content-type', 	'application/json');
						$body->write(json_encode($versions));
					} else {
						ob_start();
						$preparedAreas->debugDumpParams();
						$statement = ob_get_clean();

						$logger->error("Failed query: " . $statement, $conn->errorInfo());
							
						$response = $response->withStatus(500);
					}
				}
			} else {
				$response = $response->withStatus(500);
			}
		}
	}

	return $response;
});


/**
 * Create new version.
 */
$app->post('/versions', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();

			$areasAreValid = true;
			if (isset($parameters["areas"])) {
				for ($i = 0; $i < count($parameters["areas"]); $i++) {
					if (isset($parameters["areas"][$i]["probability"], $parameters["areas"][$i]["geodata"]) === false) {
						$areasAreValid = false;
						break;
					}
				}
			}

			if ($areasAreValid && isset($parameters["name"], $parameters["search_operation_id"], $parameters["probability"])) {
				$insertedValues = array();
				$insertedValues["name"] = ":name";
				$insertedValues["search_operation_id"] = ":search_operation_id";
				$insertedValues["probability"] = ":probability";

				$optionalParameters = array("description", "display_style");
				for ($i = 0; $i < count($optionalParameters); $i++) {
					if (isset($parameters[$optionalParameters[$i]])) {
						$insertedValues[$optionalParameters[$i]] = ":" . $optionalParameters[$i];
					}
				}

				$insertedValues["author_id"] = ":author_id";
				$parameters["author_id"] = $userId;

				$conn->beginTransaction();
				$keys = array_keys($insertedValues);
				$prepared = $conn->prepare("INSERT INTO versions(" . implode(",", $keys) . ") VALUES(" . implode(", ", $insertedValues) . ") RETURNING id;");
				for ($i = 0; $i < count($keys); $i++) {
					$prepared->bindParam(":" . $keys[$i], $parameters[$keys[$i]]);
				}

				if ($prepared->execute()) {
					$result = $prepared->fetch();
					if (isset($result["id"])) {
						$preparedAreas = $conn->prepare("DELETE FROM version_areas WHERE version_id = :id");
						$preparedAreas->bindParam(":id", $result["id"]);
						$preparedAreas->execute();

						$areasInserted = true;
						for ($i = 0; $i < count($parameters["areas"]); $i++) {
							$columns = array("description", "probability", "geodata");

							$insertedValues = array();
							for ($j = 0; $j < count($columns); $j++) {
								if (isset($parameters["areas"][$i][$columns[$j]])) {
									if ($columns[$j] === "geodata") {
										$insertedValues[$columns[$j]] = "ST_GeomFromText(:geodata)";
									} else {
										$insertedValues[$columns[$j]] = ":" . $columns[$j];
									}
								}
							}

							$insertedValues["version_id"] = ":version_id";
							$insertedValues["author_id"] = ":author_id";

							$keys = array_keys($insertedValues);
							$preparedAreas = $conn->prepare("INSERT INTO version_areas(" . implode(",", $keys) . ") VALUES(" . implode(", ", $insertedValues) . ");");
							for ($j = 0; $j < count($keys); $j++) {
								if (in_array($keys[$j], $columns) === true) {
									$preparedAreas->bindParam(":" . $keys[$j], $parameters["areas"][$i][$keys[$j]]);
								}
							}

							$preparedAreas->bindParam(":version_id", $result["id"]);
							$preparedAreas->bindParam(":author_id", $userId);
							if ($preparedAreas->execute() === false) {
								$areasInserted = false;
								
								ob_start();
								$preparedAreas->debugDumpParams();
								$statement = ob_get_clean();

								$logger->error("Failed query: " . $statement, $conn->errorInfo());
								
								break;
							}
						}

						if ($areasInserted) {
							$conn->commit();

							$response = $response->withHeader('Content-type', 'application/json');
							$body = $response->getBody();
							$body->write(json_encode(array(
								"id" => $result["id"]
							)));
						} else {
							$conn->rollBack();
							$response = $response->withStatus(400);
						}
					} else {
						$conn->rollBack();						
						$response = $response->withStatus(500);
					}
				} else {
					$conn->rollBack();
					
					ob_start();
					$preparedAreas->debugDumpParams();
					$statement = ob_get_clean();

					$logger->error("Failed query: " . $statement, $conn->errorInfo());

					$response = $response->withStatus(500);
				}
			} else {
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});


/**
 * Update specific version.
 */
$app->put('/versions/{id}', function ($request, $response, $args) {
    global $conn;
    global $logger;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$parameters = $request->getParsedBody();

			$areasAreValid = true;
			if (isset($parameters["areas"])) {
				for ($i = 0; $i < count($parameters["areas"]); $i++) {
					if (isset($parameters["areas"][$i]["probability"], $parameters["areas"][$i]["geodata"]) === false) {
						$areasAreValid = false;
						break;
					}
				}
			}

			if ($areasAreValid) {
				$conn->beginTransaction();
				
				$insertedValues = array();
				$optionalParameters = array("description", "name", "probability", "display_style");
				for ($i = 0; $i < count($optionalParameters); $i++) {
					if (isset($parameters[$optionalParameters[$i]])) {
						$insertedValues[$optionalParameters[$i]] = $optionalParameters[$i] . " = :" . $optionalParameters[$i];
					}
				}

				$keys = array_keys($insertedValues);
				
				$result = true;
				if (count($keys) > 0) {
					$prepared = $conn->prepare("UPDATE versions SET " . implode(", ", $insertedValues) . " WHERE id = :id;");
					for ($i = 0; $i < count($keys); $i++) {
						$prepared->bindParam(":" . $keys[$i], $parameters[$keys[$i]]);
					}

					$prepared->bindParam(":id", $id);
					$result = $prepared->execute();
				}

				if ($result) {
					$preparedAreas = $conn->prepare("DELETE FROM version_areas WHERE version_id = :id");
					$preparedAreas->bindParam(":id", $id);
					$preparedAreas->execute();

					$areasInserted = true;
					for ($i = 0; $i < count($parameters["areas"]); $i++) {
						$columns = array("description", "probability", "geodata");

						$insertedValues = array();
						for ($j = 0; $j < count($columns); $j++) {
							if (isset($parameters["areas"][$i][$columns[$j]])) {
								if ($columns[$j] === "geodata") {
									$insertedValues[$columns[$j]] = "ST_GeomFromText(:geodata)";
								} else {
									$insertedValues[$columns[$j]] = ":" . $columns[$j];
								}
							}
						}

						$insertedValues["version_id"] = ":version_id";
						$insertedValues["author_id"] = ":author_id";

						$keys = array_keys($insertedValues);
						$preparedAreas = $conn->prepare("INSERT INTO version_areas(" . implode(",", $keys) . ") VALUES(" . implode(", ", $insertedValues) . ");");
						for ($j = 0; $j < count($keys); $j++) {
							if (isset($parameters["areas"][$i][$keys[$j]])) {
								$preparedAreas->bindParam(":" . $keys[$j], $parameters["areas"][$i][$keys[$j]]);
							}
						}

						$preparedAreas->bindParam(":version_id", $id);
						$preparedAreas->bindParam(":author_id", $userId);

						if ($preparedAreas->execute() === false) {
							$areasInserted = false;
							
							ob_start();
							$preparedAreas->debugDumpParams();
							$statement = ob_get_clean();

							$logger->error("Failed query: " . $statement, $conn->errorInfo());
							
							break;
						}
					}

					if ($areasInserted) {
						$conn->commit();

						$body = $response->getBody();
						$body->write("OK");
					} else {
						$conn->rollBack();

						$response = $response->withStatus(400);
					}
				} else {
					$conn->rollBack();
					
					ob_start();
					$prepared->debugDumpParams();
					$statement = ob_get_clean();

					$logger->error("Failed query: " . $statement, $conn->errorInfo());

					$response = $response->withStatus(500);
				}
			} else {
				$response = $response->withStatus(400);
			}
		}
	}

	return $response;
});



/**
 * Delete specific version.
 */
$app->delete('/versions/{id}', function ($request, $response, $args) {
    global $conn;

	$id = intval($args["id"]);

	$header = $request->getHeader("Auth");
	if (count($header) === 0) {
		$response = $response->withStatus(401);
	} else {
		$userId = getUserId($conn, array_pop($header));
		if ($userId === false || $userId === null) {
			$response = $response->withStatus(401);
		} else {
			$conn->beginTransaction();

			$prepared = $conn->prepare("DELETE FROM version_areas WHERE version_id = :version_id");
			$prepared->bindParam(":version_id", $id);
			if ($prepared->execute()) {
				$prepared = $conn->prepare("DELETE FROM versions WHERE id = :id");
				$prepared->bindParam(":id", $id);

				if ($prepared->execute()) {
					$conn->commit();

					$body = $response->getBody();
					$body->write("OK");
				} else {
					$conn->rollBack();
					$response = $response->withStatus(500);
				}
			} else {
				$conn->rollBack();
				$response = $response->withStatus(500);
			}
		}
	}

	return $response;
});


?>
