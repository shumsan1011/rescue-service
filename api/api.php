<?php

ini_set('xdebug.var_display_max_depth', 16);
ini_set('xdebug.var_display_max_children', 1024);
ini_set('xdebug.var_display_max_data', 4096);

date_default_timezone_set("Europe/Moscow");

require_once __DIR__ . "/../config/config.php";
require_once "vendor/autoload.php";

$logger = new Katzgrau\KLogger\Logger(__DIR__ . "/logs");

$slimConfiguration = new \Slim\Container(array(
    "settings" => array(
        "displayErrorDetails" => true,
    )
));
$app = new \Slim\App($slimConfiguration);

$conn = new PDO($config["database"]["connectionString"]);

/**
 * Retrieves userId using provided token.
 *
 * @param PDO    $conn      Database connection
 * @param string $authToken Token
 *
 * @return mixed False if no token exists or integer user identifier
 */
function getUserId($conn, $authToken) {
	$prepared = $conn->prepare("SELECT * FROM tokens WHERE token = :token");
	$prepared->bindParam(":token", $authToken);
	$prepared->execute();

	$result = $prepared->fetch();
	if (count($result) === 0) {
		return false;
	} else {
		return $result["user_id"];
	}
} //end getUserId()


function getUserIsAdmin($conn, $id) {
	$prepared = $conn->prepare("SELECT is_admin FROM users WHERE id = :id");
	$prepared->bindParam(":id", $id);
	$prepared->execute();

	$result = $prepared->fetch();
	if ($result["is_admin"]) {
		return true;
	} else {
		return false;
	}
} //end getUserIsAdmin()


function getAuthorizationResponse($userId) {
	global $conn;

	$response = array();

	$prepared = $conn->prepare("SELECT * FROM users WHERE id = :userId");
	$prepared->bindParam(":userId", $userId);
	$prepared->execute();

	$userData = $prepared->fetch();

	$response["id"] = $userId;
	$response["name"] = $userData["name"];
	$response["isAdmin"] = $userData["is_admin"];

	return $response;
} //end getAuthorizationResponse()


require("appRoutes/Auth.php");
require("appRoutes/Admin.php");
require("appRoutes/Routes.php");
require("appRoutes/SpecialMarkup.php");
require("appRoutes/SpecialMarkupTemplates.php");
require("appRoutes/SettingsTemplates.php");
require("appRoutes/BaseLayers.php");
require("appRoutes/SearchOperations.php");
require("appRoutes/MissingPeople.php");
require("appRoutes/Versions.php");

$app->run();

?>