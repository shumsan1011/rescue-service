var fs = require("fs");
var sc = require("./lib/SpreadCalculation");

if (!Date.now) { Date.now = function() { return new Date().getTime(); }}

var cleanedStages;
if (!process.argv[2]) {
	cleanedStages = JSON.parse('{"meters_in_pixel":"7","start_point":{"lat":"59.8027","lng":"30.7816"},"time_periods":[{"time_from":"2016-06-26T05:10:00+08:00","time_to":"2016-06-26T06:00:00+08:00","speed":"5"},{"time_from":"2016-06-26T06:00:00+08:00","time_to":"2016-06-26T22:00:00+08:00","speed":"5"},{"time_from":"2016-06-26T22:00:00+08:00","time_to":"2016-06-27T00:00:00+08:00","speed":"5"},{"time_from":"2016-06-27T00:00:00+08:00","time_to":"2016-06-27T05:00:00+08:00","speed":"5"},{"time_from":"2016-06-27T05:00:00+08:00","time_to":"2016-06-27T06:00:00+08:00","speed":"5"},{"time_from":"2016-06-27T06:00:00+08:00","time_to":"2016-06-27T08:16:16+08:00","speed":"5"}],"objects":[{"type":"polygon","geodata":{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[30.713664293289185,59.84446992580006],[30.67109227180481,59.8092678885682],[30.73975682258606,59.7916529165298],[30.821467638015747,59.84998843915201],[30.74524998664856,59.86240174913176],[30.713664293289185,59.84446992580006]]]}},"speed_modifier":"0","name":"0.5"},{"type":"polyline","geodata":{"type":"Feature","geometry":{"type":"LineString","coordinates":[["30.848246812820438","59.862746497190884"],["30.90729832649231","59.822386721075624"],[30.910731554031376,59.77748511245075],[30.86747288703918,59.74878552830563],[30.78164219856262,59.732176920272785]]}},"speed_modifier":"0","name":"0"}]}');
} else {
	cleanedStages = JSON.parse(process.argv[2].replace(/'/g, '"'));
}

var folder = "./log/" + Date.now();
fs.mkdirSync(folder);
fs.writeFileSync(folder + "/input.json", JSON.stringify(cleanedStages));

var spread = new sc.SpreadCalculation();
var result = spread.compute(cleanedStages);

fs.writeFileSync(folder + "/output.json", JSON.stringify(result));

console.log(folder + "/output.json");