var rsLeafletTranslations = {
	en: {
		draw: {
			toolbar: {
				actions: {
					title: 'Cancel drawing',
					text: 'Cancel'
				},
				undo: {
					title: 'Delete last point drawn',
					text: 'Delete last point'
				},
				buttons: {
					polyline: 'Draw a polyline',
					polygon: 'Draw a polygon',
					rectangle: 'Draw a rectangle',
					circle: 'Draw a circle',
					marker: 'Draw a marker'
				}
			},
			handlers: {
				circle: {
					tooltip: {
						start: 'Click and drag to draw circle.'
					},
					radius: 'Radius'
				},
				marker: {
					tooltip: {
						start: 'Click map to place marker.'
					}
				},
				polygon: {
					tooltip: {
						start: 'Click to start drawing shape.',
						cont: 'Click to continue drawing shape.',
						end: 'Click first point to close this shape.'
					}
				},
				polyline: {
					error: '<strong>Error:</strong> shape edges cannot cross!',
					tooltip: {
						start: 'Click to start drawing line.',
						cont: 'Click to continue drawing line.',
						end: 'Click last point to finish line.'
					}
				},
				rectangle: {
					tooltip: {
						start: 'Click and drag to draw rectangle.'
					}
				},
				simpleshape: {
					tooltip: {
						end: 'Release mouse to finish drawing.'
					}
				}
			}
		},
		edit: {
			toolbar: {
				actions: {
					save: {
						title: 'Save changes.',
						text: 'Save'
					},
					cancel: {
						title: 'Cancel editing, discards all changes.',
						text: 'Cancel'
					}
				},
				buttons: {
					edit: 'Edit layers.',
					editDisabled: 'No layers to edit.',
					remove: 'Delete layers.',
					removeDisabled: 'No layers to delete.'
				}
			},
			handlers: {
				edit: {
					tooltip: {
						text: 'Drag handles, or marker to edit feature.',
						subtext: 'Click cancel to undo changes.'
					}
				},
				remove: {
					tooltip: {
						text: 'Click on a feature to remove'
					}
				}
			}
		}
	},
	ru: {
		draw: {
			toolbar: {
				actions: {
					title: 'Отменить рисование',
					text: 'Отмена'
				},
				undo: {
					title: 'Удалить последнюю нарисованную точку',
					text: 'Удалить нарисованную точку'
				},
				buttons: {
					polyline: 'Нарисовать полилинию',
					polygon: 'Нарисовать полигон',
					rectangle: 'Нарисовать квадрат',
					circle: 'Нарисовать круг',
					marker: 'Нарисовать маркер'
				}
			},
			handlers: {
				circle: {
					tooltip: {
						start: 'Нажмите и тяните для рисования окружности.'
					},
					radius: 'Радиус'
				},
				marker: {
					tooltip: {
						start: 'Нажмите на карте для установки маркера.'
					}
				},
				polygon: {
					tooltip: {
						start: 'Нажмите для начала рисования фигуры.',
						cont: 'Нажмите для продолжения рисования фигуры.',
						end: 'Нажмите первую точку для завершения рисования фигуры.'
					}
				},
				polyline: {
					error: '<strong>Ошибка:</strong> границы фигуры не могут пересекаться!',
					tooltip: {
						start: 'Нажмите для рисования линии.',
						cont: 'Нажмите для продолжения рисования линии.',
						end: 'Нажмите для завершения рисования линии.'
					}
				},
				rectangle: {
					tooltip: {
						start: 'Нажмите и тяните для рисования квадрата.'
					}
				},
				simpleshape: {
					tooltip: {
						end: 'Отпустите мышь для завершения рисования.'
					}
				}
			}
		},
		edit: {
			toolbar: {
				actions: {
					save: {
						title: 'Сохранить изменения.',
						text: 'Сохранить'
					},
					cancel: {
						title: 'Отменить редактирование, сбросить все изменения.',
						text: 'Отмена'
					}
				},
				buttons: {
					edit: 'Редактировать слои.',
					editDisabled: 'Нет слоев для редактирования.',
					remove: 'Удалить слои.',
					removeDisabled: 'Нет слоев для удаления.'
				}
			},
			handlers: {
				edit: {
					tooltip: {
						text: 'Тяните указатели или маркер для редактирования элемента.',
						subtext: 'Нажмите Отмена для сброса изменений.'
					}
				},
				remove: {
					tooltip: {
						text: 'Нажмите на элементе для удаления'
					}
				}
			}
		}
	}
};


L.drawLocal = rsLeafletTranslations.en;