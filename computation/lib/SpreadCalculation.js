var moment = require("moment");
var _      = require("underscore");
var turf   = require("./turf");
var LatLon = require("./geodesyTools");

/**
 *
 * @param Number settings.period_length Length of the period in seconds
 *
 */
var SpreadCalculation = function(settings) {
    var _periodLength = 120;
    if (settings && settings.period_length) _periodLength = parseInt(settings.period_length);
    var _maximumDistanceBetweenPoints = 80; // pixels
    var _minimumDistanceBetweenPoints = 20; // pixels
    var _maxDistanceFromCenter = 150000; // meters
	var _numberOfFirstRoundPoints = 8; // initial spread
	var _periodIndexToStartPlacingMiddlePoints = 10; // 10 by default
	var _pointsLimit = 1000;
    var _metersInPixels = 7;
    
    var _objects = [];
    var _timePeriods = []; 
    
	var _getInt = function (p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y) {
		var s1_x, s1_y, s2_x, s2_y;
		s1_x = p1_x - p0_x;
		s1_y = p1_y - p0_y;
		s2_x = p3_x - p2_x;
		s2_y = p3_y - p2_y;

		var s, t;
		s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
		t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

		if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
		{
			// Collision detected
			return true;
		}

		return false; // No collision
	};
    
    
    var _getPeriods = function() {
        var startTime = _timePeriods[0].time_from;
        var endTime = _timePeriods[(_timePeriods.length - 1)].time_to;
        var numberOfPeriods = Math.ceil((endTime - startTime) / _periodLength);
        
        var _periods = [];
        for (var i = 0; i < numberOfPeriods; i++) {
            var periodStartTime = startTime + i * _periodLength;
            var periodEndTime = startTime + (i + 1) * _periodLength;

            var matchingPeriodsIndexes = [];
            for (var j = 0; j < _timePeriods.length; j++) {
                if (periodStartTime >= _timePeriods[j].time_from && periodStartTime < _timePeriods[j].time_to ||
                    periodEndTime > _timePeriods[j].time_from && periodEndTime <= _timePeriods[j].time_to) {
                    matchingPeriodsIndexes.push(j);
                }
            }

            matchingPeriodsIndexes = _.uniq(matchingPeriodsIndexes);
            var matchingPeriods = [];
            for (var j = 0; j < _timePeriods.length; j++) {
                if (matchingPeriodsIndexes.indexOf(j) !== -1) {
                    matchingPeriods.push(_timePeriods[j]);
                }
            }

            var speed = false;
            if (matchingPeriods.length === 1) {
                speed = matchingPeriods[0].speed;
            } else {
                var intervals = [];

                for (var j = 0; j < matchingPeriods.length; j++) {
                    if (periodStartTime < matchingPeriods[j].time_from) {
                        if (periodEndTime <= matchingPeriods[j].time_to) {
                            intervals.push({
                                speed: matchingPeriods[j].speed,
                                duration: (periodEndTime - matchingPeriods[j].time_from)
                            });
                        } else if (periodEndTime > matchingPeriods[j].time_to) {
                            intervals.push({
                                speed: matchingPeriods[j].speed,
                                duration: (matchingPeriods[j].time_to - matchingPeriods[j].time_from)
                            });
                        }
                    } else if (periodStartTime === matchingPeriods[j].time_from) {
                        if (periodEndTime > matchingPeriods[j].time_to) {
                            intervals.push({
                                speed: matchingPeriods[j].speed,
                                duration: (matchingPeriods[j].time_to - matchingPeriods[j].time_from)
                            });
                        }
                    } else if (periodStartTime > matchingPeriods[j].time_from) {
                        if (periodEndTime > matchingPeriods[j].time_to) {
                            intervals.push({
                                speed: matchingPeriods[j].speed,
                                duration: (matchingPeriods[j].time_to - periodStartTime)
                            });
                        }
                    }
                }

                var lengthCollected = 0;
                var speedCollected = 0;
                for (var j = 0; j < intervals.length; j++) {
                    speedCollected += (intervals[j].speed * intervals[j].duration);
                    lengthCollected += intervals[j].duration;
                }
                
                speed = Math.round(speedCollected / lengthCollected);
            }

            _periods.push({
                time_from: periodStartTime,
                time_to: periodEndTime,
                speed: speed
            });
        }

        return _periods;
    };
    
    
	var _inside = function (lat, lon, poly) {
		var currentPoint = new LatLon(lat, lon);
		var distance = 200;
		var split = 4;
		
		var inside = true;
		for (var i = 0; i < split; i++) {
			var point = currentPoint.destinationPoint(distance, ((360 / split) * i));
			var point = {
				"geometry": {
					"type": "Point",
					"coordinates": [point.lon, point.lat]
				}
			};
			
			if (turf.inside(point, poly) === false) {
				inside = false;
				break;
			}
		}
		
		return inside;
	}


	var _computePrechecks = function (params) {
        _timePeriods = new Array();
        if (!params.time_periods || params.time_periods.length === 0) {
            throw "Provide at least one time period";
        } else {
            _timePeriods = params.time_periods;
            for (var i = 0; i < _timePeriods.length; i++) {
                _timePeriods[i].time_from = moment(_timePeriods[i].time_from).unix();
                _timePeriods[i].time_to = moment(_timePeriods[i].time_to).unix();
            }
        }

        if (!params.start_point || !params.start_point.lat || !params.start_point.lng) {
            throw "Provide coordinates in correct form";
        } else {
            params.start_point.lat = parseFloat(params.start_point.lat);
            params.start_point.lng = parseFloat(params.start_point.lng);
        }
        
        if (params.objects) {
            for (var i = 0; i < params.objects.length; i++) {
                if (!params.objects[i].geodata || (!params.objects[i].speed_modifier && params.objects[i].speed_modifier !== 0) || !params.objects[i].name) {
                    throw "Invalid object is provided at the position " + i; 
                } else {
                    _objects.push(params.objects[i]);
                }
            }
        }
        
        if (!params.meters_in_pixel) {
            throw "Define how much meters fit in one pixel";
        } else {
            //_metersInPixels = parseInt(params.meters_in_pixel);
        }

		return params;
	};

	
	var _pointsAreTooFarFromTheBeginning = function(points, startPoint) {
		var calculationIsTooFar = false;
		var everyNPoint = Math.ceil(points.length / 20);
		for (var p = 0; p < points.length; p += everyNPoint) {
			var distance = startPoint.distanceTo(points[p]);
			if (distance > _maxDistanceFromCenter) {
				calculationIsTooFar = true;
				break;
			}
		}

		return calculationIsTooFar;
	};


    this.compute = function(params) {
		params = _computePrechecks(params);

        var periods = _getPeriods();
        if (!periods) {
            throw "Unable to generate time periods";
        }

        console.log("Total periods: " + periods.length);       
        
        var allowedMinDistance = _minimumDistanceBetweenPoints * _metersInPixels;       
        var allowedMaxDistance = _maximumDistanceBetweenPoints * _metersInPixels;
        
        var startPoint = new LatLon(params.start_point.lat, params.start_point.lng);
        
        var calculationIsTooFar = false;
        
        var resultingPolygonSet = [];
        
        var currentPolygon = {
            points: [],
            description: "Initial polygon"
        };
		
		var noMovementYet = true;
        for (var periodIndex = 0; periodIndex < periods.length; periodIndex++) {
            var movedDistance = ((periods[periodIndex].speed * 1000 / 3600) * _periodLength);
            movedDistance = +movedDistance.toFixed(1);

            var stopExploration = false;

			var points = currentPolygon.points;
			
			if (points.length > _pointsLimit) {
				console.log("Too much points: " + points.length + ", period index: " + periodIndex);
				break;
			}

			if (periodIndex === 0 || (periods[periodIndex].speed > 0 && noMovementYet)) {
				noMovementYet = false;
				var firstRoundPoints = [];
				for (var i = 0; i < _numberOfFirstRoundPoints; i++) {
					var angle = (360 / _numberOfFirstRoundPoints);
					var p2 = startPoint.destinationPoint(movedDistance, i * angle);
					p2.brng = i * angle;
					firstRoundPoints.push(p2);
				}

				points = firstRoundPoints;
			} else if (movedDistance > 0) {
				if (_pointsAreTooFarFromTheBeginning(points, startPoint)) {
					c("Calculation reached its limits, aborting");
				} else {
					var latLngArray = [];
					for (var i = 0; i < points.length; i++) {
						var previousPointIndex = i - 1;
						if (i === 0) previousPointIndex = points.length - 1;
						
						var nextPointIndex = i + 1;
						if (i === (points.length - 1)) nextPointIndex = 0;
						
						if (points[i].skip) {
							// Not moving points
							latLngArray.push([points[i].lon, points[i].lat]);
						} else if (points[previousPointIndex].skip && points[nextPointIndex].skip) {
							points[i].skip = true;
							latLngArray.push([points[i].lon, points[i].lat]);
						} else {
							// Moving points
							var localDistance = movedDistance;
							var currentPoint = {
								"geometry": {
									"type": "Point",
									"coordinates": [points[i].lon, points[i].lat]
								}
							};

							for (var k = 0; k < _objects.length; k++) {
								if (_objects[k].type === "polygon") {
									var isInside = turf.inside(currentPoint, _objects[k].geodata);
									if (isInside) {
										localDistance = (movedDistance * _objects[k].speed_modifier);
									}
								}
							}

							var p1 = points[i];
							var p2 = p1.destinationPoint(localDistance, p1.brng);
							
							var pointAdvanceCrossesLine = false;
							for (var k = 0; k < _objects.length; k++) {
								if (_objects[k].type === "polyline") {
									for (var l = 1; l < _objects[k].geodata.geometry.coordinates.length; l++) {
										var coordinates = _objects[k].geodata.geometry.coordinates;
										var intersect = _getInt(p1.lat, p1.lon, p2.lat, p2.lon,
											coordinates[(l-1)][1], coordinates[(l-1)][0], coordinates[l][1], coordinates[l][0]);

										if (intersect) {
											pointAdvanceCrossesLine = true;
											break;
										}
									}
								}
							}

							if (pointAdvanceCrossesLine) {
								points[i].skip = true;
							} else {
								if (points[i].skip) p2.skip = true;
								
								p2.brng = p1.brng;
								points[i] = p2;
								
								latLngArray.push([p2.lon, p2.lat]);
							}
						}
					}

					// Polygon with moved points
					var temporaryPolygon = {
						"type":"Feature",
						"properties":{},
						"geometry":{
							"type":"Polygon",
							"coordinates":[latLngArray]
						}
					};

					// Replacing pairs of too close points with midpoint
					if (periodIndex > _periodIndexToStartPlacingMiddlePoints) {
						for (var i = 0; i < points.length; i++) {
							if (!points[i].skip) {
								if (i < (points.length - 1)) {
									var currentPointIndex = i;
									var nextPointIndex = i + 1;
								} else {
									var currentPointIndex = i;
									var nextPointIndex = 0;
								}

								var distance = points[currentPointIndex].distanceTo(points[nextPointIndex]);
								if (distance < allowedMinDistance) {
									var midpoint = points[currentPointIndex].midpointTo(points[nextPointIndex]);
									var lineBearing = turf.bearing({
										"geometry": {
											"type": "Point",
											"coordinates": [points[currentPointIndex].lon, points[currentPointIndex].lat]
										}
									}, {
										"geometry": {
											"type": "Point",
											"coordinates": [points[nextPointIndex].lon, points[nextPointIndex].lat]
										}
									});

									
									if (lineBearing < 0) {
										lineBearing = 360 + lineBearing;
									}

									var testDistance = 100;

									var possibleBearing1 = lineBearing + 90;
									if (possibleBearing1 > 360) possibleBearing1 = possibleBearing1 - 360;
									var possiblePoint1 = midpoint.destinationPoint(testDistance, possibleBearing1);

									var possibleBearing2 = lineBearing - 90;
									if (possibleBearing2 < 0) possibleBearing2 = 360 + possibleBearing2;
									var possiblePoint2 = midpoint.destinationPoint(testDistance, possibleBearing2);

									var bearing = false;

									var firstPointIsInside = turf.inside({
										"geometry": {
											"type": "Point",
											"coordinates": [possiblePoint1.lon, possiblePoint1.lat]
										}
									}, temporaryPolygon);
									
									var secondPointIsInside = turf.inside({
										"geometry": {
											"type": "Point",
											"coordinates": [possiblePoint2.lon, possiblePoint2.lat]
										}
									}, temporaryPolygon);
									
									if (firstPointIsInside === true && secondPointIsInside === false) {
										bearing = possibleBearing2;
									} else if (firstPointIsInside === false && secondPointIsInside === true) {
										bearing = possibleBearing1;
									} else {
										
										var firstBearing = points[currentPointIndex].brng;
										if (firstBearing === 0) firstBearing = 360;
										var secondBearing = points[nextPointIndex].brng;
										if (secondBearing === 0) secondBearing = 360;
									
										if (firstBearing < secondBearing) {
											bearing = Math.floor((firstBearing + secondBearing) / 2);
										} else {
											bearing = Math.floor((firstBearing + secondBearing + 360) / 2);
											if (bearing > 360) {
												bearing = bearing - 360;
											}

											bearing = bearing - 180;
										}
									}

									if (bearing < 0) bearing = 360 + bearing;
									if (bearing >= 360) bearing = bearing - 360;

									if (points[currentPointIndex].skip && points[nextPointIndex].skip) {
										midpoint.skip = true;
									}

									midpoint.brng = bearing;
									points.splice(i, 2, midpoint);
								}
							}
						}
					}

					for (var i = 0; i < points.length; i++) {
						if (!points[i].skip) {
							var isInside = _inside(points[i].lat, points[i].lon, temporaryPolygon);

							if (isInside) {
								if (i !== 0) points[(i - 1)].skip = true;
								
								points[i].skip = true;
								
								if (i !== (points.length - 1)) points[(i + 1)].skip = true;
							} else {
								if (i < (points.length - 1)) {
									var currentPointIndex = i;
									var nextPointIndex = i + 1;
								} else {
									var currentPointIndex = i;
									var nextPointIndex = 0;
								}

								var distance = points[currentPointIndex].distanceTo(points[nextPointIndex]);
								if (distance > allowedMaxDistance) {
									var midpoint = points[currentPointIndex].midpointTo(points[nextPointIndex]);

									var lineBearing = turf.bearing({
										"geometry": {
											"type": "Point",
											"coordinates": [points[currentPointIndex].lon, points[currentPointIndex].lat]
										}
									}, {
										"geometry": {
											"type": "Point",
											"coordinates": [points[nextPointIndex].lon, points[nextPointIndex].lat]
										}
									});

									if (lineBearing < 0) {
										lineBearing = 360 + lineBearing;
									}

									var testDistance = 100;

									var possibleBearing1 = lineBearing + 90;
									if (possibleBearing1 > 360) possibleBearing1 = possibleBearing1 - 360;
									var possiblePoint1 = midpoint.destinationPoint(testDistance, possibleBearing1);

									var possibleBearing2 = lineBearing - 90;
									if (possibleBearing2 < 0) possibleBearing2 = 360 + possibleBearing2;
									var possiblePoint2 = midpoint.destinationPoint(testDistance, possibleBearing2);

									var bearing = false;

									var firstPointIsInside = turf.inside({
										"geometry": {
											"type": "Point",
											"coordinates": [possiblePoint1.lon, possiblePoint1.lat]
										}
									}, temporaryPolygon);
									
									var secondPointIsInside = turf.inside({
										"geometry": {
											"type": "Point",
											"coordinates": [possiblePoint2.lon, possiblePoint2.lat]
										}
									}, temporaryPolygon);
									
									if (firstPointIsInside === true && secondPointIsInside === false) {
										bearing = possibleBearing2;
									} else if (firstPointIsInside === false && secondPointIsInside === true) {
										bearing = possibleBearing1;
									} else {
										var firstBearing = points[currentPointIndex].brng;
										if (firstBearing === 0) firstBearing = 360;
										var secondBearing = points[nextPointIndex].brng;
										if (secondBearing === 0) secondBearing = 360;
									
										if (firstBearing < secondBearing) {
											bearing = Math.floor((firstBearing + secondBearing) / 2);
										} else {
											bearing = Math.floor((firstBearing + secondBearing + 360) / 2);
											if (bearing > 360) {
												bearing = bearing - 360;
											}

											bearing = bearing - 180;
										}
									}

									if (bearing < 0) bearing = 360 + bearing;
									if (bearing >= 360) bearing = bearing - 360;
									
									
									if (points[currentPointIndex].skip && points[nextPointIndex].skip) {
										midpoint.skip = true;
									}
									
									midpoint.brng = bearing;
									points.splice(nextPointIndex, 0, midpoint);
								}
							}
						}
					}
				}
			}

			if (calculationIsTooFar) {
				c("Maximum distance from the center was reached (" + _maxDistanceFromCenter + "), aborting at period " + periodIndex + ".");
				break;
			}

			currentPolygon.points = points;

            if (stopExploration === false) {
                var points = [];
                for (var j = 0; j < currentPolygon.points.length; j++) {
                    points.push([currentPolygon.points[j].lon.toFixed(4), currentPolygon.points[j].lat.toFixed(4)])
                }

                var result = {
                     periodIndex: periodIndex,
                     geodata: {
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [points]
                        }
                    }
                };
                
                resultingPolygonSet.push(result);
            } else {
                break;
            }
        }

        return {
			stages: resultingPolygonSet,
			step: _periodLength
		};
    }
}

module.exports = {
  SpreadCalculation: SpreadCalculation
};