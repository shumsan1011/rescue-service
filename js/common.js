var getRandomColor = function () {
    var color;
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    color = "rgb("+r+" ,"+g+","+ b+")";
    return color;
};

var c = function (data) {
    console.log(data);
};
    
var toTimestamp = function (value) {
    return moment(value).unix();
}

var fromTimestamp = function (value) {
    return moment.unix(value).format();
}
