var Map = function () {
	var _map;
    var _drawControl;
    var _drawnItems;
    var _searchOperationCenterMarker;
    var _drawnRoutesFeatureGroup;
    var _drawnMarkupFeatureGroup;
    var _drawnAreasFeatureGroup;
    var _layerControl;
    var _pendingMapCenter = new Array();
    var _pendingBaseLayers = new Array();
    var _pendingMarkers = new Array();
	var _pendingRoutes = new Array();
	var _pendingSpecialMarkup = new Array();
	var _pendingVersions = new Array();
    var _initiated = false;
    var _center = [51.9057, 100.8888];
    var _zoom = 11;
    var _self = this;
	var _mapElementId = "map";
	var _saveButton, _getLocationButton;
	var _baseLayers = [];
	var _activeLayerControl;
	var _baseLayerHistory = [];
	var _markersFeatureGroup = new L.FeatureGroup();

	var _onMapMoveEnd = false;
	
    var _onZoomLevelChangeCallbacks = {};

    var DECIMAL_PLACES = 4;

    var _layerGroupToWKT = function (layerGroup) {
        var type = false;
        var points = new Array();

        var layers = layerGroup.getLayers();
        var multi = false;
        if (layers.length === 0) {
            throw "Empty layerGroup was provided"
        } else if (layers.length > 1) {
            multi = true;
        }

        var elements = new Array();
        for (var i = 0; i < layers.length; i++) {
            var lng, lat;
            var latlngs = layers[i].getLatLngs();

            var coords = new Array();
            for (var j = 0; j < latlngs.length; j++) {
                var coordLng = latlngs[j].lng.toFixed(DECIMAL_PLACES);
                var coordLat = latlngs[j].lat.toFixed(DECIMAL_PLACES);

                coords.push(coordLng + " " + coordLat);
                if (i === 0) {
                    lng = coordLng;
                    lat = coordLat;
                }
            }

            var representation = false;
            if (layers[i] instanceof L.Polygon) {
                representation = "((" + coords.join(",") + "," + lng + " " + lat + "))";
            } else if (layers[i] instanceof L.Polyline) {
                representation = "(" + coords.join(",") + ")";
            } else {
                representation = "(" + layer.getLatLng().lng + " " + layer.getLatLng().lat + ")";
            }

            elements.push(representation)
        }

        var WKTresult = false;
        if (multi) {
            if (layers[0] instanceof L.Polyline) {
                WKTresult = "MULTILINESTRING(" + elements.join(",") + ")";
            }
        } else {
            if (layers[0] instanceof L.Polyline) {
                WKTresult = "LINESTRING" + elements.join(",") + "";
            }
        }

        return WKTresult;
    };


    var _metersInPixel = function () {
		var metersInPixels = 40075016.686 * Math.abs(Math.cos(_map.getCenter().lat * 180/Math.PI)) / Math.pow(2, _map.getZoom()+8);
        return metersInPixels;
    };
    
	
	this.clearSpreadStages = function () {	
		if (_map) {
			if (_spreadStage !== false) {
				_map.removeLayer(_spreadStage);
			} else {
				_map.addLayer(_markersFeatureGroup);
			}

			_markersFeatureGroup.clearLayers();
		}
	};
	
	
    var _spreadStage = false;
    this.drawSpreadStage = function (spreadStage) {	
		if (_spreadStage !== false) {
            _map.removeLayer(_spreadStage);
        } else {
			_map.addLayer(_markersFeatureGroup);
        }

		_markersFeatureGroup.clearLayers();
		
		
		
if (0) {
		 var points = spreadStage.geodata.geometry.coordinates[0];
         for (var i = 0; i < points.length; i++) {
             var marker = L.marker([points[i][1], points[i][0]]);
             
             marker.bindPopup("<p>" + i + "</p>");
             _markersFeatureGroup.addLayer(marker);
         }
}
		
		
		
        _spreadStage = L.geoJson(spreadStage.geodata, {
			style: {
				fillOpacity: 0.5,
				fillColor: "#F7779D",
				stroke: false
			}
		});

        _map.addLayer(_spreadStage);
    };
    
    
    this.getMetersInPixels = function () {
        return _metersInPixel();
    };
    
    
    this.addOnZoomLevelChange = function (id, callback) {
		_onZoomLevelChangeCallbacks[id] = callback;
		if (_map) {
			_map.on("zoomend", function() {
				for (var key in _onZoomLevelChangeCallbacks) {
					_onZoomLevelChangeCallbacks[key]();
				}
			});
        }
    };
    
	
    this.initiate = function (options) {
		if (options.mapCenter) _center = [options.mapCenter.lat, options.mapCenter.lng];
		if (options.mapZoom) _zoom = options.mapZoom;
		if (options.translate) _translate = options.translate;
		
        _map = L.map(_mapElementId, {editable: true, measureControl: true}).setView(_center, _zoom);

		_map.on("moveend", function (e) {
			if (options.onMoveEnd) {
				options.onMoveEnd(e.target.getCenter(), e.target.getZoom());
			}
		});
		
        var googleLayerSatellite = new L.Google('SATELLITE');
        var googleLayerHybrid = new L.Google('HYBRID');
        var googleLayerRoadmap = new L.Google('ROADMAP');
        var osmLayer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

        var yandexLayerRegularMap = new L.Yandex("map");
        var yandexLayerSatelliteMap = new L.Yandex("satellite");
        var yandexLayerHybridMap = new L.Yandex("hybrid");
        var yandexLayerPublicMap = new L.Yandex("publicMap");
        var yandexLayerPublicMapHybrid = new L.Yandex("publicMapHybrid");

        _map.addLayer(googleLayerSatellite);

        _searchOperationCenterMarker = new L.FeatureGroup();
        _map.addLayer(_searchOperationCenterMarker);

        _drawnItems = new L.LayerGroup();
        _map.addLayer(_drawnItems);

        _drawnRoutesFeatureGroup = new L.FeatureGroup();
        _map.addLayer(_drawnRoutesFeatureGroup);

        _drawnMarkupFeatureGroup = new L.FeatureGroup();
        _map.addLayer(_drawnMarkupFeatureGroup);

        _drawnAreasFeatureGroup = new L.FeatureGroup();
        _map.addLayer(_drawnAreasFeatureGroup);

        if (_onZoomLevelChangeCallbacks.length > 0) {
            _self.addOnZoomLevelChange(_onZoomLevelChangeCallbacks);
        }

        var baseMaps = {
            "Google Satellite": googleLayerSatellite,
            "Google Hybrid": googleLayerHybrid,
            "Google Roadmap": googleLayerRoadmap,
            "OpenStreet map": osmLayer,
            "Yandex map": yandexLayerRegularMap,
            "Yandex satellite map": yandexLayerSatelliteMap,
            "Yandex hybrid map": yandexLayerHybridMap,
            "Yandex public map": yandexLayerPublicMap,
            "Yandex public hybrid map": yandexLayerPublicMapHybrid
        };

		_baseLayerHistory.push("Google Satellite");
		
        for (var i = 0; i < _pendingBaseLayers.length; i++) {
            var mapLayer = L.tileLayer(_pendingBaseLayers[i].tile_url);
            mapLayer.baseLayerId = _pendingBaseLayers[i].id;
            baseMaps[_pendingBaseLayers[i].name] = mapLayer;
        }
		
		for (var key in baseMaps) {
			_baseLayers[key] = baseMaps[key];
		}

		var overlayMaps = [];
		overlayMaps[_translate.instant("ROUTES")]         = _drawnRoutesFeatureGroup;
		overlayMaps[_translate.instant("SPECIAL_MARKUP")] = _drawnMarkupFeatureGroup;

        for (var i = 0; i < _pendingMarkers.length; i++) {
            var marker = new L.Marker(_pendingMarkers[i]).addTo(_map);
            _map.addLayer(marker);
        }
        
        if (_pendingMapCenter.length > 0) {
            _self.setSearchOperationCenter(_pendingMapCenter[0].coordinates, _pendingMapCenter[0].text);
        }
        
        L.control.scale().addTo(_map);
        
        _layerControl = L.control.layers(baseMaps, overlayMaps, {collapsed: options.collapsed}).addTo(_map);
        _initiated = true;
		
		_saveButton = L.easyButton('fa-floppy-o', function(btn, map){
			if (btn.onSave) {
				var wkt = new Wkt.Wkt();
				wkt.fromObject(btn.editedLayer);
				var wktString = wkt.write();
				btn.onSave(wktString);
			}

			_disableEditing(btn.editedLayer);
		}).addTo(_map);
		_saveButton.disable();

		if (_pendingRoutes.length > 0) {
			_self.drawRoutes(_pendingRoutes[0].routes, _pendingRoutes[0].onClickCallback, _pendingRoutes[0].onUnclickCallback);
		}

		if (_pendingSpecialMarkup.length > 0) {
			_self.drawMarkupElements(_pendingSpecialMarkup[0].elements, _pendingSpecialMarkup[0].onClickCallback, _pendingSpecialMarkup[0].onUnclickCallback);
		}
		
		var getLocationPopup = L.popup().setContent('Hello World!');
		
		_getLocationButton = L.easyButton({
			states: [{
				stateName: 'default',
				icon: 'fa-dot-circle-o',
				title: _translate.instant("GET_LOCATION_COORDINATES"),
				onClick: function(btn, map) {  // and its callback
					btn.disable();
					_self.enableGetLatLng(function (e) {
						btn.enable();
						_map.off("click");

						var dmd_latitude  = Dms.toLat(e.latlng.lat, 'dm', 3);
						var dmd_longitude = Dms.toLon(e.latlng.lng, 'dm', 3);

						var popup = L.popup().setLatLng(e.latlng).setContent('<strong>Coordinates:</strong><br/>' + dmd_latitude + ' ' + dmd_longitude).openOn(_map);
					});
				}
			}]
		});
		
		$(document).mouseup(function (e) {
			var container = $("#" + _mapElementId);
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				_getLocationButton.enable();
				_map.off("click");
			}
		});
		
		_getLocationButton.addTo(_map);
		
		_map.on("baselayerchange", function(e) {
			_baseLayerHistory.push(e.name);
		});
		
		$(document).bind("keydown", "alt+c", function() {
			if (_baseLayerHistory.length > 1) {
				$(".leaflet-control-layers-base").find("span").each(function (index, item) {
					if ($(this).html().trim() === _baseLayerHistory[_baseLayerHistory.length - 2]) {
						$(this).parent().find("input").trigger("click");
						return false;
					}
				});
			}
			
		});
		
		$(document).bind("keydown", "alt+z", function(e) {
			var numberOfBaseLayers = $(".leaflet-control-layers-base").find("input[type=\"radio\"]").length;
			$(".leaflet-control-layers-base").find("input[type=\"radio\"]").each(function(index, item) {
				if ($(this).prop("checked")) {
					if (index > 0) {
						var newTriggered = $(".leaflet-control-layers-base").find("input[type=\"radio\"]").get(index - 1);
						$(newTriggered).click();
						return false;
					}
				}
			});
		});
		
		$(document).bind("keydown", "alt+x", function(e) {
			var numberOfBaseLayers = $(".leaflet-control-layers-base").find("input[type=\"radio\"]").length;
			$(".leaflet-control-layers-base").find("input[type=\"radio\"]").each(function(index, item) {
				if ($(this).prop("checked")) {
					if (index < (numberOfBaseLayers - 1)) {
						var newTriggered = $(".leaflet-control-layers-base").find("input[type=\"radio\"]").get(index + 1);
						$(newTriggered).click();
						return false;
					}
				}
			});
		});
		
		var _setBaseLayer = function(layerIndex, additional) {
			var keys = [];
			for (var k in _baseLayers) keys.push(k);

			if (additional) {
				keys.splice(0, 9);
			}

			$(".leaflet-control-layers-base").find("span").each(function(index, elem) {
				if ($(this).text().indexOf(keys[layerIndex]) !== -1) {
					$(this).parent().find("input[type=\"radio\"]").click();
					return false;
				}
			});
		}

		for (var i = 1; i <= 9; i++) {
			$(document).bind("keydown", "alt+" + i, function(e) { _setBaseLayer(parseInt(e.key) - 1) });
		}

		var symbolIndexes = ["q", "w", "e", "r", "t", "a", "s", "d", "f", "g"];
		for (var i = 0; i < 10; i++) {
			$(document).bind("keydown", "alt+" + symbolIndexes[i], function(e) { 
				_setBaseLayer(symbolIndexes.indexOf(e.key), true);
				e.preventDefault();
			});
		}
    };


    var _stringCoordinatesToArray = function (coordinates) {
        var splittedCoordinates = coordinates.split(" ");
		if (splittedCoordinates.length !== 2) {
			throw "Unable to split coordinates";
		}
		
        var coordinates = [parseFloat(splittedCoordinates[0]), parseFloat(splittedCoordinates[1])];
        return coordinates;
    };
    

	_disableEditing = function (layer, callback) {
		layer.editing.disable();
		
		if (_saveButton) {
			_saveButton.disable();
		}

		if (_getLocationButton) {
			_getLocationButton.enable();
		}
		
		if (callback) {
			callback();
		}
	};


	_enableEditing = function(layer, onSave, onCancel) {
		_disableEditing(layer);

		layer.options.editing || (layer.options.editing = {});
		layer.editing.enable();

		setTimeout(function () {
			$(document).click(function(event) {
				if(!$(event.target).closest("#" + _mapElementId).length && !$(event.target).is("#" + _mapElementId)) {
					_disableEditing(layer, onCancel);
				}        
			});
		}, 1);

		_saveButton.editedLayer = layer;
		_saveButton.onSave = onSave;
		_saveButton.enable();
	};
	
	
	this.editArea = function (area, onSave, onCancel) {
		var featureLayers = _drawnAreasFeatureGroup.getLayers();
		for (var i = 0; i < featureLayers.length; i++) {
			var geoFeature = featureLayers[i].getLayers()[0];
			geoFeature.editing.disable();
		}

		for (var i = 0; i < featureLayers.length; i++) {
			if (featureLayers[i].areaId === area.id) {
				var geoFeature = featureLayers[i].getLayers()[0];
				_enableEditing(geoFeature, onSave, onCancel);
			}
		}
	};
	
	
	this.editSpecialMarkupFeature = function (specialMarkup, onSave, onCancel) {
		var featureLayers = _drawnMarkupFeatureGroup.getLayers();
		for (var i = 0; i < featureLayers.length; i++) {
			if (featureLayers[i].base) {
				var geoFeature = featureLayers[i].getLayers()[0];
				geoFeature.editing.disable();
			}
		}

		for (var i = 0; i < featureLayers.length; i++) {
			if (featureLayers[i].base) {
				if (featureLayers[i].markupId === specialMarkup.id) {
					var geoFeature = featureLayers[i].getLayers()[0];
					_enableEditing(geoFeature, onSave, onCancel);
				}
			}
		}
	};
	

    this.addBaseLayer = function (layer) {
        if (_layerControl) {
            var mapLayer = L.tileLayer(layer.tile_url).addTo(_map);
            mapLayer.baseLayerId = layer.id;

			_baseLayers[layer.name] = mapLayer;

            _layerControl.addBaseLayer(mapLayer, layer.name);
        } else {
            _pendingBaseLayers.push(layer);
        }
    };


    this.deleteBaseLayer = function (deletedLayerId) {
        _map.eachLayer(function (layer) {
            if (layer.baseLayerId && layer.baseLayerId === deletedLayerId) {
                _map.removeLayer(layer);
            }
        });
    };


    this.enableGetLatLng = function (callback) {
		$('.leaflet-container').css('cursor','crosshair');
		_map.off("click");
		_map.on("click", function (e) {
			$('.leaflet-container').css('cursor', '');
			_map.off("click");
			callback(e);
		});
    };


    this.setSearchOperationCenter = function (coordinates, text) {
        if (_map) {
            _searchOperationCenterMarker.clearLayers();
            
            if (coordinates) {
				coordinates = coordinates.replace(/,/g, ".");
                coordinates = _stringCoordinatesToArray(coordinates);

                var redMarker = L.AwesomeMarkers.icon({
                    icon: 'glyphicon-fullscreen',
                    markerColor: 'darkred',
                });

                var marker = L.marker(coordinates, {icon: redMarker}).addTo(_searchOperationCenterMarker);
                if (text) marker.bindPopup(text);
            }
        } else {
            _pendingMapCenter.push({
                coordinates: coordinates,
                text: text
            });
        }
    };


    this.addMarker = function (latLng, iconClass) {
        latLng = latLng.replace(/,/g, '.');
		var splittedCoordinates = latLng.split(" ");
        var coordinates = [parseFloat(splittedCoordinates[0]), parseFloat(splittedCoordinates[1])];

        if (_initiated) {
			var options = {};
			if (iconClass) {
				var redMarker = L.AwesomeMarkers.icon({
					icon: iconClass,
					markerColor: 'red',
					prefix: 'fa'
				});

				options.icon = redMarker;
			}
			
			var marker = new L.Marker(coordinates, options).addTo(_map);
            _map.addLayer(marker);
        } else {
            _pendingMarkers.push(coordinates);
        }

        return marker;
    };


    this.removeMarker = function (marker) {
        if (_map) {
			_map.removeLayer(marker);
		}
    };


    this.centerOn = function (coordinates) {
        coordinates = _stringCoordinatesToArray(coordinates);

        if (_map) {
            _map.setView(coordinates);
        } else {
            _center = coordinates;
        }
    };
	
	
    this.centerOnMarkup = function (markupId) {
        var layers = _drawnMarkupFeatureGroup.getLayers();
        for (var i = 0; i < layers.length; i++) {
            if (layers[i].markupId === markupId) {
				_map.setView(layers[i].getBounds().getCenter(), _map.getZoom());
            }
        }
    };


    this.centerOnArea = function (areaId) {
        var wasFound = false;
        var layers = _drawnAreasFeatureGroup.getLayers();
        for (var i = 0; i < layers.length; i++) {
            if (layers[i].areaId === areaId) {
                wasFound = true;
                _map.fitBounds(layers[i].getBounds());
				_map.zoomOut();
            }
        }

        if (wasFound === false) {
            _map.eachLayer(function (layer) {
                if (layer.areaId === areaId) {
					_map.setView(layer.getBounds().getCenter(), _map.getZoom());
                }
            });
        }
    };


    this.centerOnRoute = function (routeId) {
        var layers = _drawnRoutesFeatureGroup.getLayers();
        for (var i = 0; i < layers.length; i++) {
            if (layers[i].routeId === routeId) {
                _map.setView(layers[i].getBounds().getCenter(), _map.getZoom());
            }
        }
    }


    this.enableDrawing = function (callback, tool) {
        _map.off("click");
		if (_drawControl) {
            this.disableDrawing();
        }
 
        var options = {
            position: 'topleft',
            draw: {
                polyline: false,
                polygon: false,
                marker: false,
                circle: false,
                rectangle: false
            },
            edit: {
                featureGroup: _drawnRoutesFeatureGroup,
                edit: false,
				remove: false
            }
        };
        
        var defaultStyle = {
            shapeOptions: {
                color: "#f357a1",
                weight: 5
            }
        };
        
        if (!tool) {
            options.draw.polyline = defaultStyle;
            options.draw.polygon = defaultStyle;
            options.draw.marker = defaultStyle;
        } else if (tool === "polygon") {
            options.draw.polygon = defaultStyle;
        }

        _drawControl = new L.Control.Draw(options);
        _map.addControl(_drawControl);

        _map.removeEventListener('draw:created');
        _map.on('draw:created', function (e) {
            var type = e.layerType;

            var wkt = new Wkt.Wkt();
            wkt.fromObject(e.layer);
            var wktString = wkt.write();

            if (callback) {
                callback(type, wktString);
            }
        });
    };


    this.drawAreas = function (elements) {
        if (_drawnAreasFeatureGroup) {
            _drawnAreasFeatureGroup.clearLayers();
            for (var i = 0; i < elements.length; i++) {
                var newLayer = omnivore.wkt.parse(elements[i].geodata);

                if (elements[i].color) {
                    newLayer.setStyle({
                        color: elements[i].color,
                        stroke: true,
                        opacity: 0.8,
                        weight: 5
                    });
                }

                newLayer.areaId = elements[i].id;
                _drawnAreasFeatureGroup.addLayer(newLayer);
            }
        }
    };


    this.drawRoutes = function (routes, onClickCallback, onUnclickCallback) {
		if (_drawnRoutesFeatureGroup) {
            var metersInPixels = _metersInPixel();

            _drawnRoutesFeatureGroup.clearLayers();
            for (var i = 0; i < routes.length; i++) {
                var hearingZoneDisplayLayer = omnivore.wkt.parse(routes[i].geodata);
                
                var weight = 5;
                if (routes[i].hearing_zone) {
                    weight = Math.round(routes[i].hearing_zone / metersInPixels) * 2; 
                }

                if (routes[i].color) {
					var opacity;
                    if (routes[i].hideZone) {
						opacity = 0;
					} else {
						opacity = routes[i].opacity;
					}
					
					hearingZoneDisplayLayer.setStyle({
                        color: routes[i].color,
                        stroke: true,
                        opacity: opacity,
                        weight: weight
                    });
                }

                var newLineLayer = omnivore.wkt.parse(routes[i].geodata);
                newLineLayer.setStyle({
                    color: routes[i].color,
                    stroke: true,
                    opacity: 1,
                    weight: 5
                });

                hearingZoneDisplayLayer.routeId = routes[i].id;
                newLineLayer.routeId = routes[i].id;
				
				var popupContent = "<b>" + _translate.instant("ROUTE") + "</b><br>" + routes[i].participants;
				hearingZoneDisplayLayer.bindPopup(popupContent);
				newLineLayer.bindPopup(popupContent);

				if (onClickCallback && onUnclickCallback) {
					hearingZoneDisplayLayer.on('popupopen', onClickCallback);
					hearingZoneDisplayLayer.on('popupclose', onUnclickCallback);
					
					newLineLayer.on('popupopen', onClickCallback);
					newLineLayer.on('popupclose', onUnclickCallback);
				}
				
                _drawnRoutesFeatureGroup.addLayer(hearingZoneDisplayLayer);
                _drawnRoutesFeatureGroup.addLayer(newLineLayer);
            }
        } else {
			_pendingRoutes.push({
				routes: routes,
				onClickCallback: onClickCallback,
				onUnclickCallback: onUnclickCallback
			});
		}
    };


    this.drawMarkupElements = function (elements, onClickCallback, onUnclickCallback) {
        if (_drawnMarkupFeatureGroup) {
            _drawnMarkupFeatureGroup.clearLayers();
			
			var metersInPixels = _metersInPixel();
            for (var i = 0; i < elements.length; i++) {

				
				var opacity;
				if (elements[i].hideZone) {
					opacity = 0;
				} else {
					opacity = elements[i].opacity;
				}
				
				var hearingZoneDisplayLayer;
				if (elements[i].type === "marker") {
					var latlngRaw = elements[i].geodata;
					latlngRaw = latlngRaw.replace("POINT(", "");
					latlngRaw = latlngRaw.replace(")", "");
					latlngRaw = latlngRaw.replace(",", "");

					var coordinates = _stringCoordinatesToArray(latlngRaw);
					
					var weight = 0;
					if (elements[i].shk) {
						weight = parseInt(elements[i].shk);
					}

					hearingZoneDisplayLayer = L.circle([coordinates[1], coordinates[0]], weight);
					hearingZoneDisplayLayer.setStyle({
						color: elements[i].color,
						stroke: false,
						fillOpacity: opacity
					});
				} else {
					var weight = 0;
					if (elements[i].shk) {
						weight = Math.round(elements[i].shk / metersInPixels) * 2; 
					}

					hearingZoneDisplayLayer = omnivore.wkt.parse(elements[i].geodata);
					hearingZoneDisplayLayer.setStyle({
						color: elements[i].color,
						stroke: true,
						opacity: opacity,
						fillOpacity: elements[i].opacity,
						weight: weight
					});
				}

				hearingZoneDisplayLayer.markupId = elements[i].id;
				_drawnMarkupFeatureGroup.addLayer(hearingZoneDisplayLayer);

                var newLineLayer = omnivore.wkt.parse(elements[i].geodata);
                newLineLayer.setStyle({
                    color: elements[i].color,
                    stroke: true,
                    opacity: 1,
                    weight: 5
                });

				newLineLayer.base = true;
                newLineLayer.markupId = elements[i].id;
                _drawnMarkupFeatureGroup.addLayer(newLineLayer);

				var popupContent = "<b>" + _translate.instant("SPECIAL_MARKUP") + "</b><br>" + elements[i].description;
				hearingZoneDisplayLayer.bindPopup(popupContent);
				newLineLayer.bindPopup(popupContent);

				if (onClickCallback && onUnclickCallback) {
					hearingZoneDisplayLayer.on('popupopen', onClickCallback);
					hearingZoneDisplayLayer.on('popupclose', onUnclickCallback);
					
					newLineLayer.on('popupopen', onClickCallback);
					newLineLayer.on('popupclose', onUnclickCallback);
				}
            }
        } else {
			_pendingSpecialMarkup = [{
				elements: elements,
				onClickCallback: onClickCallback,
				onUnclickCallback: onUnclickCallback
			}];
		}
    };


    this.drawVersions = function (versions, onClickCallback, onUnclickCallback) {
        if (_drawnAreasFeatureGroup) {
            _self.removeVersions();

            for (var i = 0; i < versions.length; i++) {
                for (var j = 0; j < versions[i].areas.length; j++) {
                    var newLayer = omnivore.wkt.parse(versions[i].areas[j].geodata);

                    if (versions[i].color) {
                        var fillOpacity = parseInt(versions[i].areas[j].probability) / 1000 * 8 + 0.1;
                        newLayer.setStyle({
                            fillColor: versions[i].color,
                            fillOpacity: fillOpacity,
                            color: versions[i].color,
                            opacity: 1,
                            stroke: true,
                            weight: 2
                        });
                    }

                    newLayer.versionId = versions[i].id;
                    newLayer.areaId = versions[i].areas[j].id;

                    _layerControl.addOverlay(newLayer, versions[i].name + " (" + versions[i].areas[j].description.substring(0, 10) + ")");
                    _drawnAreasFeatureGroup.addLayer(newLayer);
					
					var popupContent = "<b>" + _translate.instant("AREA") + " (" + versions[i].name + ")</b><br>" + versions[i].areas[j].description;
					newLayer.bindPopup(popupContent);

					if (onClickCallback && onUnclickCallback) {
						newLayer.on('popupopen', onClickCallback);
						newLayer.on('popupclose', onUnclickCallback);
					}
                }
            }
        } else {
			_pendingVersions = [{
				versions: versions,
				onClickCallback: onClickCallback,
				onUnclickCallback: onUnclickCallback
			}];
		}
    };


    this.disableDrawing = function (save) {
        if (_drawControl) {
            _map.removeControl(_drawControl);
            _drawControl = false;
        }

        var drawnWKT = false;
        if (save) {
            drawnWKT = _layerGroupToWKT(_drawnItems);
        }

        _drawnItems.clearLayers();

        return drawnWKT;
    };


    this.removeVersions = function () {
        if (_drawnAreasFeatureGroup) {
            _drawnAreasFeatureGroup.eachLayer(function (layer) {
                if (layer.versionId || layer.areaId) {
                    _drawnAreasFeatureGroup.removeLayer(layer);
                    _layerControl.removeLayer(layer);
                }
            });
        }
    };


    this.resize = function () {
        setTimeout(function () {
			if (_map) _map.invalidateSize();
        }, 200);
    };
	
	this.destroy = function () {
		_map.remove();
		map = new Map();
	};
}

var map = new Map();