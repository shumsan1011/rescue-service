var app = angular.module("rescueApp", ['pascalprecht.translate', 'ui.router', 'ui.bootstrap', 'ngStorage', 'ngFileUpload', 'angularMoment']);

app.config(function ($translateProvider, $stateProvider, $urlRouterProvider) {
    $translateProvider.useSanitizeValueStrategy(null);
    $translateProvider.useStaticFilesLoader({
        prefix: "/js/app/locale/locale-",
        suffix: ".json"
    });
    $translateProvider.preferredLanguage('ru');

	$stateProvider.state('home', {
        url: '/',
        templateUrl: '/js/app/templates/partials/partial-home.html'
    }).state('admin', {
        url: '/admin',
        templateUrl: '/js/app/templates/partials/partial-admin.html'
    });
	
	$urlRouterProvider.otherwise('/');
});

app.run(["$translate", "$rootScope", "$uibModal", "$timeout", "$state", "$location", "registry", "api",
	function($translate, $rootScope, $uibModal, $timeout, $state, $location, registry, api) {
    $rootScope.$state = $state;
	
    $rootScope.toggleLanguage = function (languageId) {
        $translate.use(languageId);
        registry.languageId = languageId;
		
		if (languageId === "en") {
			moment.locale("en-gb");
		} else {
			moment.locale(languageId);
		}
		
		if (languageId in rsLeafletTranslations) {
			L.drawLocal = rsLeafletTranslations[languageId];
		}

		if ($translate.instant("APP_TITLE") !== "APP_TITLE") {
			$("title").html($translate.instant("APP_TITLE"));
		}
    };
	
	$rootScope.setInterface = function(newValue) {
		if (!newValue && newValue !== 0) newValue = registry.dashboardWidth;

		registry.dashboardWidth = newValue;
		
		$(".js-left-panel").removeClass("hidden");
		
		$(".js-left-panel").removeClass (function (index, css) {
			return (css.match (/(^|\s)col-md-\S+/g) || []).join(' ');
		});
		$(".js-right-panel").removeClass (function (index, css) {
			return (css.match (/(^|\s)col-md-\S+/g) || []).join(' ');
		});
		
		if (parseInt(newValue) === 0) {
			$(".js-left-panel").addClass("hidden");
		} else {
			$(".js-left-panel").addClass("col-md-" + newValue);
		}
		
		$(".js-right-panel").addClass("col-md-" + (12 - parseInt(newValue)));
	
		map.resize();
        
        api.get({
            path: "publicsettings",
            success: function (data) {
                if (!registry.languageId && data.defaultLanguage) {
                    $rootScope.toggleLanguage(data.defaultLanguage);
                }

                if (data.collapsedLayerControl) {
                    $rootScope.collapsedLayerControl = true;
                } else {
                    $rootScope.collapsedLayerControl = false;
                }
                
                if (data.demoIsEnabled && !$rootScope.authorized) {
                    $('#demomodal').modal();
                    $rootScope.demo = true;
                }
            }
        });
	};
	
	if (registry.token) {
        api.post({
            path: "auth-by-token",
            data: {
                token: registry.token
            },
            success: function (data) {
				registry.token    = data.token;
                registry.userId   = data.id;
                registry.userName = data.name;
                registry.isAdmin  = data.isAdmin;
				
				$rootScope.authorized = true;
				$rootScope.isAdmin    = data.isAdmin;
	
				if (registry.dashboardWidth && registry.dashboardWidth !== 0) {
					$rootScope.workspaceExpansionStage = registry.dashboardWidth;
				} else {
					$rootScope.workspaceExpansionStage = 4;
				}
            },
            error: function (status) {
                if (status === 403) {
                    $rootScope.logOut();
                }
            }
        });
    } else {
		$rootScope.authorized = false;
        $rootScope.workspaceExpansionStage = 0;
    }

    $rootScope.templateBase = "/js/app/templates/";
	$rootScope.toggleWorkspaceExpansion = function () {
		if ($rootScope.workspaceExpansionStage > 0) {
			$rootScope.workspaceExpansionStage = 0;
		} else {
			$rootScope.workspaceExpansionStage = 4;
		}
	};
	
	$rootScope.workspaceShrink = function () {
		if ($rootScope.workspaceExpansionStage > 0) $rootScope.workspaceExpansionStage--;
	};
	
	$rootScope.workspaceExpand = function () {
		if ($rootScope.workspaceExpansionStage < 6) $rootScope.workspaceExpansionStage++;
	};

	
	$rootScope.$watch("workspaceExpansionStage", function (newValue, oldValue) {
		if (newValue || newValue === 0) {
			$rootScope.setInterface(newValue);
		}
	});
	
    $rootScope.remove = function(array, index){
        array.splice(index, 1);
    }


    /**
     * Log into the application.
     *
     * @return void
     */
    $rootScope.invokeLoginWindow = function () {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/loginModal.html',
            controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                $scope.onSuccess = function () {
                    var data = {
                        email: $scope.email,
                        password: $scope.password
                    };

                    api.post({
                        path: "auth",
                        data: data,
                        success: function(data) {
                            registry.token    = data.token;
                            registry.userId   = data.id;
                            registry.userName = data.name;
							registry.isAdmin  = data.isAdmin;
				
							$rootScope.authorized = true;
							$rootScope.isAdmin    = data.isAdmin;

							$rootScope.workspaceExpansionStage = 4;
							
                            $uibModalInstance.close();
                        },
                        error: function (status) {
							if (status === 401) {
								$scope.loginError = true;
							}
                        }
                    });
                }

                $scope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }],
            size: 'sm'
        });
    }


    /**
     * Logout from the application.
     *
     * @return void
     */
    $rootScope.logOut = function () {
        registry.token = null;
        registry.name = null;

		$rootScope.authorized = false;
		$rootScope.isAdmin    = false;
		$rootScope.workspaceExpansionStage = 0;
        map.resize();
		
		$location.path("/");
    };

    if (registry.languageId) {
        $rootScope.toggleLanguage(registry.languageId);
    }
}]);