/**
 * Mask widget.
 */
(function () {
    app.directive('rsMask', function () {
        return {
            restrict: 'A',
            scope: {
                rsFormat: "@",
            },
            link: function (scope, el, attrs) {
				$(el).mask(scope.rsFormat, {translation: {
					'Z': {pattern: /[0-9]/, optional: true},
					'M': {pattern: /[-]/, optional: true},
					'N': {pattern: /[NS]/, optional: true},
					'E': {pattern: /[EW]/, optional: true}
				}});
            }
        };
    });
})();