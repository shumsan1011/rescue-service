/**
 * Slider widget.
 */
(function () {
    app.directive('rsSlider', ["$timeout", "$localStorage", function ($timeout, localStorageService) {
        return {
            restrict: 'E',
            scope: {
                rsMin: "=",
                rsMax: "=",
                rsValue: "=",
                changecallback: "&"
            },
            link: function (scope, el, attrs) {
			   var instance;
			   
			   var _apply = function () {
					if ((scope.rsValue || scope.rsValue === 0) && (scope.rsMax > scope.rsMin)) {
						if (!instance) {
							var options = {
								min: parseFloat(scope.rsMin),
								max: parseFloat(scope.rsMax),
								value: scope.rsValue,
								step: 1
							};
							
							$(el).attr("id", "sliderId");
							instance = new Slider("#sliderId", options);
							
							instance.on("slide", function (newValue) {
								scope.changecallback({val: {index: newValue, outer: true}});
							});
						} else {
							instance.setValue(scope.rsValue);
						}
                    }
                };

                scope.$watch("rsMin", function () { _apply() });
                scope.$watch("rsMax", function () { _apply() });
                scope.$watch("rsValue", function () { _apply() });
            },
            template: '<input/>'
        };
    }]);
})();