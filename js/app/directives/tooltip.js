/**
 * Directive (widget) for tooltip.
 */
(function () {
    app.directive('bootstrapTooltip', function () {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                $(el).tooltip({
                    title: attrs.heading
                });
            }
        };
    });
})();