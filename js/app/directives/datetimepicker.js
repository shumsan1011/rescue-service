/**
 * Directive (widget) for selecting date and time.
 */
(function () {
    var dateTimeController = function ($scope, $rootScope) {
        $scope.vm = {
            message: "Bootstrap DateTimePicker Directive",
            dateTime: {}
        };
    };
    var dateTimePicker = ["$rootScope", "$filter", "registry", function ($rootScope, $filter, registry) {
        return {
            require: '?ngModel',
            restrict: 'AE',
            scope: {
                format: '@',
                utc: '@',
                affectedvalue: '=',
				startdatevalue: '=',
				enddatevalue: '=',
				datedefault: '@',
            },
            link: function (scope, elem, attrs) {
				var format = "dd.mm.yyyy hh:ii";
				
				scope.$watch("affectedvalue", function (value) {
					if (value) {
						var formattedValue = moment(value).format(scope.format);
                        elem.val(formattedValue);
                    } else {
                        elem.val("");
					}
                });
				
				scope.$watch("startdatevalue", function (value) {
					if (value) {
						var nextDay = new Date(value);
						elem.datetimepicker('setStartDate', nextDay);
					}
                });

				scope.$watch("enddatevalue", function (value) {
					if (value) {
						var nextDay = new Date(value);
						elem.datetimepicker('setEndDate', nextDay);
					}
                });
				
				maxView = 4;
				minView = 0;

				if (scope.format === "DD.MM.YYYY HH:mm") {
					startView = 2;
					format = "dd.mm.yyyy hh:ii";
				} else if (scope.format === "DD.MM.YYYY") {
					minView = 2;
					startView = 2;
					format = "dd.mm.yyyy";
				} else if (scope.format === "HH:mm") {
					maxView = 1;
					startView = 1;
					format = "hh:ii";
				} else {
					throw "Unknown format";
				}
				
				elem.on("change", function (e) {
                    if (scope.utc) {
                        var momentInstance = moment($(elem).val(), scope.format);
                        var formattedValue = momentInstance.format("YYYY-MM-DDTHH:mm:ss") + "+00:00";
                    } else {
                        var formattedValue = moment($(elem).val(), scope.format).format();
                    }

                    scope.$apply(function() {
						scope.affectedvalue = formattedValue;
                    });
				});
				
				var options = {
                    format: format,
					pickDate: false,
					minView: minView,
					maxView: maxView,
					startView: startView,
					minuteStep: 10,
					language: registry.languageId,
                    useCurrent: false,
					weekStart: 1,
					autoclose: true,
				};

                elem.datetimepicker(options).on('show', function (ev) {
					if (scope.format === "HH:mm") {
						$(".datetimepicker .datetimepicker-hours thead").css("display", "none");
						$(".datetimepicker .datetimepicker-minutes thead").css("display", "none");
						$(".datetimepicker .datetimepicker-hours tbody td").css("width", "193px");
						$(".datetimepicker .datetimepicker-minutes tbody td").css("width", "193px");
					}
				});
            }
        };
    }];

    app.directive('dateTimePicker', dateTimePicker);
})();