/**
 * Directive (widget) for selecting coordinates on the map.
 */
(function () {
    app.directive('locationPicker', function ($rootScope) {
        return {
            restrict: 'E',
            scope: {
                location: '=',
                showpreview: '=',
                readonly: '@',
				iconclass: '@',
                size: '@'
            },
            controller: ["$scope", function ($scope) {
                var marker = false;
				
				var roundCoordinate = function (num) {
                    return Math.round(parseFloat(num) * 10000) / 10000;
                }

				var displayMarker = function () {
					if ($scope.readonly !== "true" && $scope.showpreview) {
						if ($scope.location) {
							if (marker) map.removeMarker(marker);
							marker = map.addMarker($scope.location, $scope.iconclass);
						}
					}
				}

				$scope.$watch("showpreview", function (value) {
					if (value) {
						displayMarker();
					} else {
						if (marker) map.removeMarker(marker);
					}
				});


				$scope.$watch("location", function (value) {
					if (value) {
						value = value.replace(/,/g, '.');

						var coordRegexp = new RegExp("^[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)\\s{1,}[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$");

						if (coordRegexp.test(value)) {
							displayMarker();
						} else {
							if (marker) map.removeMarker(marker);
						}
					} else {
						if (marker) map.removeMarker(marker);
					}
				});
				
				$scope.$on('$destroy', function() {
					if (marker) map.removeMarker(marker);
				});

                $scope.locate = function () {
                    map.centerOn($scope.location);
                }

                $scope.getLatLng = function () {
                    map.enableGetLatLng(function(e) {
                        $scope.$apply(function () {
							$scope.location = roundCoordinate(e.latlng.lat) + " " + roundCoordinate(e.latlng.lng);
                        });
                    });
                }
            }],
            template: function (elem, attrs) {
                var stylingClass = "";
                if (attrs.size === "sm") {
                    stylingClass = "-sm";
                }

                if (attrs.readonly) {
                    return '<div class="input-group location-picker">\
                      <input type="text" class="form-control input' + stylingClass + '" disabled value="{{ location }}" placeholder="0.000 0.000">\
                      <span class="input-group-btn">\
                        <button class="btn btn' + stylingClass + ' btn-default" type="button" ng-click="locate()">\
                          <span class="glyphicon glyphicon-eye-open"\ aria-hidden="true"></span>\
                        </button>\
                      </span>\
                    </div>';
                } else {
                    return '<div class="input-group">\
                      <input type="text" class="form-control input' + stylingClass + '" ng-model="location" placeholder="0.000 0.000">\
                      <span class="input-group-btn">\
                        <button class="btn btn' + stylingClass + ' btn-default" type="button" ng-disabled="!location || location.length < 5" ng-click="locate()">\
                          <span class="glyphicon glyphicon-eye-open"\ aria-hidden="true"></span>\
                        </button>\
                        <button class="btn btn' + stylingClass + ' btn-default" type="button" ng-click="getLatLng()">\
                          <span class="glyphicon glyphicon-map-marker"\ aria-hidden="true"></span>\
                        </button>\
                      </span>\
                    </div>';
                }


            }
        };
    });
})();