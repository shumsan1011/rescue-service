/**
 * Remembering state of panel groups.
 */
(function () {
    app.directive('rsPanelGroup', ["$timeout", "$localStorage", function ($timeout, localStorageService) {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                var dataTarget = false;
                var prefix = "rs-opened-tab-";

                dataTarget = $(el).find(".panel-title .collapsed").first().attr("data-target");

                if (localStorageService[prefix + dataTarget] === false) {
                    $(el).find(".collapse").collapse("hide");
                }

                $(el).find(".panel-title .collapsed").first().click(function () {
                    $timeout(function () {
                        localStorageService[prefix + dataTarget] = $(dataTarget).is(":visible");
                    }, 500);
                });

				scope.collapsePanel = function(collapse) {
					if ($(dataTarget).is(":visible") && collapse || $(dataTarget).is(":visible") === false && collapse === false) {
						$(el).find(".panel-title .collapsed").first().trigger("click");
					}
				};
            }
        };
    }]);
})();