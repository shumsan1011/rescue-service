/**
 * Directive (widget) for colorpicker.
 */
(function () {
    app.directive('bootstrapColorpicker', function () {
        return {
            restrict: 'A',
            scope: {
                colorvalue: '=',
                oncolorchange: '&'
            },
            link: function (scope, el, attrs) {
                $(el).colorpicker({
                    color: scope.colorvalue,
                    format: 'rgb'
                }).on('hidePicker.colorpicker', function(event) {
                    scope.$apply(function () {
                        var color = event.color.toRGB();
                        scope.colorvalue = "rgb(" + color.r + "," + color.g + "," + color.b + ")";
                    });

                    if (scope.oncolorchange) scope.oncolorchange();
                });
            }
        };
    });
})();