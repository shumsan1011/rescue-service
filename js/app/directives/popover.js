/**
 * Directive (widget) for popover.
 */
(function () {
    app.directive('bootstrapPopover', function () {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                $(el).popover({
                    trigger: 'hover',
                    title: attrs.heading,
                    content: attrs.content,
                    placement: 'top'
                });
            }
        };
    });
})();