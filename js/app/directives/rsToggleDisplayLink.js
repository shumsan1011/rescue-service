/**
 * Toggle display widget.
 */
(function () {
    app.directive('rsToggleDisplayLink', [function () {
        return {
            restrict: 'A',
            scope: {
                target: "@"
            },
            link: function (scope, el, attrs) {
				el.addClass("rs-toggle-display-link");
				el.click(function() {
					$("#" + attrs.target).toggle();
				});
            }
        };
    }]);
})();