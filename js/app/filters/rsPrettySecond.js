(function () {
	angular.module("rescueApp").filter('rsPrettySecond', ["$translate", function($translate) {
		return function(value) {
			var txt;
			if (Math.floor((value / 60) / 60) > 0) {
				txt = Math.floor((value / 60) / 60) + " " + $translate.instant("HOURS_SHORT") + " ";
				txt += Math.round(value / 60) - (Math.floor((value / 60) / 60) * 60) + " " + $translate.instant("MINUTES_SHORT") + " ";
			} else {
				txt = Math.round(value / 60) + " " + $translate.instant("MINUTES_SHORT") + " ";
			}

			return txt;
		};
	}]);
})();
