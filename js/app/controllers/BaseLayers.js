app.controller("BaseLayersController", ["$rootScope", "$scope", "$translate", "$timeout", "$uibModal", "api", "registry", function($rootScope, $scope, $translate, $timeout, $uibModal, api, registry) {

    $scope.ready = false;
    $scope.baseLayers = new Array();

    /**
     * Requests server for the set of base layers.
     *
     * @return void
     */
    var _refreshLayers = function () {
        api.get({
            path: "baselayers",
            success: function (data) {
                $scope.ready = true;

                $scope.baseLayers = data;
                for (var i = 0; i < data.length; i++) {
                    map.addBaseLayer(data[i]);
                }

                map.initiate({
					onMoveEnd: function (center, zoom) {
						registry.mapCenter = center;
						registry.mapZoom = zoom;
					},
					mapCenter: (registry.mapCenter) ? registry.mapCenter : false,
					mapZoom: (registry.mapZoom) ? registry.mapZoom : false,
					translate: $translate,
                    collapsed: $rootScope.collapsedLayerControl
				});
            }
        });
    }

    _refreshLayers();


    /**
     * Deletes specific base layer.
     *
     * @param Object baseLayer Base layer instance
     *
     * @return void
     */
    $scope.deleteBaseLayer = function (baseLayer) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/confirmationModal.html',
            controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                $scope.text = $translate.instant("ARE_YOU_SURE_YOU_WANT_TO_DELETE") + " " + baseLayer.name;
                $scope.onSuccess = function () {
                    $uibModalInstance.close(true);
                }

                $scope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }],
            size: 'sm'
        });

        modalInstance.result.then(function (hasToBeDeleted) {
            if (hasToBeDeleted) {
                api.delete({
                    path: "baselayer/" + baseLayer.id,
                    success: function(data) {
                        for (var i = 0; i < $scope.baseLayers.length; i++) {
                            if ($scope.baseLayers[i].id === baseLayer.id) {
                                $scope.baseLayers.splice(i, 1);
                                map.deleteBaseLayer(baseLayer.id);
                                break;
                            }
                        }
                    }
                });
            }
        });
    };


    /**
     * Edit specific base layer.
     *
     * @param Object baseLayer Base layer instance
     *
     * @return void
     */
    $scope.editBaseLayer = function (baseLayer) {
        var passedLayer = {};
        if (baseLayer && "id" in baseLayer) {
            passedLayer = baseLayer;
        } else {
            passedLayer.id = false;
            passedLayer.name = "";
            passedLayer.tile_url = "";
        }

        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/editBaseLayerModal.html',
            controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                $scope.layer = passedLayer;

                $scope.onSuccess = function () {
                    $uibModalInstance.close($scope.layer);
                }

                $scope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }],
            size: 'sm'
        });

        modalInstance.result.then(function (layer) {
            if (layer) {
                if (layer.id) {
                    api.put({
                        path: "baselayer/" + layer.id,
                        data: layer,
                        success: function () {
                            map.deleteBaseLayer(layer.id);
                            map.addBaseLayer(layer);

                            baseLayer.name = layer.name;
                        },
                        error: function (status) {
                            console.log(status);
                        }
                    });
                } else {
                    api.post({
                        path: "baselayers",
                        data: layer,
                        success: function (data) {
                            layer.id = data.id;
                            $scope.baseLayers.push(layer);

                            map.addBaseLayer(layer);
                        },
                        error: function (status) {
                            console.log(status);
                        }
                    });
                }
            }
        });
    };
	
    $scope.$on("$destroy", function() {
        map.destroy();
    });
}]);