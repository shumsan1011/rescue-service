app.controller("RoutesController", ["$rootScope", "$scope", "$translate", "$uibModal", "api", "registry", function($rootScope, $scope, $translate, $uibModal, api, registry) {

    $scope.ready = false;
    $scope.routes = new Array();

	
	var _onClickFeatureCallback = function (e) {
		for (var i = 0; i < $scope.routes.length; i++) {
			if ($scope.routes[i].id === e.target.routeId) {
				$scope.$apply(function () {
					$scope.routes[i].active = true;
				});
			}
		}
	};
	
	
	var _onUnclickFeatureCallback = function (e) {
		for (var i = 0; i < $scope.routes.length; i++) {
			if ($scope.routes[i].id === e.target.routeId) {
				$scope.$apply(function () {
					$scope.routes[i].active = false;
				});
			}
		}
	};
	
	
    var _refreshRoutes = function () {
        if (registry.currentSearchOperationId && registry.token) {
            api.get({
                path: "routes?search_operation_id=" + registry.currentSearchOperationId,
                success: function (data) {
                    $scope.ready = true;
                    
					for (var i = 0; i < data.length; i++) {
						data[i].date_from = moment(data[i].date_from).format();
						data[i].date_to = moment(data[i].date_to).format();
					}
					
					$scope.routes = data;

                    map.addOnZoomLevelChange("routes", function () {
                        map.drawRoutes($scope.routes, _onClickFeatureCallback, _onUnclickFeatureCallback);
                    });
                }
            });
        } else {
            $scope.ready = false;
        }
    }

    $scope.$on("searchOperationChanged", _refreshRoutes);

	$scope.toggleRouteZoneDisplay = function (route) {
		for (var i = 0; i < $scope.routes.length; i++) {
			if ($scope.routes[i].id === route.id) {
				if ($scope.routes[i].hideZone) {
					$scope.routes[i].hideZone = false;
				} else {
					$scope.routes[i].hideZone = true;
				}
			}
		}

		map.drawRoutes($scope.routes, _onClickFeatureCallback, _onUnclickFeatureCallback);
	};
	
	
	$scope.editRouteDisplay = function (route) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/editRoutesDisplayModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
                
				$modalScope.color = route.color;
				if ("opacity" in route) {
					$modalScope.opacity = route.opacity;
				} else {
					$modalScope.opacity = 0.5;
				}
				
                $modalScope.onSuccess = function () {
					route.color = $modalScope.color;
					route.opacity = $modalScope.opacity;
					
					var rawStyle = JSON.stringify({color: $modalScope.color, opacity: $modalScope.opacity});
					api.put({
						path: 'route/' + route.id,
						data: {display_style: rawStyle},
						success: function () {
							for (var i = 0; i < $scope.routes.length; i++) {
								if ($scope.routes[i].id === route.id) {
									$scope.routes[i].color = $modalScope.color;
									$scope.routes[i].opacity = $modalScope.opacity;
								}
							}
							
							map.drawRoutes($scope.routes, _onClickFeatureCallback, _onUnclickFeatureCallback);
							$uibModalInstance.close();
						}
					});
                }
				
				$modalScope.onCancel = function () {
					$uibModalInstance.close();
				}
            }],
			size: "sm"
        });
	};
	
	
    $scope.editRoute = function (route) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/editRouteModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
                
                $modalScope.route = route;
                if ($modalScope.route) {
                    $modalScope.geodata = true;

                    $modalScope.id = route.id;
                    $modalScope.participants = route.participants;
                    $modalScope.date_from = route.date_from;
                    $modalScope.date_to = route.date_to;
                    $modalScope.hearing_zone = route.hearing_zone;
                } else {
                    $modalScope.geodata = false;
                }

                $modalScope.filePattern = '.gpx';

                $modalScope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }

                $modalScope.upload = function (file) {
					$modalScope.filename = file.name;
					
					$modalScope.loading = true;
					
					api.upload({
                        path: 'routes/upload',
                        file: file,
                        success: function (data) {
							$modalScope.loading = false;
                            $modalScope.geodata = data.geodata;
                        },
						error: function () {
							$modalScope.loading = false;
						}
                    });
                }

                $modalScope.onSuccess = function () {
                    var data = {
                        participants: $modalScope.participants,
                        geodata: $modalScope.geodata,
                        date_from: $modalScope.date_from,
                        date_to: $modalScope.date_to,
                        hearing_zone: $modalScope.hearing_zone,
                        search_operation_id: registry.currentSearchOperationId
                    };

                    if ($modalScope.id) {
                        api.put({
                            path: 'route/' + $modalScope.id,
                            data: data,
                            success: function () {
								$scope.collapsePanel(false);
                                for (var i = 0; i < $scope.routes.length; i++) {
                                    if ($scope.routes[i].id === $modalScope.id) {
                                        $scope.routes[i].participants = data.participants;
                                        $scope.routes[i].hearing_zone = data.hearing_zone;

                                        $scope.routes[i].date_from = data.date_from;
                                        $scope.routes[i].date_to   = data.date_to;
                                    }
                                }

                                map.drawRoutes($scope.routes, _onClickFeatureCallback, _onUnclickFeatureCallback);
                                $uibModalInstance.close();
                            }
                        });
                    } else {
                        api.post({
                            path: 'routes',
                            data: data,
                            success: function (response) {
								$scope.collapsePanel(false);
								
                                data.id = response.id;
                                
                                data.date_from = data.date_from;
                                data.date_to = data.date_to;
                                
                                $scope.routes.push(data);
                                $uibModalInstance.close();
                            }
                        });
                    }
                }
            }]
        });
    }


    $scope.downloadAsGPX = function (route) {
        api.fileDownload("route/" + route.id + "?format=GPX");
    }


    $scope.$watchCollection("routes", function(routes) {
        for (var i = 0; i < routes.length; i++) {
			if (routes[i].display_style) {
				var parsedStyle = JSON.parse(routes[i].display_style);
				if (parsedStyle.color) routes[i].color = parsedStyle.color;
				if (parsedStyle.opacity) routes[i].opacity = parsedStyle.opacity;
			}
			
            if (!routes[i].color) {
                routes[i].color = getRandomColor();
                routes[i].opacity = 0.5;
            }
        }

        map.drawRoutes(routes, _onClickFeatureCallback, _onUnclickFeatureCallback);
    });


    $scope.centerOnRoute = function (route) {
        map.centerOnRoute(route.id);
    }

    $scope.deleteRoute = function (route) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/confirmationModal.html',
            controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                $scope.text = $translate.instant("ARE_YOU_SURE_YOU_WANT_TO_DELETE") + " (" + route.participants + ")";
                $scope.onSuccess = function () {
                    $uibModalInstance.close(true);
                }

                $scope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }],
            size: 'sm'
        });

        modalInstance.result.then(function (hasToBeDeleted) {
            if (hasToBeDeleted) {
                api.delete({
                    path: "route/" + route.id,
                    success: function(data) {
                        for (var i = 0; i < $scope.routes.length; i++) {
                            if ($scope.routes[i].id === route.id) {
                                $scope.routes.splice(i, 1);
                            }
                        }
                    }
                });
            }
        });
    }
}]);