app.controller("TimeIntervalsModalController", ["$scope", "$uibModalInstance", "$translate", "timeSettings", "customStages", "stages", "rsStages", "rsSettings", function ($scope, $uibModalInstance, $translate, timeSettings, customStagesOuter, stagesOuter, rsStages, rsSettings) {
    $scope.defaultSpeed = rsSettings.defaultMissingPersonSpeed;

    var _checkSettings = function (settings) {
        var errorCodes = new Array();
        if (!settings.finish_time || !settings.start_time) {
            errorCodes.push("SET_START_AND_FINISH_TIME");
        }

        if (settings.finish_time < settings.start_time) {
            errorCodes.push("SET_START_TIME_EARLIER_THAN_FINISH_TIME");
        }

        if (!settings.sunrise_time || !settings.sunset_time) {
            errorCodes.push("SET_SUNRISE_AND_SUNSET_TIME");
        }

        if (!settings.moonrise_time || !settings.moonset_time) {
            errorCodes.push("SET_MOONRISE_AND_MOONSET_TIME");
        }

        return errorCodes;
    };

    var errors = _checkSettings(timeSettings);
    if (errors.length === 0) {        
        if (timeSettings.start_staying_duration) timeSettings.start_staying_duration = parseInt(timeSettings.start_staying_duration);

        if (timeSettings.finish_time) timeSettings.finish_time     = toTimestamp(timeSettings.finish_time);
        if (timeSettings.start_time) timeSettings.start_time       = toTimestamp(timeSettings.start_time);
        if (timeSettings.moonrise_time) timeSettings.moonrise_time = toTimestamp(timeSettings.moonrise_time);
        if (timeSettings.moonset_time) timeSettings.moonset_time   = toTimestamp(timeSettings.moonset_time);
        if (timeSettings.sunrise_time) timeSettings.sunrise_time   = toTimestamp(timeSettings.sunrise_time);
        if (timeSettings.sunset_time) timeSettings.sunset_time     = toTimestamp(timeSettings.sunset_time);
        
        for (var i = 0; i < timeSettings.weather_conditions.length; i++) {
            timeSettings.weather_conditions[i].time_from = toTimestamp(timeSettings.weather_conditions[i].time_from);
            timeSettings.weather_conditions[i].time_to = toTimestamp(timeSettings.weather_conditions[i].time_to);
        }
    } else {
        $scope.errors = errors;
    }

	$scope.customStages = customStagesOuter.stages;

    $scope.generateStages = function () {
        if (!$scope.errors) {
            var timeSettingsCopy = angular.copy(timeSettings);

            var customStages = new Array();
            for (var i = 0; i < $scope.customStages.length; i++) {
                customStages.push({
                    time_from: moment($scope.customStages[i].time_from).unix(),
                    time_to: moment($scope.customStages[i].time_to).unix(),
                    description: $scope.customStages[i].description
                });
            }

            timeSettingsCopy.weather_conditions = timeSettingsCopy.weather_conditions.concat(customStages);
            $scope.stages = rsStages.generate(timeSettingsCopy);
        }
    }
    
	if (stagesOuter.stages) {
		$scope.stages = stagesOuter.stages;
	} else {
		$scope.generateStages();
	}
    
    $scope.editCustomStage = function (stage) {
        $scope.customStage = {};
        if (stage) {
            $scope.customStage = stage;
        }
        
        $scope.editing = true;
    };
    
    
    $scope.deleteCustomStage = function (stage) {
        $scope.customStages.splice($scope.customStages.indexOf(stage), 1);
        $scope.generateStages();
    };
    
    
    $scope.addCustomStage = function () {
        if ("$$hashKey" in $scope.customStage === false) {
            var savedStage = $scope.customStage;
            $scope.customStage.temperature = false;
            $scope.customStages.push(savedStage);
        }
        
        $scope.generateStages();
        $scope.editing = false;
    };
    
    
    $scope.cancelEditingCustomStage = function () {
        $scope.editing = false;
    };
    

    $scope.onSuccess = function () {
        $uibModalInstance.close($scope.stages);
    }

    $scope.onCancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
}]);