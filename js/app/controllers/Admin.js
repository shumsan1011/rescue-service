app.controller("AdminController", ["$rootScope", "$scope", "$translate", "$uibModal", "api", "registry", function($rootScope, $scope, $translate, $uibModal, api, registry) {

	$scope.users = new Array();

	var _refreshUsers = function () {
		api.get({
			path: "admin/users",
			success: function (data) {
				$scope.users = data;
			}
		});
	};

	_refreshUsers();
	
	$scope.editUser = function(user) {
		if (!user) user = {};

		var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/editUserModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
				$modalScope.name     = user.name;
				$modalScope.email    = user.email;
				$modalScope.password = user.password;
				$modalScope.is_admin = user.is_admin;
				
				if (user.id) {
					$modalScope.id       = user.id;
					$modalScope.password = "";
					$modalScope.text     = $translate.instant("EDIT_USER");
				} else {
					$modalScope.text = $translate.instant("ADD_USER");
				}

				$modalScope.onSuccess = function () {
					if (user.id) {
						api.put({
							path: "admin/users/" + user.id,
							data: {
								name: $modalScope.name,
								email: $modalScope.email,
								password: $modalScope.password,
								is_admin: $modalScope.is_admin
							},
							success: function (data) {
								_refreshUsers();
								$uibModalInstance.dismiss('cancel');
							}
						});
					} else {
						api.post({
							path: "admin/users",
							data: {
								name: $modalScope.name,
								email: $modalScope.email,
								password: $modalScope.password,
								is_admin: $modalScope.is_admin
							},
							success: function (data) {
								_refreshUsers();
								$uibModalInstance.dismiss('cancel');
							}
						});
					}
				};

                $modalScope.onCancel = function () {
					$uibModalInstance.dismiss('cancel');
				};
            }],
			size: 'sm'
        });
	};
	
	$scope.deleteUser = function(user) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/confirmationModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
                $modalScope.text = $translate.instant("ARE_YOU_SURE_YOU_WANT_TO_DELETE") + " " + user.name + "?";
                $modalScope.onSuccess = function () {
                    api.delete({
                        path: "admin/users/" + user.id,
                        success: function() {
							_refreshUsers();
							$uibModalInstance.close(true);
                        }
                    });
                }

                $modalScope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }],
            size: 'sm'
        });
	};
}]);