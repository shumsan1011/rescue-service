app.controller("VersionsController", ["$rootScope", "$scope", "$translate", "$uibModal", "api", "registry", function($rootScope, $scope, $translate, $uibModal, api, registry) {

    $scope.ready = false;
    $scope.currentVersion = {};
    $scope.currentArea = {};
    $scope.currentAreas = new Array();
    $scope.versions = new Array();
    $scope.versionsProbabilitySum = 0;
    $scope.areasProbabilitySums = new Array();


	var _onClickFeatureCallback = function (e) {
		for (var i = 0; i < $scope.versions.length; i++) {
			for (var j = 0; j < $scope.versions[i].areas.length; j++) {
				if ($scope.versions[i].areas[j].id === e.target.areaId) {
					$scope.$apply(function () {
						$scope.versions[i].areas[j].active = true;
					});
				}
			}
		}
	};
	
	
	var _onUnclickFeatureCallback = function (e) {
		for (var i = 0; i < $scope.versions.length; i++) {
			for (var j = 0; j < $scope.versions[i].areas.length; j++) {
				if ($scope.versions[i].areas[j].id === e.target.areaId) {
					$scope.$apply(function () {
						$scope.versions[i].areas[j].active = false;
					});
				}
			}
		}
	};
	
	
    /**
     * Retrieve versions from the backend.
     *
     * @return void
     */
    var _refreshVersions = function () {
        if (registry.currentSearchOperationId && registry.token) {
            api.get({
                path: "versions?search_operation_id=" + registry.currentSearchOperationId,
                success: function (data) {
                    $scope.ready = true;

                    for (var i = 0; i < data.length; i++) {
						if (data[i].display_style) {
							var parsedStyle = JSON.parse(data[i].display_style);
							if (parsedStyle.color) data[i].color = parsedStyle.color;
						}
						
						if (!data[i].color) {
							data[i].color = getRandomColor();
						}

                        for (var j = 0; j < data[i].areas.length; j++) {
                            data[i].areas[j].probability = parseInt(data[i].areas[j].probability);
                        }

                        data[i].probability = parseInt(data[i].probability);
                    }

                    $scope.versions = data;
                    map.drawVersions($scope.versions, _onClickFeatureCallback, _onUnclickFeatureCallback);
                }
            });
        } else {
            $scope.ready = false;
        }
    }


    $scope.$on("searchOperationChanged", _refreshVersions);
    $scope.$watch("currentAreas", function (areas) {
        if (areas) {
            map.drawAreas(areas);
        }
    }, true);


    $scope.saveArea = function () {
        if ("id" in $scope.currentArea === false) {
            $scope.currentAreas.push({
                id: Date.now(),
                description: $scope.currentArea.description,
                probability: $scope.currentArea.probability,
                geodata: $scope.currentArea.geodata,
				version_id: $scope.currentVersion.id
            });
        }

        $scope.currentArea = {};
    };


    $scope.showArea = function (area) {
        map.centerOnArea(area.id);
    };


    $scope.editArea = function (area) {
        $scope.currentArea = area;
    }


    $scope.deleteArea = function (area) {
        for (var i = 0; i < $scope.currentAreas.length; i++) {
            if ($scope.currentAreas[i].id === area.id) {
                $scope.currentAreas.splice(i, 1);
            }
        }
    };


    $scope.drawArea = function () {
        map.enableDrawing(function (type, wkt) {
            $scope.$apply(function () {
                $scope.currentArea.geodata = wkt;
				map.drawAreas([$scope.currentArea]);
            });
        }, "polygon", true);
    };

	
	$scope.editAreaFeature = function (area) {
		map.editArea(area, function (geodata) {
			var data = {};

			var correspondingVersion = false;
			for (var i = 0; i < $scope.versions.length; i++) {
				if ($scope.versions[i].id === area.version_id) {
					correspondingVersion = $scope.versions[i];
					
					for (var j = 0; j < $scope.versions[i].areas.length; j++) {
						if ($scope.versions[i].areas[j].id === area.id) {
							$scope.versions[i].areas[j].geodata = geodata;
						}
					}
					
					data.areas = $scope.versions[i].areas;
					break;
				}
			}
			
			if (!correspondingVersion) throw "Unable to detect corresponding version";
			api.put({
				path: 'versions/' + correspondingVersion.id,
				data: data,
				success: function () {
					map.drawVersions($scope.versions, _onClickFeatureCallback, _onUnclickFeatureCallback);
				}
			});
		}, function () { map.drawVersions($scope.versions, _onClickFeatureCallback, _onUnclickFeatureCallback); });
	};
	
	
    $scope.getAreasProbabilitySum = function (areas) {
        var sum = 0;
        for (var i = 0; i < areas.length; i++) {
            sum += areas[i].probability;
        }

        return sum;
    };


    var _probabilityCheck = function () {
        var sum = 0;
        var areaSums = new Array();
        
        for (var i = 0; i < $scope.versions.length; i++) {
            sum += $scope.versions[i].probability;

            areaSums[$scope.versions[i].id] = 0;
            for (var j = 0; j < $scope.versions[i].areas.length; j++) {
                areaSums[$scope.versions[i].id] += $scope.versions[i].areas[j].probability;
            }
        }

        $scope.areasProbabilitySums = areaSums;
        $scope.versionsProbabilitySum = sum;
    };


    $scope.$watch("versions", _probabilityCheck, true);

	
	$scope.editVersionDisplay = function (version) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/editFeatureDisplayModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
                
				$modalScope.color = version.color;
				if ("opacity" in version) {
					$modalScope.opacity = version.opacity;
				} else {
					$modalScope.opacity = 0.5;
				}
				
                $modalScope.onSuccess = function () {
					version.color = $modalScope.color;
					version.opacity = $modalScope.opacity;
					
					var data = {
						display_style: JSON.stringify({color: $modalScope.color, opacity: $modalScope.opacity}),
						areas: []
					};
					
					for (var i = 0; i < $scope.versions.length; i++) {
						if ($scope.versions[i].id === version.id) {
							data.areas = $scope.versions[i].areas;
						}
					}
					
					api.put({
						path: 'versions/' + version.id,
						data: data,
						success: function () {
							for (var i = 0; i < $scope.versions.length; i++) {
								if ($scope.versions[i].id === version.id) {
									for (var j = 0; j < $scope.versions[i].areas.length; j++) {
										if (!$scope.versions[i].areas[j].display_style) {
											$scope.versions[i].areas[j].color = $modalScope.color;
											$scope.versions[i].areas[j].opacity = $modalScope.opacity;
										}
									}
								}
							}

							map.drawVersions($scope.versions, _onClickFeatureCallback, _onUnclickFeatureCallback);
							$uibModalInstance.close();
						}
					});
                }
				
				$modalScope.onCancel = function () {
					$uibModalInstance.close();
				}
            }],
			size: "sm"
        });
	};
	
	
    $scope.applyColorChange = function () {
        map.drawVersions($scope.versions, _onClickFeatureCallback, _onUnclickFeatureCallback)
    };


    $scope.setProbabilityForVersion = function () {
		var probability = 0;
        for (var i = 0; i < $scope.versions.length; i++) {
            probability += parseInt($scope.versions[i].probability);
        }

        var predictedProbability = (100 - probability);
        if (!$scope.currentVersion.id) {
			if (predictedProbability <= 100 && predictedProbability >= 0) {
				$scope.currentVersion.probability = predictedProbability;
			} else {
				$scope.currentVersion.probability = 0;
			}
		}
    };


    $scope.setProbabilityForAreas = function () {
        var probability = 0;
        if ($scope.currentAreas.length !== 0) {
            for (var i = 0; i < $scope.currentAreas.length; i++) {
                probability += parseInt($scope.currentAreas[i].probability);
            }
        }

        var predictedProbability = (100 - probability);
        if (predictedProbability <= 100 && predictedProbability >= 0) {
            $scope.currentArea.probability = predictedProbability;
        } else {
            $scope.currentArea.probability = 0;
        }
    };


    $scope.saveVersion = function () {
        var areas = new Array();
        for (var i = 0; i < $scope.currentAreas.length; i++) {
            areas.push({
                description: $scope.currentAreas[i].description,
                probability: $scope.currentAreas[i].probability,
                geodata: $scope.currentAreas[i].geodata,
            });
        }

        var data = {
            name: $scope.currentVersion.name,
            description: $scope.currentVersion.description,
            probability: $scope.currentVersion.probability,
            search_operation_id: registry.currentSearchOperationId,
            areas: areas
        };

        /**
         * Set of routines that has to be performed on regular creation or update.
         *
         * @return void
         */
        var _onSuccess = function () {
            $scope.editing = false;
            map.disableDrawing();
            map.drawVersions($scope.versions, _onClickFeatureCallback, _onUnclickFeatureCallback);
            _probabilityCheck();
        };

        if ($scope.currentVersion.id) {
            api.put({
                path: 'versions/' + $scope.currentVersion.id,
                data: data,
                success: _onSuccess
            });
        } else {
            api.post({
                path: 'versions',
                data: data,
                success: function (response) {
                    data.id = response.id;
                    data.color = getRandomColor();
                    $scope.versions.push(data);

                    _onSuccess();
                }
            });
        }
    };

    $scope.deleteVersion = function (version) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/confirmationModal.html',
            controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                $scope.text = $translate.instant("ARE_YOU_SURE_YOU_WANT_TO_DELETE") + " (" + version.name + ")?";
                $scope.onSuccess = function () {
                    $uibModalInstance.close(true);
                }

                $scope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }],
            size: 'sm'
        });

        modalInstance.result.then(function (hasToBeDeleted) {
            if (hasToBeDeleted) {
                api.delete({
                    path: "versions/" + version.id,
                    success: function(data) {
                        for (var i = 0; i < $scope.versions.length; i++) {
                            if ($scope.versions[i].id === version.id) {
                                $scope.versions.splice(i, 1);
                            }
                        }

                        map.drawVersions($scope.versions, _onClickFeatureCallback, _onUnclickFeatureCallback);
                    }
                });
            }
        });
    };


    var backupVersion = {};
    $scope.cancelVersionEditing = function () {
        $scope.currentVersion = backupVersion;
        $scope.editing = false;
        
        map.drawVersions($scope.versions, _onClickFeatureCallback, _onUnclickFeatureCallback);
    };

    
    $scope.editVersion = function (version) {
        backupVersion = angular.copy(version);

        $scope.editing = true;
        $scope.currentVersion = version;
        $scope.currentAreas = version.areas;

        map.removeVersions();
        $scope.setProbabilityForVersion();
        $scope.setProbabilityForAreas();
    };


    $scope.createVersion = function () {
		$scope.collapsePanel(false);
		
		$scope.editing = true;
        $scope.currentVersion = {};
        $scope.currentAreas = new Array();

        map.removeVersions();
        $scope.setProbabilityForVersion();
        $scope.setProbabilityForAreas();
    };

}]);