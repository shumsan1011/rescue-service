app.controller("MissingPeopleController", ["$rootScope", "$scope", "$translate", "$uibModal", "api", "registry", "keeper", function($rootScope, $scope, $translate, $uibModal, api, registry, keeper) {

    $scope.ready = false;
    $scope.keeper = keeper;

    /**
     * Request server for the list of missing people.
     *
     * @return void
     */
    var _refreshMissingPeople = function () {
        if (registry.currentSearchOperationId && registry.token) {
            api.get({
                path: "missing?search_operation_id=" + registry.currentSearchOperationId,
                success: function (data) {
                    $scope.ready = true;
                    $scope.keeper.missingPeople = data;
                }
            });
        } else {
            $scope.ready = false;
        }
    }

    $scope.$watch("authorized", function (isAuthorized) {
		if (isAuthorized) _refreshMissingPeople();
	});
    $scope.$on("searchOperationChanged", _refreshMissingPeople);


    /**
     * Edit missing person.
     *
     * @param Object person Missing person instance
     *
     * @return void
     */
    $scope.editMissingPerson = function (person) {
        if (!person) person = {sex: 0};

        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/missingPersonModal.html',
            controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {

                $scope.weather_condition = {};
                $scope.person = person;
                
                if (!$scope.person.weather_conditions) {
                    $scope.person.weather_conditions = new Array();
                }

                $scope.onSuccess = function () {
                    $uibModalInstance.close($scope.person);
                }

                $scope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }

                $scope.addWeatherCondition = function () {
                    $scope.person.weather_conditions.push($scope.weather_condition);
                    $scope.weather_condition = {};
                    $scope.showWeatherConditionForm = false;
                }
            }]
        });

        modalInstance.result.then(function (savedPerson) {
            if (savedPerson) {
                var data = {
                    name: savedPerson.name,
                    sex: parseInt(savedPerson.sex),
                };

                if (savedPerson.birth_year) data.birth_year = savedPerson.birth_year;

                if (savedPerson.id) {
                    api.put({
                        path: "missing/" + savedPerson.id,
                        data: data
                    });
                } else {
                    data.search_operation_id = registry.currentSearchOperationId;

                    api.post({
                        path: "missing",
                        data: data,
                        success: function (data) {
                            savedPerson.id = data.id;
                            $scope.keeper.missingPeople.push(savedPerson);
                        }
                    });
                }
            }
        });
    }


    /**
     * Delete missing person record.
     *
     * @param Object person Missing person instance
     *
     * @return void
     */
    $scope.deleteMissingPerson = function (person) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/confirmationModal.html',
            controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                $scope.text = $translate.instant("ARE_YOU_SURE_YOU_WANT_TO_DELETE") + "?";
                $scope.onSuccess = function () {
                    $uibModalInstance.close(person);
                }

                $scope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }],
            size: 'sm'
        });

        modalInstance.result.then(function (person) {
            if (person) {
                api.delete({
                    path: "missing/" + person.id,
                    success: function(data) {
                        for (var i = 0; i < $scope.keeper.missingPeople.length; i++) {
                            if ($scope.keeper.missingPeople[i].id === person.id) {
                                $scope.keeper.missingPeople.splice(i, 1);
                            }
                        }
                    }
                });
            }
        });
    };
}]);