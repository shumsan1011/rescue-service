app.controller("ModellingSettingsController", ["$rootScope", "$scope", "$timeout", "$uibModal", "keeper", "api", "registry", "keeper", "rsSettings", function($rootScope, $scope, $timeout, $uibModal, keeper, api, registry, keeper, rsSettings) {

    $scope.keeper = keeper;
    $scope.selectedMissingPerson = $scope.keeper.missingPeople[0];
	
	function _setup() {
		$scope.displayedTime = false;
		$scope.calculating   = false;
		$scope.downloading   = false;
		$scope.spreadStages  = false;

		$scope.customStages = new Array();
		$scope.stages       = undefined;

		$scope.settings = {
			sunrise_time: moment("1970-01-01 06:00:00").format(),
			sunset_time: moment("1970-01-01 22:00:00").format(),
			moonrise_time: moment("1970-01-01 00:00:00").format(),
			moonset_time: moment("1970-01-01 05:00:00").format(),
			height: rsSettings.defaultMissingPersonHeight,
			weight: rsSettings.defaultMissingPersonWeight,
			speed: rsSettings.defaultMissingPersonSpeed,
			tiredness: null,
			hearing: null,
			voice: null,
			goal: null,
			weather_conditions: [],
			start_coordinates: undefined,
			start_time: undefined,
			stayed_for: undefined,
			finish_time: moment(new Date()).format()
		};
		
		map.clearSpreadStages();
	}
	
    $scope.$on("searchOperationChanged", function () {
		_setup();
    });

	_setup();
	
    $scope.$watch("keeper", function (value) {
        if (value) {
            $scope.selectedMissingPerson = value.missingPeople[0];
        }
    }, true);

    $scope.templates = new Array();


    /**
     * Request server for the list of settings templates.
     *
     * @return void
     */
    var _refreshSettings = function () {
        if (registry.token) {
            api.get({
                path: "settings-templates",
                success: function (data) {
                    $scope.templates = data;
                    $scope.currentTemplate = data[0];
                }
            });
        }
    }

    $scope.$watch("authorized", function (isAuthorized) {
		if (isAuthorized) _refreshSettings();
	});

	$scope.clearCompuitations = function () {
		_setup();
	};
    
    $scope.computeLocation = function () {
		$scope.calculating = true;
		$scope.downloading = false;
		
		$timeout(function () {
			var geoObjects = [];
			for (var i = 0 ; i < keeper.specialMarkup.length; i++) {
				if ((keeper.specialMarkup[i].type === "polygon" || keeper.specialMarkup[i].type === "polyline") && (keeper.specialMarkup[i].sp || keeper.specialMarkup[i].sp === 0 || keeper.specialMarkup[i].ovn === 1)) {
					var speed_modifier = keeper.specialMarkup[i].sp;
					if (keeper.specialMarkup[i].ovn === 1) {
						speed_modifier = 0;
					}
					
					geoObjects.push({
						type: keeper.specialMarkup[i].type,
						geodata: omnivore.wkt.parse(keeper.specialMarkup[i].geodata).toGeoJSON().features.pop(),
						speed_modifier: speed_modifier,
						name: keeper.specialMarkup[i].description
					});
				}
			}

			var stagesCopy = angular.copy($scope.stages);
			
			var coordinates = $scope.settings.start_coordinates.split(" ")
			var cleanedStages = {
				meters_in_pixel: 7,
				start_point: {
					lat: coordinates[0],
					lng: coordinates[1]
				},
				time_periods: new Array(),
				objects: geoObjects
			};

			for (var i = 0; i < stagesCopy.length; i++) {
				cleanedStages.time_periods.push({
					time_from: stagesCopy[i].time_from,
					time_to: stagesCopy[i].time_to,
					speed: stagesCopy[i].speed
				});
			}

			$.ajax({
				type: "POST",
				url: "/computation/app.php",
				timeout: 60000,
				data: {data: btoa(unescape(encodeURIComponent(JSON.stringify(cleanedStages))))},
				success: function (path) {
					$scope.$apply(function () {
						$scope.calculating  = false;
						$scope.downloading  = true;
					});

					if (path === "ERROR") {
						$scope.calculating  = false;
						$scope.downloading  = false;
						
						var errorCodes = ["ERROR_OCCURED_AT_COMPUTATION"];
						var modalInstance = $uibModal.open({
							templateUrl: $rootScope.templateBase + 'modals/errorModal.html',
							controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
								$scope.errorCodes = errorCodes;
								
								$scope.onSuccess = function () {
									$uibModalInstance.close(true);
								}
							}]
						});
					} else {
						path = path.replace("./log", "/computation/log");
						$.ajax({
							type: "GET",
							url: path,
							timeout: 60000,
							success: function (data) {
								$scope.$apply(function () {
									$scope.step         = data.step;
									$scope.spreadStages = data.stages;
									$scope.calculating  = false;
									$scope.downloading  = false;

									$scope.currentSpreadCalculation = 0;
									$scope.numberOfSpreadsMin = 0;
									$scope.numberOfSpreadsMax = ($scope.spreadStages.length - 1);
								});
							}
						});
					}
				}
			});
		}, 100);
    };


    /**
     * Filling up time intervals.
     *
     * @return void
     */
    $scope.editTimeIntervals = function () {
		var settings = angular.copy($scope.settings);
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/timeIntervalsModal.html',
            resolve: {
                customStages: {stages: $scope.customStages},
				stages: {stages: $scope.stages},
				timeSettings: settings
            },
            controller: 'TimeIntervalsModalController',
            size: 'lg'
        });

        modalInstance.result.then(function (data) {
            if (data) {
				$scope.stages = data;
            }
        });
    };


    /**
     * Edit specific person settings.
     *
     * @param Object setting Settings list
     *
     * @return void
     */
    $scope.editMissingPersonSettings = function (settings) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/missingPersonSettingsModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
                var backupSettings = angular.copy(settings);

                $modalScope.templates = $scope.templates;
                $modalScope.currentTemplate = $scope.templates[0];

                $modalScope.settings = settings;

                $modalScope.applyTemplate = function () {
                    var settingsCopy = angular.copy($modalScope.currentTemplate);
                    $modalScope.settings = settingsCopy;
                }

                $modalScope.deleteTemplate = function () {
                    api.delete({
                        path: "settings-templates/" + $modalScope.currentTemplate.id,
                        success: function (response) {
                            for (var i = 0; i < $scope.templates.length; i++) {
                                if ($scope.templates[i].id === $modalScope.currentTemplate.id) {
                                    $scope.templates.splice(i, 1);

                                    if ($modalScope.templates.length > 0) {
                                        $modalScope.currentTemplate = $modalScope.templates[0];
                                    }
                                }
                            }
                        }
                    });
                }


                $modalScope.saveTemplate = function () {
                    var data = {
                        name: $modalScope.templateName
                    };

                    if ($modalScope.settings.height) data.height = $modalScope.settings.height;
                    if ($modalScope.settings.weight) data.weight = $modalScope.settings.weight;
                    if ($modalScope.settings.speed) data.speed = $modalScope.settings.speed;
                    if ($modalScope.settings.tiredness) data.tiredness = $modalScope.settings.tiredness;
                    if ($modalScope.settings.hearing) data.hearing = $modalScope.settings.hearing;
                    if ($modalScope.settings.voice) data.voice = $modalScope.settings.voice;
                    if ($modalScope.settings.goal) data.goal = $modalScope.settings.goal;

                    api.post({
                        path: "settings-templates",
                        data: data,
                        success: function (response) {
                            data.id = response.id;
                            $scope.templates.push(data);

                            $modalScope.templateIsSaved = true;
                        }
                    });
                }

                $modalScope.onSuccess = function () {
                    $uibModalInstance.dismiss('cancel');
                }

                $modalScope.onCancel = function () {
                    $scope.settings = backupSettings;
                    $uibModalInstance.dismiss('cancel');
                }
            }]
        });
    };


    /**
     * Edit weather and light settings.
     *
     * @param Object setting Settings list
     *
     * @return void
     */
    $scope.editWeatherAndLightSettings = function (settings) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/weatherAndLightSettingsModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
                var backupSettings = angular.copy(settings);

                $modalScope.settings = settings;
                $modalScope.weather_condition = {};
				
				$modalScope.clearWeatherCondition = function() {
					$modalScope.weather_condition.time_from   = undefined;
                    $modalScope.weather_condition.time_to     = undefined;
                    $modalScope.weather_condition.temperature = undefined;
                    $modalScope.weather_condition.description = undefined;
				};
				
                $modalScope.addWeatherCondition = function () {
                    var weatherEntry = {
                        time_from: $modalScope.weather_condition.time_from,
                        time_to: $modalScope.weather_condition.time_to,
                        temperature: $modalScope.weather_condition.temperature,
                        description: $modalScope.weather_condition.description
                    };

                    $modalScope.settings.weather_conditions.push(weatherEntry);
                    $modalScope.weather_condition = {};
                };

                $modalScope.onSuccess = function () {
					$uibModalInstance.dismiss('cancel');
                };

                $modalScope.onCancel = function () {
                    $scope.settings = backupSettings;
                    $uibModalInstance.dismiss('cancel');
                };
            }]
        });
    };


    /**
     * Edit time settings.
     *
     * @param Object setting Settings list
     *
     * @return void
     */
    $scope.editTimeSettings = function (settings) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/timeSettingsModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
                var backupSettings = angular.copy(settings);

                $modalScope.settings = settings;
                $modalScope.weather_condition = {};
                $modalScope.addWeatherCondition = function () {
                    var weatherEntry = {
                        time_from: $modalScope.weather_condition.time_from,
                        time_to: $modalScope.weather_condition.time_to,
                        temperature: $modalScope.weather_condition.temperature,
                        description: $modalScope.weather_condition.description
                    };

                    $modalScope.settings.weather_conditions.push(weatherEntry);
                    $modalScope.weather_condition = {};
                };

                $modalScope.onSuccess = function () {
                    $uibModalInstance.dismiss('cancel');
                }

                $modalScope.onCancel = function () {
                    $scope.settings = backupSettings;
                    $uibModalInstance.dismiss('cancel');
                }
            }]
        });
    };
	
	$scope.$watch("currentSpreadCalculation", function(value) {
		if (value) {
			var minutes          = Math.floor($scope.currentSpreadCalculation * $scope.step / 60);
			$scope.displayedTime = moment($scope.settings.start_time).add((minutes - Math.floor($scope.step / 60)), 'm').format("DD.MM.YYYY HH:mm");
		}
	});
	
	
    $scope.changeCurrentSpreadCalculation = function (val) {
		if (val.index || val.index === 0) {
            if (val.outer) {
				$scope.$apply(function () {
					$scope.currentSpreadCalculation = val.index;
				});
			} else {
				$scope.currentSpreadCalculation = val.index;
			}

            var spreadStage = $scope.spreadStages[parseInt(val.index)];
            map.drawSpreadStage(spreadStage);
        }
    }
	
    $scope.nextStage = function (outer) {
		if ($scope.currentSpreadCalculation < $scope.numberOfSpreadsMax) {
			$scope.currentSpreadCalculation++;
			$scope.changeCurrentSpreadCalculation({index: $scope.currentSpreadCalculation, outer: outer});
		}
    };
    
    
    $scope.previousStage = function (outer) {
		if ($scope.currentSpreadCalculation > 0) {
			$scope.currentSpreadCalculation--;
			$scope.changeCurrentSpreadCalculation({index: $scope.currentSpreadCalculation, outer: outer});
		}
    };
	
	
	var playInterval;
	$scope.playbackStage = function () {
		$scope.playing = true;
		
		playInterval = setInterval(function() {
			$scope.previousStage(true);
			if ($scope.currentSpreadCalculation < 1) {
				clearInterval(playInterval);
				$scope.$apply(function() {
					$scope.playing = false;
				});
			}
		}, 50);
	};
	
	
	$scope.playStage = function () {
		$scope.playing = true;
		
		playInterval = setInterval(function() {
			$scope.nextStage(true);
			if ($scope.currentSpreadCalculation > ($scope.numberOfSpreadsMax - 1)) {
				clearInterval(playInterval);
				$scope.$apply(function() {
					$scope.playing = false;
				});
			}
		}, 50);
	};
	
	
	$scope.stopStage = function () {
		$scope.playing = false;
		clearInterval(playInterval);
	};
}]);