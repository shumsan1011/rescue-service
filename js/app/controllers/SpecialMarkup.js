app.controller("SpecialMarkupController", ["$rootScope", "$scope", "$uibModal", "$translate", "keeper", "api", "registry", "keeper",
  function($rootScope, $scope, $uibModal, $translate, keeper, api, registry, keeper) {

    $scope.templates = new Array();
    $scope.keeper = keeper;

	
	var _onClickFeatureCallback = function (e) {
		for (var i = 0; i < $scope.keeper.specialMarkup.length; i++) {
			if ($scope.keeper.specialMarkup[i].id === e.target.markupId) {
				$scope.$apply(function () {
					$scope.keeper.specialMarkup[i].active = true;
				});
			}
		}
	};
	
	
	var _onUnclickFeatureCallback = function (e) {
		for (var i = 0; i < $scope.keeper.specialMarkup.length; i++) {
			if ($scope.keeper.specialMarkup[i].id === e.target.markupId) {
				$scope.$apply(function () {
					$scope.keeper.specialMarkup[i].active = false;
				});
			}
		}
	};


    /**
     * Request server for the list of special markup.
     *
     * @return void
     */
    var _refreshSpecialMarkup = function () {
        if (registry.token && registry.currentSearchOperationId) {
            api.get({
                path: "special-markup?search_operation_id=" + registry.currentSearchOperationId,
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
						if (data[i].shk) data[i].shk = parseInt(data[i].shk);
						if (data[i].sp) data[i].sp = parseFloat(data[i].sp);
						if (data[i].skd) data[i].skd = parseFloat(data[i].skd);
						if (data[i].ovn) data[i].ovn = parseFloat(data[i].ovn);
					}
					
					$scope.keeper.specialMarkup = data;
                    map.addOnZoomLevelChange("specialMarkup", function () {
                        map.drawMarkupElements($scope.keeper.specialMarkup, _onClickFeatureCallback, _onUnclickFeatureCallback);
                    });
                }
            });
        }

        if (registry.token) {
            api.get({
                path: "special-markup-templates",
                success: function (data) {
                    $scope.templates = data;
                    $scope.currentTemplate = data[0];
                }
            });
        }
    }

    $scope.$watch("authorized", function (isAuthorized) {
		if (isAuthorized) _refreshSpecialMarkup();
	});
    $scope.$on("searchOperationChanged", _refreshSpecialMarkup);


    var _initiateEditingModal = function (type, wkt, item) {
        if (!item) {
            if (["polygon", "marker", "polyline"].indexOf(type) === -1) {
                throw "Invalid type of geometry is provided " + type;
            }
        }

        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/editSpecialMarkupModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
                $modalScope.errors = [];
				
				var backupItem;
                if (item) {
                    backupItem = angular.copy(item);

                    $modalScope.type = item.type;
                    $modalScope.description = item.description;
                    $modalScope.sp = item.sp;
                    $modalScope.skd = item.skd;
                    $modalScope.ovn = item.ovn;
                    $modalScope.shk = item.shk;

                    $modalScope.global = "1";
                    if (item.search_operation_id) {
                        $modalScope.global = "0";
                    }

                    $modalScope.ovn = item.ovn;
                } else {
                    $modalScope.type = type;
                    $modalScope.global = "0";
                    $modalScope.ovn = "0";
                }

                $modalScope.templates = $scope.templates;
                $modalScope.currentTemplate = $scope.templates[0];

                $modalScope.applyTemplate = function () {
                    var templateCopy = angular.copy($modalScope.currentTemplate);

                    $modalScope.description = templateCopy.description;
                    $modalScope.sp = templateCopy.sp;
                    $modalScope.skd = templateCopy.skd;
                    $modalScope.ovn = templateCopy.ovn;
                    $modalScope.shk = templateCopy.shk;
                }

                $modalScope.deleteTemplate = function () {
                    api.delete({
                        path: "special-markup-templates/" + $modalScope.currentTemplate.id,
                        success: function (response) {
                            for (var i = 0; i < $scope.templates.length; i++) {
                                if ($scope.templates[i].id === $modalScope.currentTemplate.id) {
                                    $scope.templates.splice(i, 1);

                                    if ($modalScope.templates.length > 0) {
                                        $modalScope.currentTemplate = $modalScope.templates[0];
                                    }
                                }
                            }
                        }
                    });
                }

                $modalScope.saveTemplate = function () {
                    var data = {
                        name: $modalScope.templateName
                    };

                    if ($modalScope.description) data.description = $modalScope.description;
                    if ($modalScope.sp) data.sp = $modalScope.sp;
                    if ($modalScope.skd) data.skd = $modalScope.skd;
                    if ($modalScope.ovn) data.ovn = $modalScope.ovn;
                    if ($modalScope.shk) data.shk = $modalScope.shk;

                    api.post({
                        path: "special-markup-templates",
                        data: data,
                        success: function (response) {
                            data.id = response.id;
                            $scope.templates.push(data);

                            $modalScope.templateIsSaved = true;
                        }
                    });
                }

                $modalScope.onSuccess = function () {
					$modalScope.errors = [];
					
					if ($(".js-sp").val() && (parseFloat($(".js-sp").val()) < 0 || parseFloat($(".js-sp").val()) > 1)) {
						$modalScope.errors.push($translate.instant("SP_ERROR_NOTIFICATION"));
					}
					
					if ($(".js-skd").val() && (parseFloat($(".js-skd").val()) < 0 || parseFloat($(".js-skd").val()) > 1)) {
						$modalScope.errors.push($translate.instant("SKD_ERROR_NOTIFICATION"));
					}
					
					if ($modalScope.errors.length === 0) {
						var data = {
							description: $modalScope.description,
							sp: $modalScope.sp,
							skd: $modalScope.skd,
							ovn: $modalScope.ovn,
							shk: $modalScope.shk,
							zs: $modalScope.zs
						};

						if ($modalScope.global === "0") {
							data.search_operation_id = registry.currentSearchOperationId;
						} else {
							data.search_operation_id = null;
						}

						if (item && item.id) {
							if (wkt && type === "marker") {
								data.geodata = wkt;
							}
							
							api.put({
								path: "special-markup/" + item.id,
								data: data,
								success: function () {
									for (var i = 0; i < $scope.keeper.specialMarkup.length; i++) {
										if ($scope.keeper.specialMarkup[i].id === item.id) {
											$scope.keeper.specialMarkup[i].description = data.description;
											$scope.keeper.specialMarkup[i].sp = data.sp;
											$scope.keeper.specialMarkup[i].skd = data.skd;
											$scope.keeper.specialMarkup[i].ovn = data.ovn;
											$scope.keeper.specialMarkup[i].shk = data.shk;
											
											if (data.geodata) {
												$scope.keeper.specialMarkup[i].geodata = data.geodata;
											}

											if ($modalScope.global === "0") {
												$scope.keeper.specialMarkup[i].search_operation_id = data.search_operation_id;
											} else {
												$scope.keeper.specialMarkup[i].search_operation_id = null;
											}
										}
									}
									
									map.disableDrawing();
									map.drawMarkupElements($scope.keeper.specialMarkup, _onClickFeatureCallback, _onUnclickFeatureCallback);
									$uibModalInstance.dismiss('cancel');
								}
							});
						} else {
							data.type = $modalScope.type;
							data.geodata = wkt;

							api.post({
								path: "special-markup",
								data: data,
								success: function (response) {
									$scope.collapsePanel(false);
									
									data.id = response.id;
									$scope.keeper.specialMarkup.push(data);
									
									map.disableDrawing();
									map.drawMarkupElements($scope.keeper.specialMarkup, _onClickFeatureCallback, _onUnclickFeatureCallback);
									$uibModalInstance.dismiss('cancel');
								}
							});
						}
					}
                }

                $modalScope.onCancel = function () {
                    map.disableDrawing();
                    $uibModalInstance.dismiss('cancel');
                }
            }]
        });
    }

	
	$scope.toggleMarkupZoneDisplay = function (specialMarkup) {
		for (var i = 0; i < $scope.keeper.specialMarkup.length; i++) {
			if ($scope.keeper.specialMarkup[i].id === specialMarkup.id) {
				if ($scope.keeper.specialMarkup[i].hideZone) {
					$scope.keeper.specialMarkup[i].hideZone = false;
				} else {
					$scope.keeper.specialMarkup[i].hideZone = true;
				}
			}
		}

		map.drawMarkupElements($scope.keeper.specialMarkup, _onClickFeatureCallback, _onUnclickFeatureCallback);
	};
	
	
	$scope.editFeature = function (specialMarkup) {
		map.editSpecialMarkupFeature(specialMarkup, function (geodata) {
			var data = {geodata: geodata};
			if (specialMarkup.search_operation_id) data.search_operation_id = registry.currentSearchOperationId;
			
			api.put({
				path: 'special-markup/' + specialMarkup.id,
				data: data,
				success: function () {
					for (var i = 0; i < $scope.keeper.specialMarkup.length; i++) {
						if ($scope.keeper.specialMarkup[i].id === specialMarkup.id) {
							$scope.keeper.specialMarkup[i].geodata = geodata;
						}
					}

					map.drawMarkupElements($scope.keeper.specialMarkup, _onClickFeatureCallback, _onUnclickFeatureCallback);
				}
			});
		}, function () { map.drawMarkupElements($scope.keeper.specialMarkup, _onClickFeatureCallback, _onUnclickFeatureCallback); });
	}
	

	$scope.editSpecialMarkupDisplay = function (specialMarkup) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/editFeatureDisplayModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
                
				$modalScope.color = specialMarkup.color;
				if ("opacity" in specialMarkup) {
					$modalScope.opacity = specialMarkup.opacity;
				} else {
					$modalScope.opacity = 0.5;
				}
				
                $modalScope.onSuccess = function () {
					specialMarkup.color = $modalScope.color;
					specialMarkup.opacity = $modalScope.opacity;
					
					var data = {display_style: JSON.stringify({color: $modalScope.color, opacity: $modalScope.opacity})};
					if (specialMarkup.search_operation_id) data.search_operation_id = registry.currentSearchOperationId;
					
					api.put({
						path: 'special-markup/' + specialMarkup.id,
						data: data,
						success: function () {
							for (var i = 0; i < $scope.keeper.specialMarkup.length; i++) {
								if ($scope.keeper.specialMarkup[i].id === specialMarkup.id) {
									$scope.keeper.specialMarkup[i].color = $modalScope.color;
									$scope.keeper.specialMarkup[i].opacity = $modalScope.opacity;
								}
							}
							
							map.drawMarkupElements($scope.keeper.specialMarkup, _onClickFeatureCallback, _onUnclickFeatureCallback);
							$uibModalInstance.close();
						}
					});
                }
				
				$modalScope.onCancel = function () {
					$uibModalInstance.close();
				}
            }],
			size: "sm"
        });
	};
	
	
    $scope.$watchCollection('keeper.specialMarkup', function(specialMarkup) {
        for (var i = 0; i < specialMarkup.length; i++) {
			if (specialMarkup[i].display_style) {
				var parsedStyle = JSON.parse(specialMarkup[i].display_style);
				if (parsedStyle.color) specialMarkup[i].color = parsedStyle.color;
				if (parsedStyle.opacity) specialMarkup[i].opacity = parsedStyle.opacity;
			}
			
            if (!specialMarkup[i].color) {
                specialMarkup[i].color = getRandomColor();
                specialMarkup[i].opacity = 0.5;
            }
        }

        map.drawMarkupElements(specialMarkup, _onClickFeatureCallback, _onUnclickFeatureCallback);
    });


    $scope.centerOnMarkup = function (item) {
        map.centerOnMarkup(item.id);
    };


    $scope.editMarkup = function (item) {
        _initiateEditingModal(false, false, item);
    };


    $scope.deleteMarkup = function (item) {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/confirmationModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
                $modalScope.text = $translate.instant("ARE_YOU_SURE_YOU_WANT_TO_DELETE") + " (" + item.description + ")";
                $modalScope.onSuccess = function () {
                    $uibModalInstance.close(true);
                    api.delete({
                        path: "special-markup/" + item.id,
                        success: function(data) {
                            for (var i = 0; i < $scope.keeper.specialMarkup.length; i++) {
                                if ($scope.keeper.specialMarkup[i].id === item.id) {
                                    $scope.keeper.specialMarkup.splice(i, 1);
                                }
                            }
                        }
                    });
                }

                $modalScope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }],
            size: 'sm'
        });
    };


    $scope.addSpecialMarkup = function () {
        map.enableDrawing(_initiateEditingModal);
    };
	
	
	$scope.editMarkupByCoordinates = function (item) {
		var lat, lon;
		if (item) {
			lat = parseFloat(item.geodata.replace("POINT(", "").replace(")").split(" ")[1]);
			lon = parseFloat(item.geodata.replace("POINT(", "").replace(")").split(" ")[0]);
		}

		var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/enterSpecialMarkupModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
				$modalScope.mode = "dd";

				$modalScope.disabled = true;
				var _check = function() {
					$modalScope.disabled = true;

					if ($modalScope.mode === "dd" && $modalScope.dd_latitude && $modalScope.dd_longitude) {
						$modalScope.disabled = false;
						return;
					}

					if ($modalScope.mode === "dmd" && $modalScope.dmd_latitude && $modalScope.dmd_longitude) {
						$modalScope.disabled = false;
						return;
					}

					if ($modalScope.mode === "dms" && $modalScope.dms_latitude && $modalScope.dms_longitude) {
						$modalScope.disabled = false;
						return;
					}
				}

				$modalScope.$watch("dd_latitude", _check);
				$modalScope.$watch("dd_longitude", _check);
				$modalScope.$watch("dmd_latitude", _check);
				$modalScope.$watch("dmd_longitude", _check);
				$modalScope.$watch("dms_latitude", _check);
				$modalScope.$watch("dms_longitude", _check);
				$modalScope.$watch("mode", _check);

				if (lat && lon) {
					$modalScope.dd_latitude  = lat;
					$modalScope.dd_longitude = lon;
					
					$modalScope.dmd_latitude  = Dms.toLat(lat, 'dm', 2);
					$modalScope.dmd_longitude = Dms.toLon(lon, 'dm', 2);
					
					$modalScope.dms_latitude  = Dms.toLat(lat, 'dms', 0);
					$modalScope.dms_longitude = Dms.toLon(lon, 'dms', 0);
				}

                $modalScope.onSuccess = function () {
					var lat, lon;
					if ($modalScope.mode === "dd") {
						lat = $modalScope.dd_latitude;
						lon = $modalScope.dd_longitude;
					} else if ($modalScope.mode === "dmd") {
						lat = Dms.parseDMS($modalScope.dmd_latitude);
						lon = Dms.parseDMS($modalScope.dmd_longitude);
					} else if ($modalScope.mode === "dms") {
						lat = Dms.parseDMS($modalScope.dms_latitude);
						lon = Dms.parseDMS($modalScope.dms_longitude);
					} else {
						throw "Unable to detect type";
					}

					_initiateEditingModal("marker", "POINT(" + lon + " " + lat + ")", item);
					$uibModalInstance.close(true);
                }

                $modalScope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }]
        });
	};
}]);