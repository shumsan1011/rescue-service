app.controller("SearchOperationsController", ["$rootScope", "$scope", "$uibModal", "$translate", "registry", "api", function($rootScope, $scope, $uibModal, $translate, registry, api) {

    $scope.ready = false;
    $scope.editing = false;
    $scope.currentSearchOperation = false;
    $scope.searchOperations = new Array();
    var _backup = {};

	$rootScope.setInterface();
	
    /**
     * Request list of current search operations from server.
     *
     * @return void
     */
    var _refreshSearchOperations = function () {
		api.get({
            path: "searchoperations",
            success: function (data) {
                var currentSearchOperationIndex = 0;

				for (var i = 0; i < data.length; i++) {
					if (data[i].operation_accident_date) data[i].operation_accident_date = moment(data[i].operation_accident_date).format();
					if (data[i].operation_beginning_date) data[i].operation_beginning_date = moment(data[i].operation_beginning_date).format();
				}

				$scope.ready = true;
                if (registry.currentSearchOperationId) {
                    var currentSearchOperationEncountered = false;

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].id === parseInt(registry.currentSearchOperationId)) {
                            currentSearchOperationIndex = i;
                            currentSearchOperationEncountered = true;
                            break;
                        }
                    }

                    if (currentSearchOperationEncountered === false) {
                        if (data.length > 0) {
                            registry.currentSearchOperationId = data[0].id;
                        } else {
                            registry.currentSearchOperationId = null;
                        }
                    }
                } else {
                    if (data.length > 0) {
                        registry.currentSearchOperationId = data[0].id;
                    }
                }

                $scope.searchOperations = data;
                $scope.currentSearchOperation = data[currentSearchOperationIndex];
                $scope.setCurrentSearchOperation();
            }
        });
    }

    $scope.$watch("authorized", function (isAuthorized) {
		if (isAuthorized) _refreshSearchOperations();
	});

    /**
     * Setup environment for selected search operation.
     *
     * @return void
     */
    $scope.setCurrentSearchOperation = function () {
        if ($scope.currentSearchOperation) {
            if ($scope.currentSearchOperation.center_coordinates) {
                map.setSearchOperationCenter($scope.currentSearchOperation.center_coordinates, $translate.instant("SEARCH_OPERATION_CENTER"));
            }

            registry.currentSearchOperationId = $scope.currentSearchOperation.id;
            $scope.$broadcast("searchOperationChanged");
        }
    };


    /**
     * Edit current search operation.
     *
     * @return void
     */
    $scope.editSearchOperation = function () {
        _backup = angular.copy($scope.currentSearchOperation);

        $scope.editing = true;
    };


    /**
     * Create search operation.
     *
     * @return void
     */
    $scope.createSearchOperation = function () {
        map.setSearchOperationCenter();

        $scope.editing = true;
        $scope.currentSearchOperation = {};
    };


    /**
     * Delete current search operation.
     *
     * @return void
     */
    $scope.deleteSearchOperation = function () {
        var deletedId = $scope.currentSearchOperation.id;

        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/confirmationModal.html',
            controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                $scope.text = $translate.instant("ARE_YOU_SURE_YOU_WANT_TO_DELETE") + "?";
                $scope.onSuccess = function () {
                    $uibModalInstance.close(true);
                }

                $scope.onCancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }],
            size: 'sm'
        });

        modalInstance.result.then(function (hasToBeDeleted) {
            if (hasToBeDeleted) {
                api.delete({
                    path: "searchoperation/" + deletedId,
                    success: function(data) {
                        for (var i = 0; i < $scope.searchOperations.length; i++) {
                            if ($scope.searchOperations[i].id === deletedId) {
                                $scope.searchOperations.splice(i, 1);

                                $scope.currentSearchOperation = $scope.searchOperations[0];
                                $scope.setCurrentSearchOperation();
                                break;
                            }
                        }
                    }
                });
            }
        });
    }


    /**
     * Import search operation from outer.
     *
     * @return void
     */
	$scope.importSearchOperation = function () {
        var modalInstance = $uibModal.open({
            templateUrl: $rootScope.templateBase + 'modals/importSearchOperationModal.html',
            controller: ["$scope", "$uibModalInstance", function ($modalScope, $uibModalInstance) {
				$modalScope.importSearchOperationById = function () {
					if ($modalScope.identifier) {
						$modalScope.loading = true;
						api.post({
							path: "searchoperations/import",
							data: {
								id: $modalScope.identifier
							},
							success: function (data) {
								$modalScope.loading = false;
								if (data.id) {
									$modalScope.successMessage = $translate.instant("SEARCH_OPERATION_WAS_IMPORTED") + data.name;
									_refreshSearchOperations();
								}
							},
							error: function (status, data, handler) {
								$modalScope.loading = false;
								if (status === 400) {
									$modalScope.errorMessage = $translate.instant(data.error);
								} else {
									handler();
								}
							}
						})
						
						
					} else {
						throw "Invalid identifier"
					}
				}
				
				
				var _leaveModal = function () {
					$uibModalInstance.dismiss('cancel');
				};
				
				$modalScope.onOk = _leaveModal;
                $modalScope.onSuccess = _leaveModal;
                $modalScope.onCancel = _leaveModal;
            }],
            //size: 'sm'
        });
	};
	
	
    /**
     * Save new search operation.
     *
     * @return void
     */
    $scope.saveSearchOperation = function () {
		
		c($scope.currentSearchOperation);
		//return;
		
		var errorCodes = [];
		if ($scope.currentSearchOperation.operation_accident_date) {
			if (moment($scope.currentSearchOperation.operation_beginning_date).isBefore($scope.currentSearchOperation.operation_accident_date)) {
				errorCodes.push("ERROR_OPERATION_BEGINNING_DATE_IS_EARLIER_THAN_ACCIDENT_DATE");
			}
		}

        if ($scope.searchOperationForm.$valid && errorCodes.length === 0) {
            var data = {
                location_region: $scope.currentSearchOperation.location_region,
                location_district: $scope.currentSearchOperation.location_district,
                location_locality: $scope.currentSearchOperation.location_locality,
                operation_beginning_date: $scope.currentSearchOperation.operation_beginning_date
            };

            if ($scope.currentSearchOperation.operation_accident_date) data.operation_accident_date = $scope.currentSearchOperation.operation_accident_date;
            if ($scope.currentSearchOperation.organization) data.organization = $scope.currentSearchOperation.organization;
            if ($scope.currentSearchOperation.leader) data.leader = $scope.currentSearchOperation.leader;
            if ($scope.currentSearchOperation.description) data.description = $scope.currentSearchOperation.description;
            if ($scope.currentSearchOperation.center_coordinates) data.center_coordinates = $scope.currentSearchOperation.center_coordinates;
            if ($scope.currentSearchOperation.index_number) data.index_number = $scope.currentSearchOperation.index_number;

            if ($scope.currentSearchOperation.id) {
                api.put({
                    path: "searchoperation/" + $scope.currentSearchOperation.id,
                    data: data,
                    success: function (data) {
                        $scope.editing = false;
                        $scope.setCurrentSearchOperation();
                    }
                });
            } else {
                api.post({
                    path: "searchoperations",
                    data: data,
                    success: function (data) {
                        $scope.currentSearchOperation.id = data.id;
                        $scope.searchOperations.push($scope.currentSearchOperation);
                        $scope.editing = false;

                        registry.currentSearchOperationId = data.id;
                        $scope.setCurrentSearchOperation();
                        $scope.$broadcast("searchOperationChanged");
                    }
                });
            }
        } else {
			var modalInstance = $uibModal.open({
				templateUrl: $rootScope.templateBase + 'modals/errorModal.html',
				controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
					$scope.errorCodes = errorCodes;
					
					$scope.onSuccess = function () {
						$uibModalInstance.close(true);
					}
				}]
			});
		}
    };


    /**
     * Cancel any search operation form editing.
     *
     * @return void
     */
    $scope.cancelEditing = function () {
        if (!$scope.currentSearchOperation.id) {
            $scope.currentSearchOperation = $scope.searchOperations[0];
        }

        for (var i = 0; i < $scope.searchOperations.length; i++) {
            if ($scope.searchOperations[i].id === _backup.id) {
                $scope.searchOperations[i] = _backup;
                $scope.currentSearchOperation = $scope.searchOperations[i];
                break;
            }
        }

        $scope.setCurrentSearchOperation();
        $scope.editing = false;
    };
}]);