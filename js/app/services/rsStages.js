/**
 * Constructs stages based on settings and custom time periods.
 */
angular.module("rescueApp").service("rsStages", ["$translate", function($translate) {

    /**
     * Validates and populates final stages for future computations.
     */
    var StageKeeper = function (settings) {
        var _stages = new Array();
		
		var _speed = false;
		if (settings) {
			if (settings.speed) {
				_speed = parseFloat(settings.speed);
			} else {
				throw "Provide speed setting for the StageKeeper";
			}
		} else {
			throw "Provide correct settings object for the StageKeeper";
		}
		

        /**
         * Creates stage record.
         *
         * @param Object item Stage description
         *
         * @return void
         */
        this.create = function (item) {
            if (!item.time_from || !item.time_to || !item.description) {
                console.log(item);
                throw "Invalid data was provided at the stage creation, see console log";
            } else {
                item.tiredness = 1;
                item.daylight = 1;
                item.weather = 1;
                item.group = 1;
                item.other = 1;
                item.speed = _speed;

                _stages.push(item);
            }
        };

        /**
         * Returns saved stages.
         *
         * @return Array
         */
        this.getStages = function () {
            for (var i = 0; i < _stages.length; i++) {
                _stages[i].time_from = moment.unix(_stages[i].time_from).format();
                _stages[i].time_to = moment.unix(_stages[i].time_to).format();
            }

            return _stages;
        };
    };

    /**
     * Returns base day light periods, based only on day light settings.
     *
     * @param Number startTime Starting time in Unix timestamp
     * @param Number endTime   End time in Unix timestamp
     * @param Number sunrise   Sun rise time in Unix timestamp
     * @param Number sunset    Sun set time in Unix timestamp
     * @param Number moonrise  Moon rise time in Unix timestamp
     * @param Number moonset   Moon set time in Unix timestamp
     *
     * @return Array
     */
    var _getDayLightPeriods = function (startTime, endTime, sunrise, sunset, moonrise, moonset) {
		var periods = new Array();

        var currentTime = moment.unix(startTime).add(-1, "d");
        while (currentTime.format("DD.MM.YYYY") !== moment.unix(endTime).add(1, "d").format("DD.MM.YYYY")) {
            var sunRiseTime = currentTime;
            sunRiseTime.set({
                hour: moment.unix(sunrise).format("HH"),
                minute: moment.unix(sunrise).format("mm")
            });

            periods.push({
                type: 'sunrise',
                time_from: toTimestamp(sunRiseTime.format()),
                description: $translate.instant('DAY')
            });

            var sunSetTime = currentTime;
            sunSetTime.set({
                hour: moment.unix(sunset).format("HH"),
                minute: moment.unix(sunset).format("mm")
            });

            periods.push({
                type: 'sunset',
                time_from: toTimestamp(sunSetTime.format()),
                description: $translate.instant('NIGHT'),
                type_specific_speed_modifier: 0
            });

            var moonRiseTime = currentTime;
            moonRiseTime.set({
                hour: moment.unix(moonrise).format("HH"),
                minute: moment.unix(moonrise).format("mm")
            });

            periods.push({
                type: 'moonrise',
                time_from: toTimestamp(moonRiseTime.format()),
                description: $translate.instant('NIGHT_MOON_RISE'),
                type_specific_speed_modifier: 0.2
            });

            var moonSetTime = currentTime;
            moonSetTime.set({
                hour: moment.unix(moonset).format("HH"),
                minute: moment.unix(moonset).format("mm")
            });

            periods.push({
                type: 'moonset',
                time_from: toTimestamp(moonSetTime.format()),
                description: $translate.instant('NIGHT'),
                type_specific_speed_modifier: 0
            });

            currentTime = currentTime.add(1, "d");
        }

        var sorted = new Array();
        while (periods.length > 0) {
            var earliestRecordIndex = false;
            var earliestRecord = false;
            for (var i = 0; i < periods.length; i++) {
                if (earliestRecord === false) {
                    earliestRecord = periods[i].time_from;
                    earliestRecordIndex = i;
                } else if (earliestRecord > periods[i].time_from) {
                    earliestRecord = periods[i].time_from;
                    earliestRecordIndex = i;
                }
            }

            var extractedPeriod = periods.splice(earliestRecordIndex, 1)[0];
            sorted.push(extractedPeriod);
        }

        for (var i = 0; i < sorted.length; i++) {
            if (i > 1) {
                sorted[i - 1].time_to = sorted[i].time_from;
            }
        }

        sorted[0].time_to = sorted[1].time_from;

		for (var i = 0; i < sorted.length; i++) {
			if (sorted[i].time_from === sorted[i].time_to) {
				sorted.splice(i, 1);
			}
		}
		
		sorted[sorted.length - 1].time_to = endTime;
		
        return sorted;
    };


    /**
     * Constructs readable description based on stage parameters.
     *
     * @param Object condition Condition object
     *
     * @return String
     */
    var _getDescription = function (condition) {
        var description = "";
        if (condition.temperature) {
            description = condition.description + "(" + condition.temperature + " C)";
        } else {
            description = condition.description;
        }

        return description;
    };


    /**
     * Recursively walks down stages and splits them based on user-defined time periods.
     *
     * @param Object parentStage       Stage-container
     * @param Array  weatherConditions User-defined conditions
     *
     * @return Array
     */
    var _getSplittedStages = function (parentStage, weatherConditions) {
        var intersectionCondition = false;
        for (var j = 0; j < weatherConditions.length; j++) {
            var condition = weatherConditions[j];
            if (condition.time_from <= parentStage.time_to && condition.time_to > parentStage.time_from) {
                intersectionCondition = weatherConditions.splice(j, 1)[0];
                break;
            }
        }

		if (!parentStage.time_from || !parentStage.time_to) {
			console.log(parentStage);
			throw "Broken parent stage";
		}
		
        var stages = new Array();
        if (intersectionCondition) {
            var localStages = new Array();
            if (intersectionCondition.time_from <= parentStage.time_from) {
                if (intersectionCondition.time_to < parentStage.time_to) {
                    localStages.push({
                        time_from: parentStage.time_from,
                        time_to: intersectionCondition.time_to,
                        description: parentStage.description + ", " + _getDescription(intersectionCondition),
                        type_specific_speed_modifier: parentStage.type_specific_speed_modifier
                    });

                    localStages.push({
                        time_from: intersectionCondition.time_to,
                        time_to: parentStage.time_to,
                        description: parentStage.description,
                        type_specific_speed_modifier: parentStage.type_specific_speed_modifier
                    });
                } else {
                    localStages.push({
                        time_from: parentStage.time_from,
                        time_to: parentStage.time_to,
                        description: parentStage.description + ", " + _getDescription(intersectionCondition),
                        type_specific_speed_modifier: parentStage.type_specific_speed_modifier
                    });
                }
            } else if (intersectionCondition.time_from > parentStage.time_from) {
                localStages.push({
                    time_from: parentStage.time_from,
                    time_to: intersectionCondition.time_from,
                    description: parentStage.description
                });

                if (intersectionCondition.time_to < parentStage.time_to) {
                    localStages.push({
                        time_from: intersectionCondition.time_from,
                        time_to: intersectionCondition.time_to,
                        description: parentStage.description + ", " + _getDescription(intersectionCondition),
                        type_specific_speed_modifier: parentStage.type_specific_speed_modifier
                    });

                    localStages.push({
                        time_from: intersectionCondition.time_to,
                        time_to: parentStage.time_to,
                        description: parentStage.description,
                        type_specific_speed_modifier: parentStage.type_specific_speed_modifier
                    });
                } else {
                    localStages.push({
                        time_from: intersectionCondition.time_from,
                        time_to: parentStage.time_to,
                        description: parentStage.description + ", " + _getDescription(intersectionCondition),
                        type_specific_speed_modifier: parentStage.type_specific_speed_modifier
                    });
                }
            }

            for (var i = 0; i < localStages.length; i++) {
                var weatherConditionsCopy = angular.copy(weatherConditions);

                var underlyingStages = _getSplittedStages(localStages[i], weatherConditionsCopy);
                for (var j = 0; j < underlyingStages.length; j++) {
                    stages.push(underlyingStages[j]);
                }
            }
        } else {
			stages.push({
                time_from: parentStage.time_from,
                time_to: parentStage.time_to,
                description: parentStage.description,
                type_specific_speed_modifier: parentStage.type_specific_speed_modifier
            });
        }

        return stages;
    }


    /**
     * Generates stages based on various settings.
     *
     * @param Object settings Stage generation settings
     *
     * @return Array
     */
    this.generate = function (settings) {
        var stageKeeper = new StageKeeper({speed: settings.speed});

        var dayLightPeriods = _getDayLightPeriods(settings.start_time, settings.finish_time, settings.sunrise_time,
        settings.sunset_time, settings.moonrise_time, settings.moonset_time);

        for (var i = 0; i < dayLightPeriods.length; i++) {
            if (dayLightPeriods[i].time_to < settings.start_time) {
                dayLightPeriods.splice(i, 1);
                i--;
            } else if (settings.start_time >= dayLightPeriods[i].time_from && settings.start_time <= dayLightPeriods[i].time_to) {
                dayLightPeriods[i].time_from = settings.start_time;
            } else if (settings.finish_time > dayLightPeriods[i].time_from && settings.finish_time <= dayLightPeriods[i].time_to) {
                dayLightPeriods[i].time_to = settings.finish_time;
            } else if (dayLightPeriods[i].time_from > settings.finish_time) {
                dayLightPeriods.splice(i, 1);
                i--;
            }
        }

        for (var i = 0; i < dayLightPeriods.length; i++) {
            if (dayLightPeriods[i].time_from === dayLightPeriods[i].time_to) {
                dayLightPeriods.splice(i, 1);
            }
        }

        for (var i = 0; i < dayLightPeriods.length; i++) {
            var weatherConditionsCopy = angular.copy(settings.weather_conditions);
			
            var splittedStages = _getSplittedStages(dayLightPeriods[i], weatherConditionsCopy);
            for (var j = 0; j < splittedStages.length; j++) {
				stageKeeper.create(splittedStages[j]);
            }
        }

        var stages = stageKeeper.getStages();
        for (var i = 0; i < (stages.length - 1); i++) {
            if (stages[i].time_to !== stages[(i + 1)].time_from) {
                throw "Invalid stage was generated";
            }
        }

        for (var i = 0; i < stages.length; i++) {
            if (stages[i].type_specific_speed_modifier !== undefined) {
                stages[i].daylight = stages[i].type_specific_speed_modifier;
                stages[i].speed = stages[i].speed * stages[i].type_specific_speed_modifier;
            }
        }
      
        return stages;
    };

}]);
