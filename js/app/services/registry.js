angular.module("rescueApp").service("registry", ["$localStorage", "$sessionStorage", "$state", function(e, t, n) {
    function i(e) {
        for (var t = 0, n = o.length; n > t; t++) {
            if (angular.isString(o[t]) && o[t] === e) return !0;
            if (o[t] instanceof RegExp && o[t].test(e)) return !0
        }
        return !1
    }

    var o = ["language", "companyId", /^filters:/];
    this.setProperty = function(n, i, o) {
        o ? t[n] = i : e[n] = i
    }, this.removeProperty = function(n) {
        delete t[n], delete e[n]
    };
    var r = function(n) {
            return t[n] || e[n]
        },
        a = function(t, n) {
            e[t] = n
        };
    this.clear = function() {
        var t = function(e) {
            for (key in e) angular.isFunction(e[key]) || i(key) || delete e[key]
        };
        t(e)
    };
    var s = this,
        c = function(e) {
            Object.defineProperty(s, e, {
                set: function(t) {
                    a(e, t)
                },
                get: function() {
                    return r(e)
                }
            })
        };
    c("token"),c("userId"),c("userName"),c("currentSearchOperationId"),c("languageId"),c("mapCenter"),c("mapZoom"),c("dashboardWidth")
}]);