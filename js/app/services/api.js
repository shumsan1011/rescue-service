angular.module("rescueApp").service("api", ["$http", "registry", "Upload", function(e, n, t) {
    this.baseUrl = "/api/";

    this.defaultErrorHandler = function(e, t, n, i, o, r) {};

    this.get = function(e) {
        e.method = "GET", this.call(e)
    };

    this.post = function(e) {
        e.method = "POST", this.call(e)
    };

    this.put = function(e) {
        e.method = "PUT", this.call(e)
    };

    this["delete"] = function(e) {
        e.data && (e.headers = {
            "Content-Type": "application/json;charset=utf-8"
        }), e.method = "DELETE", this.call(e)
    };


    this.fileDownload = function (path) {
        $.fileDownload(this.baseUrl + path + "&auth=" + n.token);
    }

    this.upload = function(e) {
        var nn = this.defaultErrorHandler;

        t.upload({
            url: this.baseUrl + e.path,
            file: e.file,
            headers: {
                Auth: n.token
            }
        }).success(function(t, n, i, o) {
            e.success && e.success(t, n, i, o)
        }).error(function(t, i, o, r) {
            e.error ? e.error(t, i, function() {
                nn(i, t, r)
            }) : nn(i, t, r)
        })
    };

    this.call = function(t) {
        var i = function(e) {
            t.success && t.success(e.data)
        },
        o = function(e) {
            var n = t.method,
                i = e.config.url,
                o = i.split("?")[1],
                a = t.data,
                s = e.status,
                c = e.data;
            t.error ? t.error(e.status, e.data, function() {
                r(n, i, o, a, s, c)
            }) : r(n, i, o, a, s, c)
        },
        r = this.defaultErrorHandler;

        t.headers || (t.headers = {}), n.token && (t.headers.Auth = n.token), e({
            method: t.method,
            url: this.baseUrl + t.path,
            data: t.data,
            headers: t.headers
        }).then(i, o)
    }
}]);