angular.module("rescueApp").service("rsSettings", [function() {
    this.defaultMissingPersonSpeed = 5;
    this.defaultMissingPersonHeight = 180;
    this.defaultMissingPersonWeight = 80;
}]);