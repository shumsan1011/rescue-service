<?php

ini_set('xdebug.var_display_max_depth', 16);
ini_set('xdebug.var_display_max_children', 1024);
ini_set('xdebug.var_display_max_data', 4096);

/**
 * Class for testing API service.
 *
 * @author Alexander Shumilov <alexshumilov@yahoo.com>
 */
class AppTest extends PHPUnit_Framework_TestCase {

	/**
	 * Testing authorization.
	 *
	 * @return void
	 */
	public function testAuthorization() {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"localhost/api/auth");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(400, $code);

		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"email"    => "wrong@a.co",
			"password" => "1234"
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(401, $code);

		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"email"    => "a@a.co",
			"password" => "1234"
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$this->assertEquals("John", $parsedResponse["name"]);
		$this->assertEquals("1", $parsedResponse["id"]);
		$this->assertEquals(32, strlen($parsedResponse["token"]));

		curl_close ($ch);
    } //end testAuthorization()


	/**
	 * Testing search operations management.
	 *
	 * @return void
	 */
	public function testSearchOperations() {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_URL,"localhost/api/auth");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"email"    => "a@a.co",
			"password" => "1234"
		)));

		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$token = $parsedResponse["token"];

		// Trying to create route unauthorized.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperations");
		curl_setopt($ch, CURLOPT_POST, 1);
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(401, $code);

		// Creating route with malformed request.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperations");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Auth: " . $token));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(500, $code);

		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperations");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"location" => "Test city"
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(500, $code);

		// Creating search operation.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperations");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"location_region" => "Test region",
			"location_district" => "Test district",
			"location_locality" => "Test locality",
			"operation_beginning_date" => "2016-01-19T16:07:37+00",
			"operation_activity_date" => "2016-02-19T16:07:37+00",
			"leader" => "John Parker"
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$this->assertTrue(isset($parsedResponse["id"]));

		$id = intval($parsedResponse["id"]);
		$this->assertTrue($id > 0);

		// Updating search operation.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperation/" . $id);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"location_region" => "Test new region",
			"location_district" => "Test new district",
			"location_locality" => "Test new locality",
			"operation_beginning_date" => "2016-03-19T16:07:37+08",
			"operation_accident_date" => "2016-04-19T16:07:37+08",
			"leader" => "John Parker New Name"
		)));
		$server_output = curl_exec($ch);

		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$this->assertEquals("OK", $server_output);

		$id = intval($parsedResponse["id"]);
		$this->assertTrue($id > 0);

		// Get specific search operation.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperation/" . $id);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

		$server_output = curl_exec($ch);

		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$this->assertEquals($id, $parsedResponse[0]["id"]);
		$this->assertEquals("2016-03-19 08:07:37+00", $parsedResponse[0]["operation_beginning_date"]);
		$this->assertEquals("2016-04-19 08:07:37+00", $parsedResponse[0]["operation_accident_date"]);
		$this->assertEquals("John Parker New Name", $parsedResponse[0]["leader"]);

		// Delete specific search operation.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperation/" . $id);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$this->assertEquals("OK", $server_output);
	} //end testSearchOperations()
	
	
	/**
	 * Testing search operations import.
	 *
	 * @return void
	 */
	public function testSearchOperationsImport() {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_URL,"localhost/api/auth");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"email"    => "a@a.co",
			"password" => "1234"
		)));

		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$token = $parsedResponse["token"];
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Auth: " . $token));

		// Importing search operation.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperations/import");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"id" => 1423,
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		$this->assertEquals(200, $code);
		$outputParsed = json_decode($server_output, true);
		$id = intval($outputParsed["id"]);
		$this->assertTrue($id > 0);

		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperations/import");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"id" => 1473,
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
	} //end testSearchOperationsImport()
	

	/**
	 * Testing missing people management.
	 *
	 * @return void
	 */
	public function testMissingPeople() {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_URL,"localhost/api/auth");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"email"    => "a@a.co",
			"password" => "1234"
		)));

		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$token = $parsedResponse["token"];
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Auth: " . $token));

		// Creating search operation.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperations");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"location_region" => "Test region",
			"location_district" => "Test district",
			"location_locality" => "Test locality",
			"operation_beginning_date" => "2016-01-19T16:07:37+00",
			"operation_activity_date" => "2016-02-19T16:07:37+00",
			"leader" => "John Parker"
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$this->assertTrue(isset($parsedResponse["id"]));

		$searchOperationId = intval($parsedResponse["id"]);
		$this->assertTrue($searchOperationId > 0);

		// Creating missing person.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/missing");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"name" => "Lost John",
			"birth_year" => 1991,
			"search_operation_id" => $searchOperationId,
			"sex" => 1
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$this->assertTrue(isset($parsedResponse["id"]));

		$id = intval($parsedResponse["id"]);
		$this->assertTrue($id > 0);

		// Updating missing person.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/missing/" . $id);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"name" => "Lost John New Name",
			"birth_year" => 1990,
			"sex" => 0
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$this->assertEquals("OK", $server_output);

		// Retrieving missing people.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/missing?search_operation_id=100000");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

		$server_output = curl_exec($ch);
		$this->assertEquals("[]", $server_output);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);

		curl_setopt($ch, CURLOPT_URL,"localhost/api/missing?search_operation_id=" . $searchOperationId);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);

		$this->assertEquals($id, $parsedResponse[0]["id"]);
		$this->assertEquals("Lost John New Name", $parsedResponse[0]["name"]);
		$this->assertEquals(1990, $parsedResponse[0]["birth_year"]);
		$this->assertEquals(0, $parsedResponse[0]["sex"]);

		// Deleting specific missing person.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/missing/" . $id);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
	} //end testMissingPeople()

	
	/**
	 * Testing route management.
	 *
	 * @return void
	 */
	public function testRoutes() {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_URL,"localhost/api/auth");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"email"    => "a@a.co",
			"password" => "1234"
		)));

		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$token = $parsedResponse["token"];

		// Trying to create route unauthorized.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/routes");
		curl_setopt($ch, CURLOPT_POST, 1);
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(401, $code);

		// Creating route with malformed request.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/routes");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Auth: " . $token));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(400, $code);

		// Creating search operation.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperations");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"location_region" => "Test region",
			"location_district" => "Test district",
			"location_locality" => "Test locality",
			"operation_beginning_date" => "2016-01-19T16:07:37+00",
			"operation_activity_date" => "2016-02-19T16:07:37+00",
			"leader" => "John Parker"
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$this->assertTrue(isset($parsedResponse["id"]));

		$searchOperationId = intval($parsedResponse["id"]);
		$this->assertTrue($searchOperationId > 0);

		// Creating route.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/routes");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"geodata"             => "LINESTRING(30 10, 10 30, 40 40)",
			"participants"        => "John, Peter",
			"search_operation_id" => $searchOperationId
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$this->assertTrue(isset($parsedResponse["id"]));

		$id = $parsedResponse["id"];

		// Renaming route.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/route/" . $id);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"participants" => "John, Peter, Derek"
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$this->assertEquals("OK", $server_output);

		// Getting all routes.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/routes");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		$server_output = curl_exec($ch);

		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$occured = false;
		for ($i = 0; $i < count($parsedResponse); $i++) {
			if ($parsedResponse[$i]["id"] === $id) {
				$this->assertEquals("John, Peter, Derek", $parsedResponse[$i]["participants"]);
				$occured = true;
				break;
			}
		}

		$this->assertTrue($occured);

		// Deleting route.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/route/" . $id);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$this->assertEquals("OK", $server_output);

		// Getting all routes.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/routes");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		$server_output = curl_exec($ch);

		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$occured = false;
		for ($i = 0; $i < count($parsedResponse); $i++) {
			if ($parsedResponse[$i]["id"] === $id) {
				$occured = true;
				break;
			}
		}

		$this->assertFalse($occured);
		curl_close ($ch);
    }
	

	/**
	 * Testing route importing.
	 *
	 * @return void
	 */
	public function testRoutesImport() {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_URL,"localhost/api/auth");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"email"    => "a@a.co",
			"password" => "1234"
		)));

		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$token = $parsedResponse["token"];

		// Creating search operation.
		curl_setopt($ch, CURLOPT_URL,"localhost/api/searchoperations");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Auth: " . $token));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			"location_region" => "Test region",
			"location_district" => "Test district",
			"location_locality" => "Test locality",
			"operation_beginning_date" => "2016-01-19T16:07:37+00",
			"operation_activity_date" => "2016-02-19T16:07:37+00",
			"leader" => "John Parker"
		)));
		$server_output = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$this->assertEquals(200, $code);
		$parsedResponse = json_decode($server_output, true);
		$this->assertTrue(isset($parsedResponse["id"]));

		$searchOperationId = intval($parsedResponse["id"]);
		$this->assertTrue($searchOperationId > 0);
	}

}

?>
