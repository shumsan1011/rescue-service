# Система моделирования поисковой операции #


Руководство пользователя и описание системы:

[https://gitlab.com/shumsan1011/rescue-service/tree/master/docs/manual](https://gitlab.com/shumsan1011/rescue-service/tree/master/docs/manual "Руководство пользователя")

По всем вопросам и предложениям обращаться на shumsan1011@gmail.com

Демонстрационный вариант развернут по адресу [http://rsdemo.alexshumilov.ru/](http://rsdemo.alexshumilov.ru/)


### Установка ###


Для развертки системы необходимо следующее программное и аппаратное обеспечение:

- Linux 3.10
- NodeJS v0.10.42
- PHP 5.5.33
- PostgreSQL 9.4 + PostGIS extension
- 500 MB HDD, 1GB RAM
- Open 80 / 443 port


Пример консольного скрипта для развертки приложения:

    rm -R -f /var/www/html/*
    wget https://gitlab.com/shumsan1011/rescue-service/repository/archive.zip
    unzip archive.zip
    cp -R rescue-service-*/* /var/www/html
    rm -R rescue-service-*
    rm archive.zip
    chmod 0777 -R /var/www/html/api/logs
    rm -R /var/www/html/api/vendor
    cd /var/www/html/api
    composer install
    cd /var/www/html/computation
    npm install
    mkdir log
    chmod 775 log
    sudo chown ext:apache log
    echo "App was deployed"
